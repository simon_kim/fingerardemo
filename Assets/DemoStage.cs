﻿using UnityEngine;
using System.Collections;

public class DemoStage : MonoBehaviour {

    public GameObject[] targets;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ShowTarget(string targetName)
    {
        for (int i=0;i<targets.Length;i++)
        {
            if (targets[i].name.Equals(targetName))
            {
                targets[i].SetActive(true);
            } else
            {
                targets[i].SetActive(false);
            }
        }
    }

}
