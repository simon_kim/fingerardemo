﻿using UnityEngine;
using System.Collections;

public class FingerTalkPluginConnector : MonoBehaviour {

	public delegate void NativeListener();
	private NativeListener _listener;

	// Use this for initialization
	void Start () {
	
	}

	public void AddListener(NativeListener listener)
	{
		_listener += listener;
	}

	public void RemoveListener(NativeListener listener)
	{
		_listener -= listener;
	}

	// Update is called once per frame
	void Update () {
	
	}

    public void OnPluginKeyDown(string keycode)
    {
        Debug.Log("OnPluginKeyDown : " + keycode);
		sendKeyEventToListener();
    }

    public void OnPluginKeyUp(string keycode)
    {
        Debug.Log("OnPluginKeyUp : " + keycode);
		sendKeyEventToListener();
    }

	private void sendKeyEventToListener()
	{
		if (_listener != null)
		{
			Debug.Log("check Click Listener keydown: " + _listener.Target.ToString());
			_listener();
		}
		else
		{
			Debug.Log("[FingerTalkPluginConnector] No listener assigned");
		}
	}
}
