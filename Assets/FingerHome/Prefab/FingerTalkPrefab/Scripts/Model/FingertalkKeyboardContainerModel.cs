﻿using UnityEngine;
using System.Collections.Generic;

namespace fingertalk.vr.keyboard
{
    public class FingertalkKeyboardContainerModel : IKeyboardContainerModel
    {
		private const int MAX_KEYBOARD_LAYOUT = 4;
		private const int MAX_KEYBOARD_NUM = 3;
		private KEYBOARD currentKeyboard = KEYBOARD.KOREAN_KEYBOARD;
		public KEYBOARD CurrentKeyboard { get{ return currentKeyboard; }}
        private int _cursor;
        public int cursor { 
			get
            {
                return _cursor;
            }
            set
            {
                if (value > MAX_KEYBOARD_LAYOUT - 1)
                {
                    _cursor = 0;
                } 
				else if (value < 0)
                {
                    _cursor = MAX_KEYBOARD_LAYOUT - 1;
                } 
				else
                {
                    _cursor = value;
                }
            }
        }
        public KeyboardModel[] keyboardLayouts { get; set; }
		public string fingertalkBuffer { get; set; }
		public string buffer { get; set; }

        public FingertalkKeyboardContainerModel()
        {
			Debug.Log ("FingertalkKeyboardContainerModel created!!");
            cursor = 0;

			ClearBuffer ();
            
            keyboardLayouts = new KeyboardModel[MAX_KEYBOARD_LAYOUT] {
                new KeyboardModel(),
                new KeyboardModel(),
                new KeyboardModel(),
                new KeyboardModel()
            };

			SetCurrentKeyboard ();
        }

        public ACTION GetAction(int keyboardLayout, FINGER finger)
        {
            return keyboardLayouts[keyboardLayout].fingers[finger].action;
        }

        public string GetLabel(int keyboardLayout, FINGER finger)
        {
            return keyboardLayouts[keyboardLayout].fingers[finger].label;
        }

        public string GetValue(int keyboardLayout, FINGER finger)
        {
            return keyboardLayouts[keyboardLayout].fingers[finger].value;
        }

		public void ClearBuffer() {
			fingertalkBuffer = "";
		}

		public void ClearKeyboardLayouts() {
			foreach (KeyboardModel model in keyboardLayouts) {
				model.fingers.Clear ();
			}
		}

		public void ChangeKeyboard(){
			ClearKeyboardLayouts ();
			currentKeyboard++;
			if ((int)currentKeyboard == MAX_KEYBOARD_NUM) {
				currentKeyboard = KEYBOARD.KOREAN_KEYBOARD;
			}

			SetCurrentKeyboard ();

			KeyboardContainerView view = GameObject.Find ("KeyboardContainerView").GetComponent<KeyboardContainerView> ();
			view.SetKeyboardLabels (this);
		}

		public void SetCurrentKeyboard() {
			switch (currentKeyboard) {
			case KEYBOARD.KOREAN_KEYBOARD:
				SetKoreanKeyboard ();
				break;
			case KEYBOARD.ENGLISH_KEYBOARD:
				SetEnglishKeyboard ();
				break;
			case KEYBOARD.NUMBER_KEYBOARD:
				SetNumberKeyboard ();
				break;
			default:
				SetKoreanKeyboard ();
				break;
			}
		}

		public void SetKoreanKeyboard(){
			keyboardLayouts[0].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[0].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "ㅣ", "1"));
			keyboardLayouts[0].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, ".", "2"));
			keyboardLayouts[0].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "ㅡ", "3"));
			keyboardLayouts[0].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[1].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[1].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "ㄱㅋ", "4"));
			keyboardLayouts[1].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "ㄴㄹ", "5"));
			keyboardLayouts[1].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "ㄷㅌ", "6"));
			keyboardLayouts[1].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[2].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[2].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "ㅂㅍ", "7"));
			keyboardLayouts[2].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "ㅅㅎ", "8"));
			keyboardLayouts[2].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "ㅈㅊ", "9"));
			keyboardLayouts[2].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[3].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[3].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "Space", "10"));
			keyboardLayouts[3].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "ㅇㅁ", "0"));
			keyboardLayouts[3].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "Del", "11"));
			keyboardLayouts[3].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));
		}

		public void SetEnglishKeyboard(){
			keyboardLayouts[0].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[0].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, ".,?!", "1"));
			keyboardLayouts[0].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "ABC", "2"));
			keyboardLayouts[0].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "DEF", "3"));
			keyboardLayouts[0].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[1].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[1].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "GHI", "4"));
			keyboardLayouts[1].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "JKL", "5"));
			keyboardLayouts[1].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "MNO", "6"));
			keyboardLayouts[1].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[2].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[2].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "PQRS", "7"));
			keyboardLayouts[2].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "TUV", "8"));
			keyboardLayouts[2].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "WXYZ", "9"));
			keyboardLayouts[2].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[3].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[3].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "Space", "10"));
			keyboardLayouts[3].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "", "0"));
			keyboardLayouts[3].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "Del", "11"));
			keyboardLayouts[3].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));
		}

		public void SetNumberKeyboard(){
			keyboardLayouts[0].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[0].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "1", "1"));
			keyboardLayouts[0].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "2", "2"));
			keyboardLayouts[0].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "3", "3"));
			keyboardLayouts[0].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[1].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[1].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "4", "4"));
			keyboardLayouts[1].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "5", "5"));
			keyboardLayouts[1].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "6", "6"));
			keyboardLayouts[1].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[2].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[2].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "7", "7"));
			keyboardLayouts[2].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "8", "8"));
			keyboardLayouts[2].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "9", "9"));
			keyboardLayouts[2].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));

			keyboardLayouts[3].fingers.Add(FINGER.THUMB, new FingerVO(ACTION.PREV_KEYBOARD, "", "-1"));
			keyboardLayouts[3].fingers.Add(FINGER.INDEX, new FingerVO(ACTION.VALUE, "Space", "10"));
			keyboardLayouts[3].fingers.Add(FINGER.MIDDLE, new FingerVO(ACTION.VALUE, "0", "0"));
			keyboardLayouts[3].fingers.Add(FINGER.RING, new FingerVO(ACTION.VALUE, "Del", "11"));
			keyboardLayouts[3].fingers.Add(FINGER.LITTLE, new FingerVO(ACTION.NEXT_KEYBOARD, "", "-1"));
		}
    }
}