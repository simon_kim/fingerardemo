﻿using UnityEngine;
using System.Collections.Generic;

namespace fingertalk.vr.keyboard
{
    public enum ACTION { PREV_KEYBOARD, NEXT_KEYBOARD, VALUE }

	public enum FINGER {
		FUNC_ENTER=0,
		THUMB=9, 
		INDEX=51, 
		MIDDLE=113, 
		RING=59, 
		LITTLE=57,
		SPACE=62,
		DEL=67,
		ENTER=10,
		CHANGE_KEYBOARD=11002,
		CHANGE_IME_MODE=11003,
		BT1=10001,
		BT2=10002,
		BT3=10003,
		BT4=10004,
		BT5=10005,
		BT6=10006,
		BT7=10007,
		BT8=10008,
		BT9=10009,
		BT0=10000,
		SELECT=11004
	}

	public enum KEYBOARD {
		KOREAN_KEYBOARD=0,
		ENGLISH_KEYBOARD=1,
		NUMBER_KEYBOARD=2
	}

    public interface IKeyboardContainerModel
    {
        // Variables
		int cursor { get; set; }
		KeyboardModel[] keyboardLayouts { get; set; }
		KEYBOARD CurrentKeyboard { get; }
		string fingertalkBuffer { get; set; }
		string buffer { get; set; }

        // Mehtods
        ACTION GetAction(int keyboard, FINGER finger);
        string GetLabel(int keyboard, FINGER finger);
        string GetValue(int keyboard, FINGER finger);
		void ClearBuffer ();
		void ClearKeyboardLayouts();
		void ChangeKeyboard ();
		void SetCurrentKeyboard();
		void SetKoreanKeyboard();
		void SetEnglishKeyboard();
		void SetNumberKeyboard();
    }

    public class KeyboardModel
    {
        public Dictionary<FINGER, FingerVO> fingers;

        public KeyboardModel()
        {
            fingers = new Dictionary<FINGER, FingerVO>();
        }
    }

    public class FingerVO
    {
        public ACTION action { get; set; }
        public string label { get; set; }
        public string value { get; set; }

        public FingerVO(ACTION action, string label, string value)
        {
            this.action = action;
            this.label = label;
            this.value = value;
        }
    }
}