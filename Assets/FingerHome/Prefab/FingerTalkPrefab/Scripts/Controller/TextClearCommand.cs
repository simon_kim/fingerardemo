﻿using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using System;

namespace fingertalk.vr.keyboard
{
	public class TextClearCommand : EventCommand
	{
		[Inject]
		public IKeyboardContainerModel model { get; set; }

		public override void Execute()
		{
			model.buffer = "";
		}
	}
}