﻿using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace fingertalk.vr.keyboard
{
    public class FingerActionCommand : EventCommand
    {
        [Inject]
        public IKeyboardContainerModel keyboardContainerModel { get; set; }

        [Inject]
        public IFingertalkCore fingertalkCore { get; set; }

        public override void Execute()
        {
            FINGER finger = (FINGER)evt.data;

			if (finger == FINGER.FUNC_ENTER) {
				if (fingertalkCore != null) {
					CommitText ();
				}
				dispatcher.Dispatch (FingertalkEvent.COMPLETE);
				return;
			}

			string result = null;

			switch (finger) {
			case FINGER.BT0:
			case FINGER.BT1:
			case FINGER.BT2:
			case FINGER.BT3:
			case FINGER.BT4:
			case FINGER.BT5:
			case FINGER.BT6:
			case FINGER.BT7:
			case FINGER.BT8:
			case FINGER.BT9:
				result = fingertalkCore.handleInput ((int)keyboardContainerModel.CurrentKeyboard, 0, (int)finger);
				if (fingertalkCore.commitTyped ()) {
					dispatcher.Dispatch (FingertalkEvent.TEXT_COMMIT, keyboardContainerModel.fingertalkBuffer);
					keyboardContainerModel.ClearBuffer ();
				}
				keyboardContainerModel.fingertalkBuffer = result;
				dispatcher.Dispatch(FingertalkEvent.TEXT_CHANGE, keyboardContainerModel.fingertalkBuffer);
				break;
			case FINGER.SPACE:
				if (keyboardContainerModel.fingertalkBuffer.Length > 0) {
					CommitText ();
				} else {
					dispatcher.Dispatch (FingertalkEvent.TEXT_COMMIT, " ");
				}

				break;
			case FINGER.DEL:
				if (keyboardContainerModel.fingertalkBuffer.Length > 0) {
					keyboardContainerModel.fingertalkBuffer = fingertalkCore.deleteCharacter ();
					dispatcher.Dispatch (FingertalkEvent.TEXT_CHANGE, keyboardContainerModel.fingertalkBuffer);
				} else {
					dispatcher.Dispatch (FingertalkEvent.TEXT_DELETE);
				}
				break;
			case FINGER.ENTER:

				break;
			case FINGER.CHANGE_KEYBOARD:
				Debug.Log ("FingerAction CHANGE_KEYBOARD");
				CommitText ();
				keyboardContainerModel.ChangeKeyboard ();
				break;
			case FINGER.CHANGE_IME_MODE:

				break;
			case FINGER.SELECT:
				dispatcher.Dispatch (FingertalkEvent.TEXT_SELECT);
				fingertalkCore.clearAllBufferOfFingertalkLib ();
				keyboardContainerModel.ClearBuffer ();
				break;
			default:
				break;
			}
        }

		private void CommitText(){
			dispatcher.Dispatch (FingertalkEvent.TEXT_COMMIT, keyboardContainerModel.fingertalkBuffer);
			keyboardContainerModel.ClearBuffer ();
			fingertalkCore.clearAllBufferOfFingertalkLib ();
		}
			
    }
}