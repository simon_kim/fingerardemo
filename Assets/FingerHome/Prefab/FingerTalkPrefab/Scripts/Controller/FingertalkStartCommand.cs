﻿using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using System;

namespace fingertalk.vr.keyboard
{
	public class FingertalkStartCommand : EventCommand
	{
		[Inject]
		public IInputEventService inputEventService { get; set; }

		public override void Execute()
		{
            Debug.Log("[FingertalkStartCommand] Execute");
			inputEventService.Activate();
		}
	}
}