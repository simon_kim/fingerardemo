﻿namespace fingertalk.vr.keyboard
{
	public enum FingertalkEvent
	{
		TEXT_CHANGE,
		TEXT_COMMIT,
		TEXT_DELETE,
		TEXT_CLEAR,
		TEXT_SELECT,
		FINGER_ACTION,
		COMPLETE
	}
}