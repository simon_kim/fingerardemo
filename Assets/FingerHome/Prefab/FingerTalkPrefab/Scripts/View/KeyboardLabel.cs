﻿using UnityEngine;
using UnityEngine.UI;

namespace fingertalk.vr.keyboard 
{ 
    public class KeyboardLabel : MonoBehaviour 
	{
        public int keyboard;
        public FINGER finger;

		public void setText(IKeyboardContainerModel model)
		{
			gameObject.GetComponent<Text>().text = model.GetLabel(keyboard, finger);
		}
    }
}
