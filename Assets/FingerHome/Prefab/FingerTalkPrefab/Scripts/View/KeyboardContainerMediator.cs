﻿using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;

namespace fingertalk.vr.keyboard 
{ 
    public class KeyboardContainerMediator : EventMediator 
	{
        [Inject]
        public KeyboardContainerView view { get; set; }

        [Inject]
        public IKeyboardContainerModel model { get; set; }

        public override void OnRegister()
        {
			dispatcher.AddListener(ModelEvent.KEYBOARD_CHANGE, onKeyboardChange);
            dispatcher.AddListener(FingertalkEvent.FINGER_ACTION, onKeyboardAction);
            view.init(model);
        }

        public override void OnRemove()
        {
            dispatcher.RemoveListener(ModelEvent.KEYBOARD_CHANGE, onKeyboardChange);
            dispatcher.RemoveListener(FingertalkEvent.FINGER_ACTION, onKeyboardAction);
        }

        private void onKeyboardChange()
        {
            view.curKeyboardIdx = model.cursor;
        }

        private void onKeyboardAction(IEvent evt)
        {
			Debug.Log ("AAABBB : " + evt.data);
            view.highlightButton((FINGER)evt.data);
        }
    }
}