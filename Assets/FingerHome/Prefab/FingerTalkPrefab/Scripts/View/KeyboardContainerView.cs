﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using strange.extensions.mediation.impl;

namespace fingertalk.vr.keyboard
{
    public class KeyboardContainerView : View
    {
        public GameObject[] keyboards;
        public GameObject[] fingerBtn;

        public Texture originalTexture;
        public Texture highlightTexture;

        private int _curKeyboardIdx;

        public int curKeyboardIdx
        {
            get
            {
                return _curKeyboardIdx;
            }
            set
            {
                keyboards[_curKeyboardIdx].GetComponent<KeyboardLayout>().blur();
                _curKeyboardIdx = value;
                keyboards[_curKeyboardIdx].GetComponent<KeyboardLayout>().focus();
            }
        }

        internal void init(IKeyboardContainerModel model)
        {
            curKeyboardIdx = 0;
			SetKeyboardLabels (model);
        }

		public void SetKeyboardLabels(IKeyboardContainerModel model){
			foreach (KeyboardLabel label in gameObject.GetComponentsInChildren<KeyboardLabel>())
			{
				label.setText(model);
			}
		}

        public void highlightButton(FINGER finger)
        {
            StartCoroutine(highlightEffect(finger));
        }

        private IEnumerator highlightEffect(FINGER finger)
        {
            RawImage target = getRawImage(finger);
			if (target != null) {
				target.texture = highlightTexture;
				yield return new WaitForSeconds (0.3f);
				target.texture = originalTexture;
			}
        }

        private RawImage getRawImage(FINGER finger)
        {
            switch(finger)
            {
                case FINGER.BT1:
                    return fingerBtn[0].GetComponent<RawImage>();
                case FINGER.BT2:
                    return fingerBtn[1].GetComponent<RawImage>();
                case FINGER.BT3:
                    return fingerBtn[2].GetComponent<RawImage>();
                case FINGER.BT4:
                    return fingerBtn[3].GetComponent<RawImage>();
                case FINGER.BT5:
                    return fingerBtn[4].GetComponent<RawImage>();
                case FINGER.BT6:
                    return fingerBtn[5].GetComponent<RawImage>();
                case FINGER.BT7:
                    return fingerBtn[6].GetComponent<RawImage>();
                case FINGER.BT8:
                    return fingerBtn[7].GetComponent<RawImage>();
                case FINGER.BT9:
                    return fingerBtn[8].GetComponent<RawImage>();
                case FINGER.SPACE:
                    return fingerBtn[9].GetComponent<RawImage>();
                case FINGER.BT0:
                    return fingerBtn[10].GetComponent<RawImage>();
                case FINGER.DEL:
                    return fingerBtn[11].GetComponent<RawImage>();
			default:
				return null;
            }
        }
    }
}