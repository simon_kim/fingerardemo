﻿using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using strange.extensions.mediation.api;

namespace fingertalk.vr.keyboard
{
	public class FingertalkContext : MVCSContext
	{
		public FingertalkContext (MonoBehaviour view) : base(view)
		{
		}

		public FingertalkContext (MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
		{
		}

		protected override void mapBindings()
		{
			// Injection Binding
			injectionBinder.Bind<IKeyboardContainerModel>().To<FingertalkKeyboardContainerModel>().ToSingleton();
			injectionBinder.Bind<IInputEventService>().To<UnityInputEventService>().ToSingleton();
			injectionBinder.Bind<IFingertalkCore>().To<FingertalkCore>().ToSingleton();

			// Mediation Binding
			mediationBinder.Bind<KeyboardContainerView>().To<KeyboardContainerMediator>();

			// Command Binding
			commandBinder.Bind(ContextEvent.START).To<FingertalkStartCommand>().Once();
			commandBinder.Bind(FingertalkEvent.FINGER_ACTION).To<FingerActionCommand>();
			commandBinder.Bind (FingertalkEvent.TEXT_CLEAR).To<TextClearCommand> ();
		}
	}
}