﻿using System;
using UnityEngine;

namespace fingertalk.vr.keyboard
{
	class FingertalkCore : IFingertalkCore
	{
		private AndroidJavaObject fingertalkFilter;
		public AndroidJavaClass fingertalkConst;

		public FingertalkCore()
		{
			if (!Application.isEditor)
			{
				fingertalkFilter = new AndroidJavaObject("com.xeedlab.fingertalk.fingertalklib.FingertalkFilter");
				fingertalkConst = new AndroidJavaClass ("com.xeedlab.fingertalk.fingertalklib.FingertalkConst");
			}
		}

		public string handleInput(int keyboardIdx, int layoutIdx, int primaryCode) {
			string ret = null;
			if (fingertalkFilter != null) {
				ret = fingertalkFilter.Call<string> ("handleInput", keyboardIdx, layoutIdx, primaryCode - 10000);
			} else {
					ret = "1";
			}

			return ret;
		}

		public string deleteCharacter() {
			string ret = null;
			if (fingertalkFilter != null)
			{
				ret = fingertalkFilter.Call<string>("deleteCharacter");
			}

			return ret;
		}

		public void clearAllBufferOfFingertalkLib() {
			if (fingertalkFilter != null)
			{
				fingertalkFilter.Call("clearAll");
			}
		}

		public bool commitTyped(){
			bool ret = false;
			if (fingertalkFilter != null)
			{
				ret = fingertalkFilter.Call<bool>("commitTyped");
			}

			return ret;
		}

		private void setUpFingertalkConst() {
		}
	}
}
