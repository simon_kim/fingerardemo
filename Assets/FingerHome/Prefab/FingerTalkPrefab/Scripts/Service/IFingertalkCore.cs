﻿namespace fingertalk.vr.keyboard
{
	public enum FingertalkConstField {SPACE_KEY_SET, DEL_KEY_SET, ENTER_KEY_SET, CHANGE_KEYBOARD_KEY_SET}

    public interface IFingertalkCore
    {
		string handleInput (int keyboardIdx, int layoutIdx, int primaryCode);
		string deleteCharacter ();
		void clearAllBufferOfFingertalkLib ();
		bool commitTyped();
    }
}
