﻿using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace fingertalk.vr.keyboard
{
    class UnityInputEventService : IInputEventService
    {
        [Inject(ContextKeys.CONTEXT_DISPATCHER)]
        public IEventDispatcher dispatcher { get; set; }

		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		[Inject]
		public IKeyboardContainerModel keyboardContainerModel { get; set; }

		[Inject]
		public IFingertalkCore fingertalkCore { get; set; }

        public void Activate()
        {
            GameObject go = new GameObject();
            go.name = "UnityInputInternalModule";
			UnityInputInternalModule module = go.AddComponent<UnityInputInternalModule>();
            module.setDispatcher(dispatcher);
			module.setKeyboardContainerModel (keyboardContainerModel);
			module.setFingertalkCore (fingertalkCore);
			go.transform.parent = contextView.transform;
        }
    }

    class UnityInputInternalModule : MonoBehaviour
    {
        private IEventDispatcher dispatcher;
		private IKeyboardContainerModel keyboardContainerModel;
		private IFingertalkCore fingertalkCore;
		private bool isComposing = false;

        public void setDispatcher(IEventDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
        }

		public void setKeyboardContainerModel(IKeyboardContainerModel keyboardContainerModel) {
			this.keyboardContainerModel = keyboardContainerModel;
		}

		public void setFingertalkCore(IFingertalkCore fingertalkCore) {
			this.fingertalkCore = fingertalkCore;
		}

        void Update()
        {
            if (dispatcher != null)
            {


//				if (Input.GetKeyDown (KeyCode.Alpha1)) {
//					onKeyDown (KeyCode.Alpha1);
//				}
                /*
				foreach (KeyCode keyCode in USING_KEY_CODES) {
					if (Input.GetKeyDown (keyCode)) {
						UnityEngine.Debug.Log ("UnityInputEvent: "+keyCode);
						onKeyDown (keyCode);
						break;
					}
				}
				foreach (KeyCode keyCode in USING_KEY_CODES) {
					if (Input.GetKeyUp (keyCode)) {
						onKeyUp (keyCode);
						break;
					}
				}
                */
            }
        }

        public void dispatchKeyDownEvent(KeyCode keycode)
        {
            if (dispatcher != null)
            {
                UnityEngine.Debug.Log("[UnityInputEventService] dispatchKeyDownEvent - " + keycode.ToString());
                onKeyDown(keycode);
            }
        }

        public void dispatchKeyUpEvent(KeyCode keycode)
        {
            if (dispatcher != null)
            {
                UnityEngine.Debug.Log("[UnityInputEventService] dispatchKeyUpEvent - " + keycode.ToString());
                onKeyUp(keycode);
            }
        }

        private void onKeyDown(KeyCode keyCode) 
		{
			FINGER finger = FINGER.FUNC_ENTER;
			switch (keyCode) {
			case KeyCode.Alpha1:
				finger = FINGER.BT1;
				break;
			case KeyCode.Alpha2:
				finger = FINGER.BT2;
				break;
			case KeyCode.Alpha3:
				finger = FINGER.BT3;
				break;
			case KeyCode.Alpha4:
				finger = FINGER.BT4;
				break;
			case KeyCode.Alpha5:
				finger = FINGER.BT5;
				break;
			case KeyCode.Alpha6:
				finger = FINGER.BT6;
				break;
			case KeyCode.Alpha7:
				finger = FINGER.BT7;
				break;
			case KeyCode.Alpha8:
				finger = FINGER.BT8;
				break;
			case KeyCode.Alpha9:
				finger = FINGER.BT9;
				break;
			case KeyCode.Alpha0:
				finger = FINGER.BT0;
				break;
			case KeyCode.F1:
				finger = FINGER.SPACE;
				break;
			case KeyCode.F2:
				finger = FINGER.DEL;
				break;
			case KeyCode.F3:
				finger = FINGER.SELECT;
				break;
			case KeyCode.F4:
				finger = FINGER.CHANGE_KEYBOARD;
				break;
            case KeyCode.F5:
            case KeyCode.F6:
                finger = FINGER.FUNC_ENTER;
                break;
            default:
				break;
			}
			onKey (finger);
		}

		private void onKeyUp(KeyCode keyCode)
		{
		}

		private void onKey (FINGER finger) {
			UnityEngine.Debug.Log ("onKey "+ finger);
			checkComposingTimer ();

			dispatcher.Dispatch(FingertalkEvent.FINGER_ACTION, finger);

		}


		private void checkComposingTimer() {
			if (!isComposing) {
				StartCoroutine ("Run");
			} else {
				StopCoroutine ("Run");
				StartCoroutine ("Run");
			}
		}

		private IEnumerator Run() {
				isComposing = true;
				yield return new WaitForSeconds(2);

				dispatcher.Dispatch (FingertalkEvent.TEXT_COMMIT, keyboardContainerModel.fingertalkBuffer);
				keyboardContainerModel.ClearBuffer ();
				fingertalkCore.clearAllBufferOfFingertalkLib ();
				isComposing = false;
			}
	    }
}
