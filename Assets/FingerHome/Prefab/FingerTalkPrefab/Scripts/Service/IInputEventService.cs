﻿namespace fingertalk.vr.keyboard
{
    public interface IInputEventService
    {
        void Activate();
    }
}
