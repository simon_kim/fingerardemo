﻿using strange.extensions.context.impl;
using UnityEngine;

namespace fingertalk.vr.keyboard
{
	public class FingertalkRoot : ContextView
	{
		void Awake()
		{
			context = new FingertalkContext(this);
		}
	}
}