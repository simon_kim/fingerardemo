﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using com.fingertalk.vrhome.main;

public class RecenterBase : MonoBehaviour {

    public GameObject recenterWarning;

    public GameObject leftTop;
    public GameObject rightTop;
    public GameObject rightBottom;
    public GameObject leftBottom;

    public int midCount = 4;
    private Camera origin;
    private List<Vector3> points;
    private FingerTalkPluginConnector _controller;

    // Use this for initialization
    void Start()
    {
        origin = GameObject.Find("Main Camera").GetComponent<Camera>();
        _controller = GameObject.Find("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector>();
        //_controller.AddListener(OnNativeEvent);
    }

    // Update is called once per frame
    void Update()
    {
        checkInterfere();
    }

    public void OnNativeEvent(string evt)
    {
        bool clear = true;
        updatePoints();
        foreach (Vector3 point in points)
        {
            if (!isClear(origin, point))
            {
                clear = false;
                recenterWarning.SetActive(false);
                break;
            }
        }
        if (clear)
        {
            recenterWarning.SetActive(true);
            //GvrViewer.Instance.Recenter();
			//GvrController.GetInstance.RecenterController ();
        }
    }

    void checkInterfere()
    {
        bool clear = true;
        updatePoints();
        foreach (Vector3 point in points)
        {
            if (!isClear(origin, point))
            {
                clear = false;
                recenterWarning.SetActive(false);
                break;
            }
        }
        if (clear)
        {
            recenterWarning.SetActive(true);
            /*
            if (Input.anyKeyDown)
            {
                GvrViewer.Instance.Recenter();
            }
            */
        }
    }

    void updatePoints()
    {
        Vector3 posLeftTop = leftTop.transform.position;
        Vector3 posRightTop = rightTop.transform.position;
        Vector3 posRightBottom = rightBottom.transform.position;
        Vector3 posLeftBottom = leftBottom.transform.position;

        points = new List<Vector3>();

        // Add corner points
        points.Add(posLeftTop);
        points.Add(posRightTop);
        points.Add(posRightBottom);
        points.Add(posLeftBottom);

        // Add mid points
        addMidPoints(points, posLeftTop, posRightTop, midCount);
        addMidPoints(points, posRightTop, posRightBottom, midCount);
        addMidPoints(points, posRightBottom, posLeftBottom, midCount);
        addMidPoints(points, posLeftBottom, posLeftTop, midCount);
    }

    private void addMidPoints(List<Vector3> points, Vector3 point1, Vector3 point2, int count)
    {
        for (int i = 0; i < count; i++)
        {
            points.Add(point1 + (point2 - point1) / (count + 1) * (i + 1));
        }
    }

    private bool isClear(Camera origin, Vector3 target)
    {
        Vector3 fwd = target - origin.transform.position;
        Debug.DrawRay(origin.transform.position, fwd);
        if (Physics.Raycast(origin.transform.position, fwd, 800))
        {
            return false;
        }
        return true;
    }
}
