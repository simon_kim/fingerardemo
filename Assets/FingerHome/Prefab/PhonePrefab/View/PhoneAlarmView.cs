﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace fingertalk.vr.phone
{
	public class PhoneAlarmView : EventView
	{
		public GameObject Icon;
		public GameObject Caller;
		public GameObject Time;
		public GameObject State;

		internal void init()
		{
		}

		void Update()
		{
		}

		public void hideView() {
			StartCoroutine ("destroyPhoneAlarm");
		}

		IEnumerator destroyPhoneAlarm()
		{
			GameObject PhoneAlarmView = GameObject.Find ("PhoneAlarmView");
			yield return new WaitForSeconds(4.0f);
			PhoneAlarmView.SetActive (false);
		}
	}
			
}

