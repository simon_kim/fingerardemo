﻿/// Example mediator
/// =====================
/// Make your Mediator as thin as possible. Its function is to mediate
using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;

namespace fingertalk.vr.phone
{
	public class PhoneAlarmViewMediator: EventMediator
	{
		[Inject]
		public PhoneAlarmView view { get; set; }

		GameObject phoneAlarmView;

		public override void OnRegister()
		{
			phoneAlarmView = GameObject.Find ("PhoneAlarmView");
			phoneAlarmView.SetActive (false);
			dispatcher.AddListener (MainEvent.RECEIVED_CALL, showAlarm);
			view.init();
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (MainEvent.RECEIVED_CALL, showAlarm);
		}

		public void showAlarm(IEvent evt)
		{
			phoneAlarmView.SetActive (true);
			string caller = evt.data as string;
			String sysTime = System.DateTime.Now.ToString ("tt-hh:mm");
			String[] time = sysTime.Split ('-');
			if (time [0] == "AM") {
				time [0] = "오전";
			} else {
				time[0] = "오후";
			}
			//draw UI
			Debug.Log ("[PhoneAlarmViewMediator] showAlarm Number : " + caller);
			view.Caller.GetComponent<Text> ().text = caller;
			view.Time.GetComponent<Text> ().text = time[0] + " " + time[1];
			view.hideView ();
		}
	}
}
