﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace fingertalk.vr.phone
{
    public class PhoneAlarmContext : MVCSContext
    {
		public PhoneAlarmContext(MonoBehaviour view) : base(view)
        {
        }

		public PhoneAlarmContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
			injectionBinder.Bind<IPhoneAlarmModel>().To<PhoneAlarmModel>().ToSingleton();
			injectionBinder.Bind<IPhoneAlarmService>().To<PhoneAlarmService>().ToSingleton();

			mediationBinder.Bind<PhoneAlarmView>().To<PhoneAlarmViewMediator>();

            commandBinder.Bind(ContextEvent.START).To<StartAlarmCommand>().Once();

        }
    }
}

