﻿using System;
using UnityEngine;
using strange.extensions.context.impl;

namespace fingertalk.vr.phone
{
	public class PhoneAlarmRoot : ContextView
    {
        void Awake()
        {
            context = new PhoneAlarmContext(this);
        }
    }
}

