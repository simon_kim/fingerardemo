﻿using System;
using System.Collections;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace fingertalk.vr.message
{
	public class MessageService : IMessageService
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        [Inject]
        public IEventDispatcher dispatcher { get; set; }

		public MessageService()
        {
        }
    }
}

