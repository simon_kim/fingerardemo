﻿namespace fingertalk.vr.message
{
	public enum MessageEvent
	{
		SHOW_KEYBOARD,
		DESTROY_KERYBOARD,
		SMS_RECEIVED,
		SMS_SHOW,
		SMS_SEND
	}
}