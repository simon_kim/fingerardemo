﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;

using com.fingertalk.vrhome.main;

namespace fingertalk.vr.message
{
	public class DestroyKeyboardCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		public override void Execute()
		{
			GameObject keyboard = GameObject.Find("Fingertalk") as GameObject;
			GameObject.Destroy(keyboard);
		}
	}
}