﻿using System;
using UnityEngine;
using strange.extensions.context.impl;

namespace fingertalk.vr.message
{
	public class MessageRoot : ContextView
    {
        void Awake()
        {
            context = new MessageContext(this);
        }
    }
}

