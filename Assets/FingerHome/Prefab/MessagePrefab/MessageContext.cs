﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace fingertalk.vr.message
{
    public class MessageContext : MVCSContext
    {
		public MessageContext(MonoBehaviour view) : base(view)
        {
        }

		public MessageContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
			injectionBinder.Bind<IMessageModel>().To<MessageModel>().ToSingleton();
			injectionBinder.Bind<IMessageService>().To<MessageService>().ToSingleton();

			mediationBinder.Bind<MessageIconView>().To<MessageIconMediator>();
			mediationBinder.Bind<MessageContainerView>().To<MessageContainerViewMediator>();
			mediationBinder.Bind<MessageTitleView>().To<MessageTitleViewMediator>();
			mediationBinder.Bind<MessageContentsView>().To<MessageContentsViewMediator>();
			mediationBinder.Bind<MessageReplyView>().To<MessageReplyViewMediator>();

			commandBinder.Bind (MessageEvent.DESTROY_KERYBOARD).To<DestroyKeyboardCommand>();
			commandBinder.Bind (MessageEvent.SMS_SEND).To<SendSMSCommand> ();
            commandBinder.Bind(ContextEvent.START).To<StartCommand>().Once();

        }
    }
}

