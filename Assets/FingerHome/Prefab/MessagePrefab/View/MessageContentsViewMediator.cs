﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main;
using com.fingertalk.vrhome.main.events;

namespace fingertalk.vr.message
{
	public class MessageContentsViewMediator : EventMediator
	{
		[Inject]
		public MessageContentsView view { get; set; }

		private GameObject replyButton;
		private GameObject replyContent;

		public override void OnRegister()
		{
			dispatcher.AddListener (MessageEvent.SMS_SHOW, showMessage);
			view.init();
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (MessageEvent.SMS_SHOW, showMessage);
		}

		public GameObject frameSmsContents;
		public GameObject frameBackground;
		public GameObject frameMsg;
		public GameObject frameDate;
		private Scrollbar scrollBar;

		public void showMessage(IEvent evt)
		{
			string messageInfo = evt.data as string;
			string[] data = messageInfo.Split (';');
			string content = data [2];
			String sysTime = System.DateTime.Now.ToString ("yyyy-M-d-hh:mm-tt");
			String[] time = sysTime.Split ('-');
			string contentDate = "\n" + time[0] + "년 " + time[1] + "월 " + time[2] + "일 " + time[3] + " " + time[4];

			drawMessageContent (content, contentDate);
			StartCoroutine("setScrollBar");
		}

		public IEnumerator setScrollBar() {
			yield return new WaitForSeconds(0.0f);
			GameObject scrollBar = GameObject.Find ("ContentsScrollBar");
			if (null != scrollBar) {
				Scrollbar bar = scrollBar.GetComponent<Scrollbar> ();
				bar.value = -1.0f;
			}
		}

		public void drawMessageContent(string content, string contentDate) {
			frameSmsContents = new GameObject();
			frameSmsContents.name = "SmsContents";
			frameSmsContents.AddComponent<VerticalLayoutGroup>();
			frameSmsContents.AddComponent<ContentSizeFitter>();

			frameSmsContents.transform.SetParent(view.ViewPort.transform);

			frameBackground = new GameObject();
			frameBackground.name = "MessageContentBackground";
			frameBackground.AddComponent<RawImage>();
			frameBackground.AddComponent<VerticalLayoutGroup>();
			frameBackground.AddComponent<ContentSizeFitter>();

			frameBackground.GetComponent<RawImage> ().texture = view.cotentBackgroundTexture [0];
			frameBackground.GetComponent<RawImage> ().raycastTarget = true;
			frameBackground.GetComponent<RectTransform> ().localPosition = new Vector3 (0, 0, 0);
			frameBackground.GetComponent<RectTransform> ().localRotation = new Quaternion (0, 0, 0, 0);
			frameBackground.GetComponent<RectTransform> ().sizeDelta = new Vector2 (512f, 105f);
			frameBackground.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			frameBackground.transform.SetParent(frameSmsContents.transform);

			frameMsg = new GameObject();
			frameMsg.name = "MessageContentMessage";
			frameMsg.AddComponent<Text>();
			frameMsg.GetComponent<RectTransform> ().localPosition = new Vector3 (0, 0, 0);
			frameMsg.GetComponent<RectTransform> ().localRotation = new Quaternion (0, 0, 0, 0);
			frameMsg.GetComponent<RectTransform> ().sizeDelta = new Vector2 (412f, 30f);
			frameMsg.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			frameMsg.transform.SetParent(frameBackground.transform);

			frameDate = new GameObject();
			frameDate.name = "MessageContentDate";
			frameDate.AddComponent<Text>();
			frameDate.GetComponent<RectTransform> ().localPosition = new Vector3 (0, 0, 0);
			frameDate.GetComponent<RectTransform> ().localRotation = new Quaternion (0, 0, 0, 0);
			frameDate.GetComponent<RectTransform> ().sizeDelta = new Vector2 (412f, 22f);
			frameDate.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			frameDate.transform.SetParent(frameBackground.transform);

			frameBackground.GetComponent<VerticalLayoutGroup> ().padding.left = 30;
			frameBackground.GetComponent<VerticalLayoutGroup> ().padding.right = 10;
			frameBackground.GetComponent<VerticalLayoutGroup> ().padding.top = 10;
			frameBackground.GetComponent<VerticalLayoutGroup> ().padding.bottom = 10;
			frameBackground.GetComponent<VerticalLayoutGroup> ().childForceExpandHeight = true;
			frameBackground.GetComponent<VerticalLayoutGroup> ().childForceExpandWidth = false;
			frameBackground.GetComponent<VerticalLayoutGroup> ().childAlignment = TextAnchor.MiddleCenter;
			frameBackground.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			frameBackground.GetComponent<ContentSizeFitter> ().horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
				
			frameMsg.GetComponent<Text> ().alignment = TextAnchor.UpperLeft;
			frameMsg.GetComponent<Text> ().supportRichText = false;
			frameMsg.GetComponent<Text> ().raycastTarget = true;
			frameMsg.GetComponent<Text> ().fontSize = 18;
			frameMsg.GetComponent<Text> ().font = Resources.GetBuiltinResource (typeof(Font), "Arial.ttf") as Font;
			frameMsg.GetComponent<Text> ().text = content;

			frameDate.GetComponent<Text> ().alignment = TextAnchor.LowerRight;
			frameDate.GetComponent<Text> ().supportRichText = false;
			frameDate.GetComponent<Text> ().raycastTarget = true;
			frameDate.GetComponent<Text> ().fontSize = 10;
			frameDate.GetComponent<Text> ().font = Resources.GetBuiltinResource (typeof(Font), "Arial.ttf") as Font;
			frameDate.GetComponent<Text> ().text = contentDate;

			frameSmsContents.GetComponent<VerticalLayoutGroup> ().padding.left = 20;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().padding.right = 20;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().padding.top = 30;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().padding.bottom = 30;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().spacing = 30;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().childForceExpandHeight = true;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().childForceExpandWidth = true;
			frameSmsContents.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			frameSmsContents.GetComponent<ContentSizeFitter> ().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;

			frameSmsContents.GetComponent<RectTransform> ().position = new Vector3 (-0f, -140f, 0f);

			if (view.ViewPort.transform.childCount <= 1) {
				frameSmsContents.GetComponent<RectTransform> ().localPosition = new Vector3 (241.5f, -80f, 0f);
				frameSmsContents.GetComponent<RectTransform> ().Rotate(new Vector3(0.5f, 45.0f, 1.7f));
				frameSmsContents.GetComponent<RectTransform> ().localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
				frameSmsContents.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			} else {
				frameSmsContents.GetComponent<RectTransform> ().localPosition = new Vector3 (250f,-75f*(view.ViewPort.transform.childCount), 0f);
				frameSmsContents.GetComponent<RectTransform> ().Rotate(new Vector3(0.5f, 45.0f, 1.7f));
				frameSmsContents.GetComponent<RectTransform> ().localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
				frameSmsContents.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			}
		}
	}
}