﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main;

namespace fingertalk.vr.message
{
	public class MessageContentsView : EventView
	{
		public ScrollRect ScrollRect;
		public Texture[] cotentBackgroundTexture;
		public GameObject ViewPort;
		public GameObject Content;
		public GameObject ContentsReplyButton;

		public bool replyButtonFocused = false;

		private GameObject button;
		private GameObject replyContent;

		public void onReplyButtonFocusIn() {
			Debug.Log ("onReplyButtonFocusIn()");
			replyButtonFocused = true;
			//GameObject.Find ("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector> ().AddListener (onClick);
		}

		public void onReplyButtonFocusOut() {
			Debug.Log ("onReplyButtonFocusOut()");
			replyButtonFocused = false;
			//GameObject.Find ("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector> ().RemoveListener (onClick);
		}

		public void onClick(string evt) {
			Debug.Log("MessageContentsView OnClick is started!!!");
			if (replyButtonFocused) {
				if (button != null && button.activeInHierarchy) {
					button.SetActive (false);
					replyContent.SetActive (true);
				}
			} else {
			}
		}

		internal void init()
		{
			button = ContentsReplyButton;
			replyContent = GameObject.Find ("ReplyContent");
			GameObject.Find ("ReplyContent").SetActive (false);
		}

		void Update()
		{
		}

	}
}

