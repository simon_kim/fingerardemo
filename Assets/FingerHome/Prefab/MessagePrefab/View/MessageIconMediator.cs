﻿/// Example mediator
/// =====================
/// Make your Mediator as thin as possible. Its function is to mediate
using System;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;

namespace fingertalk.vr.message
{
    public class MessageIconMediator : EventMediator
    {
        [Inject]
		public MessageIconView view { get; set; }

		GameObject smsAlarmIcon;

		public override void OnRegister()
		{
			smsAlarmIcon = GameObject.Find ("MessageIcon");
			smsAlarmIcon.SetActive (false);
			dispatcher.AddListener (MainEvent.RECEIVED_SMS, showAlarm);
			view.init();
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (MainEvent.RECEIVED_SMS, showAlarm);
		}

		public void showAlarm(IEvent evt)
		{
			smsAlarmIcon.SetActive (true);
		}
    }
}

