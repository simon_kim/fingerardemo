﻿/// Example mediator
/// =====================
/// Make your Mediator as thin as possible. Its function is to mediate
using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;

namespace fingertalk.vr.message
{
	public class MessageContainerViewMediator : EventMediator
	{
		[Inject]
		public MessageContainerView view { get; set; }

		GameObject smsAlarmView;

		public override void OnRegister()
		{
			smsAlarmView = GameObject.Find ("MessageContainerView");
			smsAlarmView.SetActive (false);
			dispatcher.AddListener (MainEvent.RECEIVED_SMS, showAlarm);
			view.init();
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (MainEvent.RECEIVED_SMS, showAlarm);
		}

		public void showAlarm(IEvent evt)
		{
			smsAlarmView.SetActive (true);
			string messageInfo = evt.data as string;
			dispatcher.Dispatch (MessageEvent.SMS_SHOW, messageInfo);
		}
	}
}

