﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main;

namespace fingertalk.vr.message
{
	public class MessageIconView : EventView
	{
//		public GameObject MessageContainerView;
		public GameObject MessageContainer;
		public GameObject MessageImage;
		public GameObject MessageCount;
		public GameObject MessageCountText;
		public GameObject MessageEffect;

		private RawImage image;
		public bool focus = false;

		internal void init()
		{
		}

		void Update()
		{
		}

	}
}

