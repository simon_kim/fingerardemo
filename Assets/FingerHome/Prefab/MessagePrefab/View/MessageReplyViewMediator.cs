﻿/// Example mediator
/// =====================
/// Make your Mediator as thin as possible. Its function is to mediate
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main;
using com.fingertalk.vrhome.main.events;
using fingertalk.vr.keyboard;
namespace fingertalk.vr.message
{
	public class MessageReplyViewMediator : EventMediator
	{
		[Inject]
		public MessageReplyView view { get; set; }

		public bool isKeyboardShow = false;
		public string[] data;
		public override void OnRegister()
		{
			dispatcher.AddListener (FingertalkEvent.TEXT_CHANGE, onTextChange);
			dispatcher.AddListener (FingertalkEvent.COMPLETE, onTextComplete);
			view.init(	);
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (FingertalkEvent.TEXT_CHANGE, onTextChange);
			dispatcher.RemoveListener (FingertalkEvent.COMPLETE, onTextComplete);
		}

		private void onTextChange(IEvent evt)
		{
			Debug.Log("MessageReplyView TextChanged!!!");
			GameObject ReplyContent = GameObject.Find ("ReplyContent");
			if (null != ReplyContent) {
				if (ReplyContent.transform.childCount >= 3) {
					view.outUIText = evt.data as string;
				}
			}
		}

		private void onTextComplete() {
			GameObject keyboard = GameObject.Find ("Fingertalk");
			GameObject.Destroy (keyboard);
		}
			
	}
}

