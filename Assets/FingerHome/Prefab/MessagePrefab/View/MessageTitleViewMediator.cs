﻿/// Example mediator
/// =====================
/// Make your Mediator as thin as possible. Its function is to mediate
using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace fingertalk.vr.message
{
	public class MessageTitleViewMediator : EventMediator
	{
		[Inject]
		public MessageTitleView view { get; set; }

		public override void OnRegister()
		{
			dispatcher.AddListener (MessageEvent.SMS_SHOW, showMessage);
			view.init();
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (MessageEvent.SMS_SHOW, showMessage);
		}

		public void showMessage(IEvent evt)
		{
			string messageInfo = evt.data as string;
			string[] data = messageInfo.Split (';');
			string sender = data [0];
			string number = data [1];
			//draw UI
			Debug.Log ("[MessageTitleView] showMessage Sender : " + sender + " Number : " + number);
			view.TitleSender.GetComponent<Text> ().text = sender;
			view.TitlePhoneNumber.GetComponent<Text> ().text = number;

		}
	}
}
