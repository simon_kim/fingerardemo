﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace fingertalk.vr.message
{
	public class MessageTitleView : EventView
	{
		public GameObject TitleBackground;
		public GameObject TitleIcon;
		public GameObject TitleSender;
		public GameObject TitlePhoneNumber;

		public bool focus = false;

		internal void init()
		{
		}

		void Update()
		{
		}

	}
}

