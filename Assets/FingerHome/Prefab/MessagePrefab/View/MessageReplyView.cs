﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main;

namespace fingertalk.vr.message
{
	public class MessageReplyView : EventView
	{
		public GameObject ReplyBackground;
		public GameObject ReplyContent;
		public GameObject ReplySMSButton;
		public Texture[] ButtonTexture;
		public Texture replyBackgroundTexture;

		public Vector3 ON_THE_KEYBOARD = new Vector3(0.0f, 0.0f, 0.0f);

		public bool replyInputFieldFocused = false;
		public bool replySMSButtonFocused = false;

		public string outUIText {
			get { return GameObject.Find ("ReplyMessage").GetComponent<Text>().text; }
			set { GameObject.Find ("ReplyMessage").GetComponent<Text>().text = value; }
		}

		internal void init()
		{
		}

		void Update()
		{
		}

		public void onReplyInputFieldFocusIn() {
			Debug.Log ("onReplyInputFieldFocusIn()");
			replyInputFieldFocused = true;
			//GameObject.Find ("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector> ().AddListener (OnClick);
		}

		public void onReplyInputFieldFocusOut() {
			Debug.Log ("onReplyInputFieldFocusOut()");
			replyInputFieldFocused = false;
			//GameObject.Find ("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector> ().RemoveListener (OnClick);
		}

		public void onReplySMSButtonFocusIn() {
			Debug.Log ("onReplySMSButtonFocusIn()");
			replySMSButtonFocused = true;
			ReplySMSButton.GetComponent<RawImage>().texture = ButtonTexture [1];
			//GameObject.Find ("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector> ().AddListener (SendSMS);
		}

		public void onReplySMSButtonFocusOut() {
			Debug.Log ("onReplySMSButtonFocusOut()");
			replySMSButtonFocused = false;
			ReplySMSButton.GetComponent<RawImage>().texture = ButtonTexture [0];
			//GameObject.Find ("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector> ().RemoveListener (SendSMS);
		}

		public void InitializeReplyInputField()
		{
			Text outUI = GameObject.Find ("ReplyMessage").GetComponent<Text>();
			outUI.text = "메시지를 입력하세요.";
			outUI.color = new Color32(255, 255, 255, 255);
		}

		private void SendSMS(string evt) {
			Debug.Log("SendSMS OnClick is started!!!");
			destroyKeyBoard ();
			string number = GameObject.Find ("PhoneNumber").GetComponent<Text> ().text;
			string content = outUIText;
			sendMessageToPlugin (number + ";" + content);
			drawReplyContent (content);
			InitializeReplyInputField ();
			StartCoroutine ("setScrollBar");
//			StartCoroutine ("hideMessagePrefab");
		}

		private void OnClick(string evt)
		{
			Debug.Log("MessageReplyView OnClick is started!!!");
			if (null == GameObject.Find("Fingertalk") && replyInputFieldFocused) {
				InitializeReplyInputField ();
				showKeyBoard ();
			}

		}

		private void showKeyBoard()
		{
			Debug.Log("John showKeyBoard!!!");
			PrefabAsyncLoader loader = GameObject.FindObjectOfType<PrefabAsyncLoader> ();
			GameObject go = GameObject.Find ("ReplyContent");
			loader.LoadPrefab("Fingertalk", go.transform, (GameObject instance) => {
				instance.name = "Fingertalk";
				Debug.Log ("instance.transform.position.x: " + instance.transform.position.x);
				instance.transform.localPosition = new Vector3(-35, -120, 0f);
				instance.transform.Rotate(new Vector3(0.5f, 45.2f, 0.0f));
				instance.transform.localScale = new Vector3(1f,1f,1f);
			});
		}

		private void destroyKeyBoard ()
		{
			Debug.Log("john destroyKeyBoard!!!");
			GameObject Fingertalk = GameObject.Find ("Fingertalk");
			GameObject.Destroy (Fingertalk);
//			GameObject ReplyContent = GameObject.Find ("ReplyContent");
//			if (ReplyContent.transform.childCount >= 3) {
//				GameObject.Destroy (ReplyContent.transform.GetChild(2));
//			}
		}

		private void sendMessageToPlugin(string data)
		{
			AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
			activity.Call ("deliverSMSToAndroid", data);
		}

		public GameObject frameSmsContents;
		public GameObject frameBackground;
		public GameObject frameMsg;
		public GameObject frameDate;

		public void drawReplyContent(string content) {
			GameObject go = GameObject.Find ("Viewport");

			String sysTime = System.DateTime.Now.ToString ("yyyy-M-d-hh:mm-tt");
			String[] time = sysTime.Split ('-');

			frameSmsContents = new GameObject();
			frameSmsContents.name = "SmsContents";
			frameSmsContents.AddComponent<VerticalLayoutGroup>();
			frameSmsContents.AddComponent<ContentSizeFitter>();

			frameSmsContents.transform.SetParent(go.transform);

			frameBackground = new GameObject();
			frameBackground.name = "MessageContentBackground";
			frameBackground.AddComponent<RawImage>();
			frameBackground.AddComponent<VerticalLayoutGroup>();
			frameBackground.AddComponent<ContentSizeFitter>();

			frameBackground.GetComponent<RawImage> ().texture = replyBackgroundTexture;
			frameBackground.GetComponent<RawImage> ().raycastTarget = true;
			frameBackground.GetComponent<RectTransform> ().localPosition = new Vector3 (0, 0, 0);
			frameBackground.GetComponent<RectTransform> ().localRotation = new Quaternion (0, 0, 0, 0);
			frameBackground.GetComponent<RectTransform> ().sizeDelta = new Vector2 (512f, 105f);
			frameBackground.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			frameBackground.transform.SetParent(frameSmsContents.transform);

			frameMsg = new GameObject();
			frameMsg.name = "MessageContentMessage";
			frameMsg.AddComponent<Text>();
			frameMsg.GetComponent<RectTransform> ().localPosition = new Vector3 (0, 0, 0);
			frameMsg.GetComponent<RectTransform> ().localRotation = new Quaternion (0, 0, 0, 0);
			frameMsg.GetComponent<RectTransform> ().sizeDelta = new Vector2 (412f, 30f);
			frameMsg.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			frameMsg.transform.SetParent(frameBackground.transform);

			frameDate = new GameObject();
			frameDate.name = "MessageContentDate";
			frameDate.AddComponent<Text>();
			frameDate.GetComponent<RectTransform> ().localPosition = new Vector3 (0, 0, 0);
			frameDate.GetComponent<RectTransform> ().localRotation = new Quaternion (0, 0, 0, 0);
			frameDate.GetComponent<RectTransform> ().sizeDelta = new Vector2 (412f, 22f);
			frameDate.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			frameDate.transform.SetParent(frameBackground.transform);

			frameBackground.GetComponent<VerticalLayoutGroup> ().padding.left = 10;
			frameBackground.GetComponent<VerticalLayoutGroup> ().padding.right = 30;
			frameBackground.GetComponent<VerticalLayoutGroup> ().padding.top = 10;
			frameBackground.GetComponent<VerticalLayoutGroup> ().padding.bottom = 10;
			frameBackground.GetComponent<VerticalLayoutGroup> ().childForceExpandHeight = true;
			frameBackground.GetComponent<VerticalLayoutGroup> ().childForceExpandWidth = false;
			frameBackground.GetComponent<VerticalLayoutGroup> ().childAlignment = TextAnchor.MiddleCenter;
			frameBackground.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			frameBackground.GetComponent<ContentSizeFitter> ().horizontalFit = ContentSizeFitter.FitMode.Unconstrained;

			frameMsg.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
			frameMsg.GetComponent<Text> ().supportRichText = false;
			frameMsg.GetComponent<Text> ().raycastTarget = true;
			frameMsg.GetComponent<Text> ().fontSize = 18;
			frameMsg.GetComponent<Text> ().font = Resources.GetBuiltinResource (typeof(Font), "Arial.ttf") as Font;
			frameMsg.GetComponent<Text> ().text = content;

			frameDate.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
			frameDate.GetComponent<Text> ().supportRichText = false;
			frameDate.GetComponent<Text> ().raycastTarget = true;
			frameDate.GetComponent<Text> ().fontSize = 10;
			frameDate.GetComponent<Text> ().font = Resources.GetBuiltinResource (typeof(Font), "Arial.ttf") as Font;
			frameDate.GetComponent<Text> ().text = "\n" + time[0] + "년 " + time[1] + "월 " + time[2] + "일 " + time[3] + " " + time[4];

			frameSmsContents.GetComponent<VerticalLayoutGroup> ().padding.left = 20;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().padding.right = 20;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().padding.top = 30;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().padding.bottom = 30;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().spacing = 30;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().childForceExpandHeight = true;
			frameSmsContents.GetComponent<VerticalLayoutGroup> ().childForceExpandWidth = true;
			frameSmsContents.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			frameSmsContents.GetComponent<ContentSizeFitter> ().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;

			frameSmsContents.GetComponent<RectTransform> ().position = new Vector3 (70f, -30f, 0);

			if (go.transform.childCount <= 1) {
				frameSmsContents.GetComponent<RectTransform> ().localPosition = new Vector3 (-65f, 30f, 0f);
				frameSmsContents.GetComponent<RectTransform> ().Rotate(new Vector3(0.5f, 45.0f, 1.7f));
				frameSmsContents.GetComponent<RectTransform> ().localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
				frameSmsContents.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			} else {
				frameSmsContents.GetComponent<RectTransform> ().localPosition = new Vector3 (290f,-75f*(go.transform.childCount), 0f);
				frameSmsContents.GetComponent<RectTransform> ().Rotate(new Vector3(0.5f, 45.0f, 1.7f));
				frameSmsContents.GetComponent<RectTransform> ().localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
				frameSmsContents.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			}

		}

		private Scrollbar scrollBar;

		public IEnumerator setScrollBar() {
			yield return new WaitForSeconds(0.0f);
			GameObject scrollBar = GameObject.Find ("ContentsScrollBar");
			if (null != scrollBar) {
				Scrollbar bar = scrollBar.GetComponent<Scrollbar> ();
				bar.value = 0f;
			}
		}

//		public IEnumerator hideMessagePrefab() {
//			yield return new WaitForSeconds(2.0f);
//			GameObject.DestroyObject (GameObject.Find ("SmsContents").transform.GetChild (0));
//			GameObject.DestroyObject (GameObject.Find ("SmsContents").transform.GetChild (1));
//			InitializeReplyInputField ();
//			GameObject.Find ("MessageIcon").SetActive (false);
//			GameObject.Find ("MessageContainerView").SetActive(false);
//			GvrViewer.Instance.Recenter ();

//		}
	}
}

