﻿using UnityEngine;
using System.Collections;

public class FingerCircle : MonoBehaviour {

    public GameObject focus;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnPointerEnter()
    {
        focus.SetActive(true);
    }

    public void OnPointerExit()
    {
        focus.SetActive(false);
    }
}
