﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.search.events;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;
using com.fingertalk.vrhome.main;
using com.fingertalk.vrhome.main.events;

namespace com.fingertalk.vrhome.webview
{
    using UnityEngine;
    using UnityEngine.EventSystems;
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;

    [RequireComponent(typeof(Renderer))]
    public class WebviewPlaneView : TextEventView, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
    {

        #region FingerTalk Integration
        enum PointerMode { GAZE, CONTROLLER };

		public string _homeURL = "http://www.naver.com";

        FingerTalkPluginConnector fingerTalkPluginConnector;
        //GvrReticlePointer fingerGaze;
        //GvrLaserPointer fingerController;
        PointerMode controlMode;

        public GameObject finger3x4;

		public void ResetAnimation()
		{
			StartCoroutine(DelayExit());
		}

		public void StartAnimation()
		{
			finger3x4.SetActive (true);
			finger3x4.GetComponent<Animator> ().SetBool ("isStart", true);
		}

		private IEnumerator DelayExit()
		{
			finger3x4.GetComponent<Animator> ().SetBool ("isBackHome", true);
			yield return new WaitForSeconds(0.5f);
			finger3x4.SetActive (false);
		}

		internal void init(IEventDispatcher dispatcher)
        {
			this.dispatcher = dispatcher;
			fingerTalkPluginConnector = GameObject.Find("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector>();
            setControlMode();
        }

        // Determine control mode
        private void setControlMode()
        {
            GameObject controllerGO = GameObject.Find("GvrControllerPointer");
            if (controllerGO != null)
            {
                controlMode = PointerMode.CONTROLLER;
                //fingerController = controllerGO.GetComponentInChildren<GvrLaserPointer>();
            }
            else
            {
                controlMode = PointerMode.GAZE;
                //fingerGaze = GameObject.Find("GvrReticlePointer").GetComponent<GvrReticlePointer>();
            }
        }

        public void OnEnter()
        {
            Debug.Log("[WebviewPlaneView] OnEnter");
            //fingerTalkPluginConnector.AddListener(OnClick);
        }

        public void OnExit()
        {
            Debug.Log("[WebviewPlaneView] OnExit");
            //fingerTalkPluginConnector.RemoveListener(OnClick);
        }

        public void OnClick(String evt)
        {
            Vector2 point = getCurrentPoint();
            PositionInformation(point, ActionEvent.Down);
            PositionInformation(point, ActionEvent.Up);
        }

		public void DestroyKeyboard() 
		{
			GameObject keyboard = GameObject.Find("Fingertalk") as GameObject;
			GameObject.Destroy(keyboard);
		}

        private bool _isFingerDown = false;
        private Vector2 _lastFingerPosition;

		public void GoFingerTalkMain()
		{
			ResetAnimation ();
			LoadUrl(_homeURL);
			dispatcher.Dispatch(MainEvent.GO_HOME);
		}

		public void GoWebviewMain()
		{
			LoadUrl(_homeURL);
		}

        public void ZoomInWebview()
        {
            Transform transform = GameObject.Find("WebviewRoot").transform;
            transform.localScale = transform.localScale + new Vector3(0.001f, 0.001f, 0.001f);
        }

        public void ZoomOutWebview()
        {
            Transform transform = GameObject.Find("WebviewRoot").transform;
            transform.localScale = transform.localScale - new Vector3(0.001f, 0.001f, 0.001f);
        }

        public void OnFingerDown()
        {
            _isFingerDown = true;
            _lastFingerPosition = getCurrentPoint();
            //PositionInformation(lastPosition, ActionEvent.Down);
        }

        public void OnFingerUp()
        {
            _isFingerDown = false;
            if (isScrolling)
            {
                OnEndFingerDrag();
            } else { 
                PositionInformation(getCurrentPoint(), ActionEvent.Up);
            }
        }

        public void OnBeginFingerDrag()
        {
            isScrolling = true;
            PositionInformation(_lastFingerPosition, ActionEvent.Down);
        }

        public void OnFingerDrag()
        {
            isScrolling = true;
            PositionInformation(_lastFingerPosition, ActionEvent.Move);
        }

        public void OnEndFingerDrag()
        {
            PositionInformation(_lastFingerPosition, ActionEvent.Up);
            isScrolling = false;
        }

        public Vector2 getCurrentPoint()
        {
            int width = 2560;
            int height = 2560;

            Vector3 worldPosition;

            switch (controlMode)
            {
                case PointerMode.GAZE:
                    //worldPosition = fingerGaze.lastPosition;
                    break;
                case PointerMode.CONTROLLER:
                    //worldPosition = fingerController.reticle.transform.position;
                    break;
                default:
                    worldPosition = new Vector3(0, 0, 0);
                    break;
            }
            /*
            Vector3 position = transform.InverseTransformPoint(worldPosition);
            float x = ((position.x + 5) / 10) * width;
            float y = height - ((position.z + 5) / 10) * height;
            return new Vector2(x, y);
            */
            return new Vector2(0, 0);
        }

        void FingerUpdate()
        {
            _lastFingerPosition = getCurrentPoint();
            if (_isFingerDown)
            {
                if (isScrolling == false)
                {
                    OnBeginFingerDrag();
                }
                else
                {
                    OnFingerDrag();
                }
            }
        }
        #endregion

        /**
         * Default URL to load, you can load a new URL with LoadUrl(String url)
         */
        public string url = "https://www.google.com/";

        /**
         * Default Texture size
         */
        public int textureWidth = 640;
        public int textureHeight = 640;

        /**
         * This is only to show that delegation and events works
         * You can remove this :)
         */
        public TextMesh statusText = null;
        public TextMesh eventText = null;

        /**
         * Get more debug message in the Android logcat
         */
        public bool isDebug = false;

        // Internals objects

        protected Texture2D nativeTexture = null;
        protected IntPtr nativeTexId = IntPtr.Zero;
        protected AndroidJavaObject view = null;

        /**
         * You need a Renderer with a texture (best result with unlit texture)
         */
        protected Renderer mainRenderer = null;

        public enum ActionEvent
        {
            Down = 1,
            Up = 2,
            Move = 3,
            Max
        }

        protected enum SurfaceEvent
        {
            Init = 0,
            Stop = 1,
            Update = 2,
            Max
        };

        // Configuration

        /**
         * Set to true if you want to send event to the Native Webview
         * Related to PositionInformation(Vector2, ActionEvent)
         */
        protected bool propagateHitEvent = true;

        // State

        protected bool isClickedDown = false;
        protected bool isScrolling = false;
        protected Vector2 lastPosition = Vector2.zero;

        // Unity life cycle

        /**
         * Used to configure the renderer and the external texture
         */
        void Awake()
        {
            JRMGX_Android_View_Init();

            mainRenderer = GetComponent<Renderer>();
            if (mainRenderer.material == null)
            {
                Debug.LogError("No material for surface");
            }
            else if (mainRenderer.material.mainTexture == null)
            {
                Debug.LogError("No material.mainTexture for surface");
            }

            nativeTexture = Texture2D.CreateExternalTexture(textureWidth, textureHeight, TextureFormat.RGBA32, true, false, IntPtr.Zero);

            IssuePluginEvent(SurfaceEvent.Init);
        }

        /**
         * Start the rendering of the Webview
         */
        void Start()
        {
            StartCoroutine(DelayedStart());
        }

        /**
         * Check each frame if the Webview has a new update
         */
        void Update()
        {
            IntPtr currTexId = JRMGX_Android_View_GetTexture();
            if (currTexId != nativeTexId)
            {
                nativeTexId = currTexId;
                nativeTexture.UpdateExternalTexture(currTexId);
                Debug.Log("AndroidView - texture size is " + nativeTexture.width + " x " + nativeTexture.height);
            }
            IssuePluginEvent(SurfaceEvent.Update);
        }

        /**
         * Ask the Webview to consume waiting event
         */
        void LateUpdate()
        {
            if (propagateHitEvent == false) return;

            if (view != null)
            {
                view.Call("eventPoll");
                FingerUpdate();
            }
        }

        void OnApplicationQuit()
        {
            IssuePluginEvent(SurfaceEvent.Stop);
        }

        // Public interface

		public void InitializeSearchInputField()
		{
			Text outUI = GetComponentInChildren<Text>();
			outUI.text = "";
			outUI.color = new Color32(0, 0, 0, 255);
		}

        /**
         * Webview method goBack
         * Load the previous page if possible
         */

		public void SearchKeyword()
		{
			DestroyKeyboard ();
			view.Call ("searchKeyword");
		}

        public void GoBack()
        {
            view.Call("goBack");
        }

        /**
         * Webview method goForward
         * Load the next page if possible
         */
        public void GoForward()
        {
            view.Call("goForward");
        }

        /**
         * Webview method loadUrl
         * Load a new Url in the webview
         */
        public void LoadUrl(String newUrl)
        {
            url = newUrl;
            if (!Application.isEditor) {
                GameObject.Find("WebviewURL").GetComponent<Text>().text = newUrl;
                view.Call("loadUrl", newUrl);
            }
        }

        public void SetText(String text)
        {
			view.Call("setText", text);
        }

		public void SetScrollUp()
		{
			view.Call("setScrollUp");
		}

		public void SetScrollDown()
		{
			view.Call("setScrollDown");
		}

		public void SetZoomIn()
		{
			view.Call("setZoomIn");
		}

		public void SetZoomOut()
		{
			view.Call("setZoomOut");
		}
        // Delegation

        /**
         * The Webview will give you delegate messages for specific events
         * message:
         *  - onPageStarted:string   <-- when a new url is about to be loaded
         *  - onPageFinished:string  <-- when a new url has finished to load
         *  - onReceivedError:string <-- when an error happen
         * you need to parse these messages and handle it as you wish
         */
        protected void Delegation(string message)
        {
            // Do something here with `message`
            // onPageStarted:http://www.naver.com
            // onPageFinished:http://www.naver.com
            if (statusText != null)
            {
                statusText.text = message;
            }
            if (message.Contains("onPageStarted:"))
            {
                GameObject.Find("WebviewURL").GetComponent<Text>().text = message.Replace("onPageStarted:", "");
            }
            
        }

        /**
         * Debug method, can be removed
         */
        protected void PrintEvent(string message)
        {
            if (eventText == null) return;
            eventText.text = message;
        }

        // Sending event

        /**
         * This is how you can send `Move` action to the Webview
         * position is the final pointer position on the underlying 2D Webview
         * It is android coordinate system: top,left = 0,0 / bottom,right = textureHeight,textureWidth
         * See the `Event system` section below for an example
         */
        public void PositionInformation(Vector2 position, ActionEvent actionType)
        {
            if (propagateHitEvent == false) return;
            if (view != null)
            {
                // This is for debugging only you should remove it
                Debug.Log("[PositionInformation] event " + actionType.ToString() + " sent: " + position.ToString());
                view.Call("eventAdd", (int)position.x, (int)position.y, (int)actionType);
            }
        }

        // Event system

        /**
         * Convert an Unity `PointerEventData` position to the webview coordinate position
         * This needs to be adapted for you scene
         */
        public void UpdateLastPosition(PointerEventData eventData)
        {
            // This needs to be adapted for you scene
            // It depends on how you transformed your GameObject
            // You will have try to adapt +/- and choose from x/y/z on position
            Vector3 position = transform.InverseTransformPoint(eventData.pointerCurrentRaycast.worldPosition);
            //Debug.Log (position); // Use debug.log and adb logcat to find out
            float x = ((position.x + 5) / 10) * textureWidth;
            float y = ((position.z - 5) / -10) * textureHeight;

            lastPosition = new Vector2(x, y);
        }

        #region IPointerExitHandler implementation

        /**
         * When the pointer get out of the GameObject we considerate that the user did an ActionEvent.Up
         */
        public void OnPointerExit(PointerEventData eventData)
        {
            isClickedDown = false;
            UpdateLastPosition(eventData);
            PositionInformation(lastPosition, ActionEvent.Up);
        }

        #endregion
        #region IPointerClickHandler implementation

        /**
         * Example of event system implementation
         */
        public void OnPointerClick(PointerEventData eventData)
        {
            if (isScrolling) return; // Skip the click event if we are scrolling
            UpdateLastPosition(eventData);
            PositionInformation(lastPosition, ActionEvent.Down);
            PositionInformation(lastPosition, ActionEvent.Up);
        }

        #endregion
        #region IEndDragHandler implementation

        public void OnEndDrag(PointerEventData eventData)
        {
            UpdateLastPosition(eventData);
            PositionInformation(lastPosition, ActionEvent.Up);
            isScrolling = false;
        }

        #endregion
        #region IDragHandler implementation

        public void OnDrag(PointerEventData eventData)
        {
            isScrolling = true;
            UpdateLastPosition(eventData);
            PositionInformation(lastPosition, ActionEvent.Move);
        }

        #endregion
        #region IBeginDragHandler implementation

        public void OnBeginDrag(PointerEventData eventData)
        {
            isScrolling = true;
            UpdateLastPosition(eventData);
            PositionInformation(lastPosition, ActionEvent.Down);
        }

        #endregion
        #region IGvrPointerHoverHandler implementation

        /**
         * Example of event system implementation for Google Daydream 
         * This will conflict with Unity event handle above, please remove them.
         *
         * This method is an example and works only with the Google Daydream asset installed (IGvrPointerHoverHandler)
         * if you don't use Google Daydream you may achieve the same result with classic Unity event handler (above)
         */
        public void OnGvrPointerHover(PointerEventData eventData)
        {
            /*
            UpdateLastPosition (eventData);
            // If you use the Google Daydream asset, you could do something like that
            if (GvrController.ClickButtonDown) {
                isClickedDown = true;
                PositionInformation (lastPosition, ActionEvent.Down);
            } 
            else if (GvrController.ClickButtonUp) {
                isClickedDown = false;
                PositionInformation (lastPosition, ActionEvent.Up);
            }
            if (isClickedDown) {
                PositionInformation (lastPosition, ActionEvent.Move);
            }
            */
        }

        #endregion

        // Internal to plugin
        // Please do not change anything from here

        protected IEnumerator DelayedStart()
        {
            yield return null; // skip 1 frame to allow Init from the plugin

            view = StartOnTextureId(textureWidth, textureHeight);
            mainRenderer.material.mainTexture = nativeTexture;
        }

        protected AndroidJavaObject StartOnTextureId(int texWidth, int texHeight)
        {

            JRMGX_Android_View_SetTextureDetails(textureWidth, textureHeight);

            IntPtr androidSurface = JRMGX_Android_View_GetObject();
			AndroidJavaObject javaObject = new AndroidJavaObject("com.fingertalk.home.web.JohnWebView", texWidth, texHeight);
            IntPtr setSurfaceMethodId = AndroidJNI.GetMethodID(javaObject.GetRawClass(), "setSurface", "(Landroid/view/Surface;)V");
            jvalue[] parms = new jvalue[1]; parms[0] = new jvalue(); parms[0].l = androidSurface;

            try
            {
                AndroidJNI.CallVoidMethod(javaObject.GetRawObject(), setSurfaceMethodId, parms);
                javaObject.Call("setDebug", isDebug);
                javaObject.Call("loadUrl", url);
                javaObject.Call("setDelegate", gameObject.name, "Delegation");
            }
            catch (Exception e)
            {
                Debug.Log("AndroidView Failed to start webview with message " + e.Message);
            }

            return javaObject;
        }

        protected static void IssuePluginEvent(SurfaceEvent eventType)
        {
            GL.IssuePluginEvent((int)eventType);
        }

        // These are the native methods imported from the plugin 
        [DllImport("JrmgxAndroidView")]
        protected static extern void JRMGX_Android_View_Init();

        [DllImport("JrmgxAndroidView")]
        protected static extern IntPtr JRMGX_Android_View_GetObject();

        [DllImport("JrmgxAndroidView")]
        protected static extern IntPtr JRMGX_Android_View_GetTexture();

        [DllImport("JrmgxAndroidView")]
        protected static extern void JRMGX_Android_View_SetTextureDetails(int texWidth, int texHeight);
    }
}