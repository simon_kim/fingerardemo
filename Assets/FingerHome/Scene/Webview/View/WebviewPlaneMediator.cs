﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.search.events;
using fingertalk.vr.keyboard;
using fingertalk.vr.message;
using com.fingertalk.vrhome.main;


namespace com.fingertalk.vrhome.webview
{
    public class WebviewPlaneMediator : TextEventMediator
    {
        [Inject]
        public WebviewPlaneView view { get; set; }

		public override void OnRegister()
        {
			base.OnRegister ();
            dispatcher.AddListener(MainEvent.SHOW_SCENE, onShow);
            dispatcher.AddListener(MainEvent.HIDE_SCENE, onHide);

			SetTextEventView (view);
			view.init(dispatcher);
        }

        public override void OnRemove()
        {
			base.OnRemove ();
        }

		protected override void onTextComeplete()
		{
			base.onTextComeplete ();
			view.InitializeSearchInputField ();
			view.SearchKeyword ();
		}

		protected override void onTextChange(IEvent evt)
		{
			base.onTextChange (evt);
			view.SetText (view.OutUIText);
		}

		protected override void onTextDelete()
		{
			base.onTextDelete ();
			view.SetText (view.OutUIText);
		}

        private void onShow(IEvent evt)
        {
            if (evt.data.Equals(contextView.scene.name))
            {
                Debug.Log("[WebviewPlaneMediator] onShow");
				//GameObject.Find ("Finger3x4").SetActive (true);
				view.StartAnimation ();
                dispatcher.AddListener(MainEvent.KEY_DOWN, onKeyDown);
                dispatcher.AddListener(MainEvent.KEY_UP, onKeyUp);
            }
        }

        private void onHide(IEvent evt)
        {
            if (evt.data.Equals(contextView.scene.name))
            {
                Debug.Log("[WebviewPlaneMediator] onHide");
				//GameObject.Find ("Finger3x4").SetActive (false);
				view.ResetAnimation ();
                dispatcher.RemoveListener(MainEvent.KEY_DOWN, onKeyDown);
                dispatcher.RemoveListener(MainEvent.KEY_UP, onKeyUp);
            }
        }

        private void onKeyUp(IEvent evt)
        {
            Debug.Log("[WebviewPlaneMediator] onKeyUp");
            switch ((KeyCode)evt.data)
            {
                case KeyCode.Alpha1:
                case KeyCode.Alpha2:
                case KeyCode.Alpha3:
                case KeyCode.Alpha4:
                case KeyCode.Alpha7:
                case KeyCode.Alpha8:
                case KeyCode.Alpha9:
                    break;
                default:
					view.OnFingerUp();
                    break;
            }
        }

        private void onKeyDown(IEvent evt)
        {
            Debug.Log("[WebviewPlaneMediator] onKeyDown");
            switch ((KeyCode)evt.data)
            {
                case KeyCode.Alpha1:
					view.ResetAnimation ();
                    view.GoFingerTalkMain();
                    break;
                case KeyCode.Alpha2:
                    view.GoWebviewMain();
                    break;
                case KeyCode.Alpha3:
					view.ResetAnimation ();
                    view.GoFingerTalkMain();
                    break;
                case KeyCode.Alpha4:
                    view.GoBack();
                    break;
				case KeyCode.Alpha5:
					view.GoForward();
					break;
				case KeyCode.Alpha7:
					view.SetScrollUp ();
                    break;
                case KeyCode.Alpha8:
					view.ZoomInWebview();
                    break;
                case KeyCode.Alpha9:
                    break;
				case KeyCode.F1:
					view.SetScrollDown ();
					break;
				case KeyCode.Alpha0:
					view.ZoomOutWebview();
					break;
	            case KeyCode.F5:
				case KeyCode.F6:
				case KeyCode.F3:
				default :
				view.OnClick ("Click");
				break;
            }
        }

	}
}