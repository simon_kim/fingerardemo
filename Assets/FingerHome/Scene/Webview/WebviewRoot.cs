﻿using System;
using UnityEngine;
using strange.extensions.context.impl;

namespace com.fingertalk.vrhome.webview
{
    public class WebviewRoot : ContextView
    {
        void Awake()
        {
            context = new WebviewContext(this);
        }
    }
}

