﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace com.fingertalk.vrhome.webview
{
    public class WebviewContext : MVCSContext
    {
        public WebviewContext(MonoBehaviour view) : base(view)
        {
        }

        public WebviewContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
            mediationBinder.Bind<WebviewPlaneView>().To<WebviewPlaneMediator>();

            commandBinder.Bind(ContextEvent.START).To<StartWebviewCommand>().Once();
            /*
            injectionBinder.Bind<IResultContainerModel>().To<ResultContainerModel>().ToSingleton();
            injectionBinder.Bind<ISearchService>().To<SearchService>().ToSingleton();

            commandBinder.Bind(ContextEvent.START).To<startSearchCommand>().Once();
            */
        }
    }
}

