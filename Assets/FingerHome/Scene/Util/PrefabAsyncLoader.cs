﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using strange.extensions.dispatcher.eventdispatcher.api;
using com.fingertalk.vrhome.movieplayer.events;

namespace com.fingertalk.vrhome.main
{
	public class PrefabAsyncLoader : MonoBehaviour 
	{
		public IEventDispatcher dispatcher { get; set; }

		public void init(IEventDispatcher dispatcher)
		{
			this.dispatcher = dispatcher;
		}
	
		public void LoadPrefab(string name, Transform parent, Action<GameObject> action)
		{
			StartCoroutine (LoadPrefabAsync(name, parent, action));
		}

		IEnumerator LoadPrefabAsync(string name, Transform parent, Action<GameObject> action)
		{
			ResourceRequest async = Resources.LoadAsync (name, typeof(GameObject));
		
			while (!async.isDone) {
				yield return null;
			}

			GameObject instance = MonoBehaviour.Instantiate (async.asset, parent) as GameObject;

			action (instance);
		}
	}
}
