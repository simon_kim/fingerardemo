﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.dispatcher.eventdispatcher.api;
using com.fingertalk.vrhome.movieplayer.events;

namespace com.fingertalk.vrhome.main
{
	public class SceneAsyncLoader : MonoBehaviour 
	{
		public IEventDispatcher dispatcher { get; set; }

		public void init(IEventDispatcher dispatcher)
		{
			this.dispatcher = dispatcher;
		}

		public void LoadScene(string name, Action action)
		{
			StartCoroutine (LoadSceneAsync(name, action));
		}

		IEnumerator LoadSceneAsync(string sceneName, Action action)
		{
			AsyncOperation async = SceneManager.LoadSceneAsync (sceneName, LoadSceneMode.Additive);

			while (!async.isDone) {
				yield return null;
			}

			action();
		}
	}
}