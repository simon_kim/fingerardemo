﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.search.events;
using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.search
{
    public class SearchContext : MVCSContext
    {
        public SearchContext(MonoBehaviour view) : base(view)
        {
        }

        public SearchContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
			injectionBinder.Bind<IResultContainerModel>().To<ResultContainerModel>().ToSingleton();
			injectionBinder.Bind<ISearchService> ().To<SearchService> ().ToSingleton ();

			mediationBinder.Bind<SearchView>().To<SearchMediator>();
			mediationBinder.Bind<ResultContainerView>().To<ResultContainerMediator>();
            mediationBinder.Bind<ResultItemView>().To<ResultItemMediator>();

            commandBinder.Bind (ContextEvent.START).To<startSearchCommand>().Once();
			commandBinder.Bind (SearchEvent.SELECT_MOVIE).To<SelectMovieCommand>();
			commandBinder.Bind (SearchEvent.SHOW_KEYBOARD).To<ShowKeyboardCommand>();
			commandBinder.Bind (SearchEvent.DESTROY_KERYBOARD).To<DestoryKeyboardCommand>();
			commandBinder.Bind (SearchEvent.SEARCH).To<SearchCommand>();
        }
    }
}

