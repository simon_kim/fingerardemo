﻿using System;
using UnityEngine;
using strange.extensions.context.impl;

namespace com.fingertalk.vrhome.search
{
    public class SearchRoot : ContextView
    {
        void Awake()
        {
            context = new SearchContext(this);
        }
    }
}

