﻿using System;
using UnityEngine;
using System.Collections;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace com.fingertalk.vrhome.search
{
	public class ThumbnailAsyncLoader : MonoBehaviour {

		public IEventDispatcher dispatcher { get; set; }

		public void init(IEventDispatcher dispatcher)
		{
			this.dispatcher = dispatcher;
		}

		public void LoadThumbnail(ResultItemModel model, Action action)
		{
			StartCoroutine (LoadThumbnailAsync(model, action));
		}

		IEnumerator LoadThumbnailAsync(ResultItemModel model, Action onLoadComplete)
		{
			WWW www = new WWW (model.thumbnailUrl);

			yield return www;

			model.thumbnailTexture = www.texture;

			onLoadComplete ();
		}
	}
}
