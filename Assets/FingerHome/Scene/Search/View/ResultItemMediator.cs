﻿/// Example mediator
/// =====================
/// Make your Mediator as thin as possible. Its function is to mediate
using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.search.events;
using com.fingertalk.vrhome.main.events;

namespace com.fingertalk.vrhome.search
{
    public class ResultItemMediator : EventMediator
    {
        [Inject]
        public ResultItemView view { get; set; }

        public override void OnRegister()
        {
			view.init();
			dispatcher.AddListener(MainEvent.KEY_DOWN, onItemSelected);
            dispatcher.AddListener(MainEvent.SHOW_SCENE, onShow);
            dispatcher.AddListener(MainEvent.HIDE_SCENE, onHide);
            view.dispatcher.AddListener (ResultItemView.LOAD_THUMBNAIL, onLoadThumbnail);
        }

        public override void OnRemove()
        {
			dispatcher.RemoveListener(MainEvent.KEY_DOWN, onItemSelected);
            dispatcher.RemoveListener(MainEvent.SHOW_SCENE, onShow);
            dispatcher.RemoveListener(MainEvent.HIDE_SCENE, onHide);
            view.dispatcher.AddListener (ResultItemView.LOAD_THUMBNAIL, onLoadThumbnail);
        }

        private void onShow(IEvent evt)
        {
            if (evt.data.Equals(contextView.scene.name))
            {
                
            }
        }

        private void onHide(IEvent evt)
        {
            if (evt.data.Equals(contextView.scene.name))
            {
                view.OnFocusOut();
            }
         }

        void onItemSelected()
        {
            if (view.focus) {
				dispatcher.Dispatch(SearchEvent.SELECT_MOVIE, view.item.videoId);
				view.focus = false;
            }
        }

		void onLoadThumbnail()
		{
			view.GetComponentInParent<ThumbnailAsyncLoader>().LoadThumbnail(view.item, view.DrawThumbnail);
		}
    }
}

