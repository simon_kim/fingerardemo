﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.search.events;
using fingertalk.vr.keyboard;
using fingertalk.vr.message;
using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.search
{
    public class SearchMediator : TextEventMediator
    {
        [Inject]
        public SearchView searchView { get; set; }

		[Inject]
		public IResultContainerModel model { get; set; }

		public bool isKeyboardShow = false;

        public override void OnRegister()
		{
			base.OnRegister ();
            dispatcher.AddListener (MainEvent.KEY_DOWN, onClick);
			dispatcher.AddListener (MainEvent.KEY_DOWN, onSearchClick);
			dispatcher.AddListener (MainEvent.KEY_DOWN, onHomeClick);

			SetTextEventView (searchView);
			searchView.init();
        }

        public override void OnRemove()
		{
			base.OnRemove ();
            dispatcher.RemoveListener (MainEvent.KEY_DOWN, onClick);
			dispatcher.RemoveListener (MainEvent.KEY_DOWN, onSearchClick);
        }

        protected override void onTextComeplete()
        {
			base.onTextComeplete ();
            searchVideo();
        }

		private void onClick()
		{
			if (!isKeyboardShow && searchView.inputFieldFocused) 
			{
				isKeyboardShow = true;

				if (model.resultItemModel != null) {
					// re-search
					dispatcher.Dispatch(ResultContainerEvent.PUT_ASIDE);
				}
					
				searchView.EnableSearchIcon (true);

				searchView.SetPosition (searchView.ON_THE_KEYBOARD);

				dispatcher.Dispatch(SearchEvent.SHOW_KEYBOARD);

				searchView.EnableInputAssist ();
			}
		}

		private void onSearchClick()
		{
			if (searchView.searchIconFocused)
			{
                searchVideo();
            }
		}

		private void onHomeClick()
		{
			if (searchView.homeIconFocused)
			{
				searchView.OnHomeIconFocusOut();
				dispatcher.Dispatch(MainEvent.GO_HOME);
			}
		}

        private void searchVideo()
        {
            isKeyboardShow = false;

            if (model.resultItemModel != null)
            {
                // re-search
                dispatcher.Dispatch(ResultContainerEvent.CLEAR_ITEMS);
                dispatcher.Dispatch(ResultContainerEvent.BRING_BACK);
            }

            searchView.EnableSearchIcon(false);
			Debug.Log ("searchVideo is called!!!");
            searchView.SetPosition(searchView.ON_THE_RESULT);

            dispatcher.Dispatch(SearchEvent.SEARCH, searchView.OutUIText);
            dispatcher.Dispatch(SearchEvent.DESTROY_KERYBOARD);
        }
    }
}