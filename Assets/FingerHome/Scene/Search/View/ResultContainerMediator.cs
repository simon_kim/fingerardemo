﻿using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.search.events;
using fingertalk.vr.keyboard;
using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.search
{
	public class ResultContainerMediator : EventMediator
	{
		[Inject]
		public ResultContainerView view { get; set; }

		[Inject]
		public IResultContainerModel model { get; set; }

		public override void OnRegister()
		{
			dispatcher.AddListener (SearchEvent.SEARCH_COMPLETE, onSearchComplete);
			dispatcher.AddListener (ResultContainerEvent.PUT_ASIDE, onPutAside);
			dispatcher.AddListener (ResultContainerEvent.BRING_BACK, onBringBack);
			dispatcher.AddListener (ResultContainerEvent.CLEAR_ITEMS, onClearItems);

			view.init();
			view.GetComponent<ThumbnailAsyncLoader> ().init (dispatcher);
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (SearchEvent.SEARCH_COMPLETE, onSearchComplete);
			dispatcher.RemoveListener (ResultContainerEvent.PUT_ASIDE, onPutAside);
			dispatcher.RemoveListener (ResultContainerEvent.BRING_BACK, onBringBack);
			dispatcher.RemoveListener (ResultContainerEvent.CLEAR_ITEMS, onClearItems);
		}

		private void onSearchComplete()
		{
			float offset = 2.0f;

			foreach (ResultItemModel item in model.resultItemModel) {

				GameObject instance = MonoBehaviour.Instantiate (Resources.Load ("ResultItem", typeof(GameObject))) as GameObject;

				ResultItemView itemView = instance.GetComponent<ResultItemView> ();
				itemView.transform.SetParent (view.transform);
				itemView.updateUI (item);

                instance.transform.position = new Vector3(0, offset, 0);
                offset = offset - 0.6f;
                instance.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            }
		}

		private void onPutAside()
		{
			view.transform.RotateAround(GameObject.FindGameObjectWithTag("MainCamera").transform.position, Vector3.up, -60.0f);
		}

		private void onBringBack()
		{
			view.transform.RotateAround(GameObject.FindGameObjectWithTag("MainCamera").transform.position, Vector3.up, 60.0f);
		}

		private void onClearItems()
		{
			ResultItemView[] items = view.gameObject.GetComponentsInChildren<ResultItemView> ();

			for (int i = 0; i < items.Length; i++)
				GameObject.Destroy (items[i].gameObject);

			model.resultItemModel = null;
		}
	}
}