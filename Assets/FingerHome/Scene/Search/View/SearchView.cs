﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.search.events;
using com.fingertalk.vrhome.main;
using UnityEngine.EventSystems;

namespace com.fingertalk.vrhome.search
{
	public class SearchView : TextEventView
    {
		public Vector3 ON_THE_STARTING_POINT = new Vector3(0.0f, 0.0f, 0.0f);
		public Vector3 ON_THE_KEYBOARD = new Vector3(0.0f, 1.0f, 0.0f);
		public Vector3 ON_THE_RESULT = new Vector3(0.0f, 1.0f, 0.0f);
        public GameObject highlight;
		public GameObject homeIcon;

		public bool searchIconFocused = false;
		public bool homeIconFocused = false;
			
		internal void init()
        {
			InitializeSearchInputField ();
        }
			
		public void OnHomeIconFocusIn() 
		{
			homeIcon.transform.localScale += new Vector3 (20f, 20f, 0);
			homeIconFocused = true;
		}

		public void OnHomeIconFocusOut() 
		{
			homeIcon.transform.localScale += new Vector3 (-20f, -20f, 0);
			homeIconFocused = false;
		}

		public void OnSearchButtonFocusIn() 
		{
			//searchIconFocused = true;
            highlight.SetActive(true);
            base.OnFocusIn();
		}

		public void OnSearchButtonFocusOut() 
		{
            //searchIconFocused = false;
            highlight.SetActive(false);
            base.OnFocusOut();
        }

		public void InitializeSearchInputField()
		{
			Text outUI = GetComponentInChildren<Text>();
			outUI.text = "";
			outUI.color = new Color32(0, 0, 0, 255);
		}

		public void EnableSearchIcon(bool enabled)
		{
			RawImage [] searchButton = GetComponentsInChildren<RawImage>();
			searchButton[1].raycastTarget = enabled;
		}

		public void SetPosition(Vector3 position)
		{
			gameObject.transform.position = position;
		}
    }
}