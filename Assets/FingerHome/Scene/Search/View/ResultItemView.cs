﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace com.fingertalk.vrhome.search
{
	public class ResultItemView : EventView
    {
		public GameObject Thumbnail;
		public GameObject Title;
		public GameObject Description;
		public GameObject Etc;
        public GameObject Highlight;

		public bool focus = false;
		public bool isLoadComplete = false;

		internal const string LOAD_THUMBNAIL = "LOAD_THUMBNAIL";

		public ResultItemModel item;

		internal void init()
        {
			EventTrigger trigger = gameObject.AddComponent<EventTrigger>( );

			EventTrigger.Entry pointerEnter = new EventTrigger.Entry( );
			pointerEnter.eventID = EventTriggerType.PointerEnter;
			pointerEnter.callback.AddListener( (data) => OnFocusIn() );
			trigger.triggers.Add( pointerEnter );

			EventTrigger.Entry pointerExit = new EventTrigger.Entry( );
			pointerExit.eventID = EventTriggerType.PointerExit;
			pointerExit.callback.AddListener( (data) => OnFocusOut() );
			trigger.triggers.Add( pointerExit );
        }

        public void OnFocusIn()
        {
            focus = true;
            Highlight.SetActive(true);
        }

        public void OnFocusOut()
        {
            focus = false;
            Highlight.SetActive(false);
        }

        public void updateUI(ResultItemModel item)
		{
			this.item = item;

			//draw UI
			Title.GetComponent<Text>().text = item.title;
			Description.GetComponent<Text> ().text = item.description;
			Etc.GetComponent<Text> ().text = item.etc;

			//load Thumbnail
			StartCoroutine("LoadThumbnailAsync");
		}

		IEnumerator LoadThumbnailAsync()
		{
			yield return new WaitForSeconds (0.0f);
			dispatcher.Dispatch(ResultItemView.LOAD_THUMBNAIL);
		}

		public void DrawThumbnail()
		{
			isLoadComplete = true;
			Thumbnail.transform.rotation = Quaternion.identity;
			Thumbnail.GetComponent<RawImage>().texture = item.thumbnailTexture;
		}

		void Update()
		{
			if (!isLoadComplete)
				Thumbnail.transform.Rotate (0, 0, -5);
		}
    }
}

