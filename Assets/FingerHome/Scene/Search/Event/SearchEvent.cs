﻿using System;

namespace com.fingertalk.vrhome.search.events
{
	public enum SearchEvent {
		SELECT_MOVIE,
		SHOW_KEYBOARD,
		DESTROY_KERYBOARD,
		SEARCH,
		SEARCH_COMPLETE
	}
}

