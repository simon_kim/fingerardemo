﻿using UnityEngine;
using System.Collections;

namespace com.fingertalk.vrhome.search.events
{
	public enum ResultContainerEvent{
		PUT_ASIDE,
		BRING_BACK,
		CLEAR_ITEMS
	}
}