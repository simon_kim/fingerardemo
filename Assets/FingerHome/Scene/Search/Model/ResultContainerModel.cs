﻿using System;
using UnityEngine;

namespace com.fingertalk.vrhome.search
{
	public class ResultContainerModel : IResultContainerModel
	{
		public ResultItemModel [] resultItemModel {get; set;}

		public ResultContainerModel()
		{
		}
	}

    public class ResultItemModel 
    {
		public SearchListItemVO searchList {get; set;}
		public Texture2D thumbnailTexture;

		public string title {
			get {
				return searchList.snippet.title;
			}
		}

		public string description {
			get {
				return searchList.snippet.description;
			}
		}

		public string etc {
			get {
				return searchList.snippet.channelTitle + "-" + searchList.snippet.publishedAt;
			}
		}

		public string thumbnailUrl {
			get {
				return searchList.snippet.thumbnails.high.url;
			}
		}

		public string videoId {
			get {
				return searchList.id.videoId;
			}
		}

		public ResultItemModel()
		{
			thumbnailTexture = null;
		}
    }
}