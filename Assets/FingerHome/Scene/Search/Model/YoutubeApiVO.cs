﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.fingertalk.vrhome.search
{
	[Serializable]
	public class YoutubeSearchListVO
	{
		public string kind;
		public string etag;
		public string nextPageToken;
		public string regionCode;
		public PageInfoVO pageInfo;
		public List<SearchListItemVO> items;

		public static YoutubeSearchListVO CreateFromJSON(string jsonString)
		{
			return JsonUtility.FromJson<YoutubeSearchListVO>(jsonString);
		}
	}

	[Serializable]
	public class PageInfoVO
	{
		public string totalResults;
		public string resultsPerPage;
	}

	[Serializable]
	public class SearchListItemVO
	{
		public string kind;
		public string etag;
		public SearchListItemIdVO id;
		public SnippetVO snippet;
	}

	[Serializable]
	public class SearchListItemIdVO
	{
		public string kind;
		public string videoId;
	}

	[Serializable]
	public class SnippetVO
	{
		public string publishedAt;
		public string channelId;
		public string title;
		public string description;
		public ThumbnailVo thumbnails;
		public string channelTitle;
		public string liveBroadcastContent;
	}

	[Serializable]
	public class ThumbnailVo
	{
		//	public ThumbnailDetailVo default;
		public ThumbnailDetailVo medium;
		public ThumbnailDetailVo high;
	}


	[Serializable]
	public class ThumbnailDetailVo
	{
		public string url;
		public string width;
		public string height;
	}
}

