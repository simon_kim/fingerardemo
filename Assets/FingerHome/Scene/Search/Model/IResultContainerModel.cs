﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace com.fingertalk.vrhome.search
{
	public interface IResultContainerModel
	{
		ResultItemModel [] resultItemModel {get; set;}
	}
}

