﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using com.fingertalk.vrhome.search.events;

namespace com.fingertalk.vrhome.search
{
    public class SearchService : ISearchService
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

		[Inject(ContextKeys.CONTEXT_DISPATCHER)]
		public IEventDispatcher dispatcher { get; set; }

		public SearchServiceModule service;

		public void Activate()
		{
			GameObject go = new GameObject();
			go.name = "SearchServiceModule";
			service = go.AddComponent<SearchServiceModule>();
			service.setDispatcher(dispatcher);
			go.transform.parent = contextView.transform; 
		}

		public void searchKeyword(String keyword, Action<YoutubeSearchListVO> action) 
		{
			service.search(keyword, action);
		}

        public void GetVideoURL(String videoId, Action<String> action)
        {
            service.GetVideoURL(videoId, action);
        }
    }

	public class SearchServiceModule : MonoBehaviour
	{
		private IEventDispatcher dispatcher;
        private string defaultVideo = "twice.mp4";
        private string baseUrl = "https://www.googleapis.com/youtube/v3/search?part=snippet&key=AIzaSyAKgJOGBHqKR5fNQIY9-BzY6IEaknRVcsA&type=video";
        private string getInfoUrl = "https://www.youtube.com/get_video_info";
		private string keyword;
		private Action<YoutubeSearchListVO> onSearchComplete;

		public void setDispatcher(IEventDispatcher dispatcher)
		{
			this.dispatcher = dispatcher;
		}

		public void search(string keyword, Action<YoutubeSearchListVO> action) 
		{
			this.keyword = keyword;
			this.onSearchComplete = action;
			StartCoroutine ("SearchStart");
		}

        public void GetVideoURL(string videoID, Action<String> callback)
        {
            Debug.Log("[SearchService] GetVideoURL : " + videoID);
            StartCoroutine(getVideoInfo(videoID, (String result) => {
                if (result != null) { 
                    String rawResult = WWW.UnEscapeURL(result);
                    String[] splitResult = rawResult.Split('&');
                    String[] tmp;
                    for (int i=0;i<splitResult.Length; i++)
                    {
                        tmp = splitResult[i].Split('=');
                        if (tmp.Length == 2)
                        {
                            if (tmp[0].Equals("url"))
                            {
                                callback(WWW.UnEscapeURL(tmp[1]));
                                return;
                            }
                        }
                    }
                } else
                {
                    callback(null);
                }
            }));
        }

        IEnumerator getVideoInfo(String videoID, Action<String> callback)
        {
            WWW www = new WWW(getInfoUrl + "?&video_id=" + videoID);

            while (!www.isDone)
            {
                yield return www;
            }

            callback(getResult(www));
        }

        String getResult(WWW www)
        {
            if (string.IsNullOrEmpty(www.error))
            {
                if (www.text.Contains("status=fail"))
                {
                    Debug.Log("[SearchServiceModule] getVideoInfo failed : Youtube failed");
                    return null;
                }
                else
                {
                    return www.text;
                }
            }
            else
            {
                // Network failed
                Debug.Log("[SearchServiceModule] getVideoInfo www.url : " + www.url);
                Debug.Log("[SearchServiceModule] getVideoInfo www.error : " + www.error);
                return null;
            }
        }

        IEnumerator SearchStart() {

			WWW www = new WWW ( baseUrl + "&q=" + WWW.EscapeURL(keyword) );

			while (!www.isDone) {
				yield return www;
			}

			if (string.IsNullOrEmpty (www.error)) {
				YoutubeSearchListVO vo = YoutubeSearchListVO.CreateFromJSON (www.text);
				onSearchComplete(vo);
			} else {
				Debug.Log ("[SearchServiceModule] SearchStart www.url : " + www.url);
				Debug.Log ("[SearchServiceModule] SearchStart www.error : " + www.error);
			}
		}
	}
}

