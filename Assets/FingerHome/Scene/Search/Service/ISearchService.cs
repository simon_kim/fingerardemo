﻿using System;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace com.fingertalk.vrhome.search
{
    public interface ISearchService
    {
		void Activate();
		void searchKeyword (String keyword, Action<YoutubeSearchListVO> action);
        void GetVideoURL (String videoId, Action<String> action);
    }
}

