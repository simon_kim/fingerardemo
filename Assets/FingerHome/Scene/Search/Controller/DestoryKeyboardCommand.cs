﻿using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace com.fingertalk.vrhome.search
{
	public class DestoryKeyboardCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		public override void Execute()
		{
			GameObject keyboard = GameObject.Find("Fingertalk") as GameObject;
			GameObject.Destroy(keyboard);
		}
	}
}

