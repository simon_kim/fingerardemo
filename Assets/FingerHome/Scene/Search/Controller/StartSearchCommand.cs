﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.search.events;

namespace com.fingertalk.vrhome.search
{
    public class startSearchCommand : EventCommand
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

		[Inject]
		public ISearchService searchService { get; set; }

        public override void Execute()
		{
			searchService.Activate();
            dispatcher.Dispatch(SearchEvent.SHOW_KEYBOARD);
        }
    }
}

