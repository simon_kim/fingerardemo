﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;

using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.search
{
	public class ShowKeyboardCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		public override void Execute()
		{	
			Debug.Log ("Show Keyboard command");
			PrefabAsyncLoader loader = GameObject.FindObjectOfType<PrefabAsyncLoader> ();

			loader.LoadPrefab("Fingertalk", contextView.transform, (GameObject instance) => {
				instance.name = "Fingertalk";
				instance.transform.position = new Vector3 (0f, 1f, 0f);
			});

		}
	}
}

