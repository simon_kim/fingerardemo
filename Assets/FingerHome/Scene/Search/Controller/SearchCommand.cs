﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.search.events;

namespace com.fingertalk.vrhome.search
{
	public class SearchCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		[Inject]
		public ISearchService searchService { get; set; }

		[Inject]
		public IResultContainerModel model {get; set;}

		public override void Execute()
		{
			searchService.searchKeyword(evt.data as string, (YoutubeSearchListVO result) => {
				model.resultItemModel = new ResultItemModel[ Int32.Parse( result.pageInfo.resultsPerPage ) ];

				for(int i=0;i<model.resultItemModel.Length;i++) {
					model.resultItemModel [i] = new ResultItemModel ();
					model.resultItemModel [i].searchList = result.items [i];
				}

				dispatcher.Dispatch (SearchEvent.SEARCH_COMPLETE);
			});
		}
	}
}

