﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main.events;

namespace com.fingertalk.vrhome.search
{
	public class SelectMovieCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

        [Inject]
        public ISearchService searchService { get; set; }

		public override void Execute()
		{
			string videoID = evt.data as string;
            Debug.Log("[SelectMovieCommand] Execute " + videoID);
            searchService.GetVideoURL(videoID, (String url) => {
                Debug.Log("[SelectMovieCommand] URL : " + url);
                dispatcher.Dispatch(MainEvent.PLAY_MOVIE, url);
            });
		}
	}
}

