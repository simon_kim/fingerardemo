﻿using System;
using System.Collections;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.menu.events;

namespace com.fingertalk.vrhome.menu
{
	public class RouteMenuMediator : EventMediator
	{
		[Inject]
		public RouteMenuView view { get; set; }

		public override void OnRegister()
		{
			dispatcher.AddListener (MainEvent.KEY_DOWN, onNaverClick);
			dispatcher.AddListener (MainEvent.KEY_DOWN, onYoutubeClick);
			dispatcher.AddListener (MainEvent.KEY_DOWN, onLocalMediaClick);
			view.init ();
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (MainEvent.KEY_DOWN, onNaverClick);
			dispatcher.RemoveListener (MainEvent.KEY_DOWN, onYoutubeClick);
			dispatcher.RemoveListener (MainEvent.KEY_DOWN, onLocalMediaClick);
		}

		private void onNaverClick()
		{
			if (view.naverBtnFocused)
			{
				view.SetPosition (view.UNDER_THE_PAGE);

				dispatcher.Dispatch(MenuEvent.Go_NAVER);
			}
		}

		private void onYoutubeClick()
		{
			if (view.youtubeBtnFocused)
			{
				view.SetPosition (view.UNDER_THE_PAGE);

				dispatcher.Dispatch(MenuEvent.GO_YOUTUBE);
			}
		}

		private void onLocalMediaClick()
		{
			if (view.localMediaBtnFocused)
			{
				view.SetPosition (view.UNDER_THE_PAGE);

				dispatcher.Dispatch(MenuEvent.GO_LOCAL_MEDIA);
			}
		}
	}
}