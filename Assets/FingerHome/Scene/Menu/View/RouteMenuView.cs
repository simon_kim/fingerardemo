﻿using System;
using System.Collections;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace com.fingertalk.vrhome.menu
{
	public class RouteMenuView : EventView
	{
		public Vector3 ON_THE_STARTING_POINT = new Vector3(0.0f, 0.0f, 0.0f);
		public Vector3 UNDER_THE_PAGE = new Vector3(0.0f, -1.0f, 0.0f);

		public bool naverBtnFocused;
		public bool youtubeBtnFocused;
		public bool localMediaBtnFocused;

		internal void init()
		{
			naverBtnFocused = false;
			youtubeBtnFocused = false;
			localMediaBtnFocused = false;
		}

		public void OnNaverBtnFocusIn() 
		{
			naverBtnFocused = true;
		}

		public void OnNaverBtnFocusOut() 
		{
			naverBtnFocused = false;
		}

		public void OnYoutubeBtnFocusIn() 
		{
			youtubeBtnFocused = true;
		}

		public void OnYoutubeBtnFocusOut() 
		{
			youtubeBtnFocused = false;
		}

		public void OnLocalMediaBtnFocusIn() 
		{
			localMediaBtnFocused = true;
		}

		public void OnLocalMediaBtnFocusOut() 
		{
			localMediaBtnFocused = false;
		}


		public void SetPosition(Vector3 position)
		{
			gameObject.transform.position = position;
		}

	}
}