﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.menu.events;
using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.menu
{
	public class MenuContext : MVCSContext
	{
		public MenuContext(MonoBehaviour view) : base(view)
		{
		}

		public MenuContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
		{
		}

		protected override void mapBindings()
		{
			//injectionBinder.Bind<IModel>().To<Model>();

			injectionBinder.Bind<IMainRouterService>().To<MainRouterService>().ToSingleton();

			mediationBinder.Bind<RouteMenuView> ().To<RouteMenuMediator> ();

			//commandBinder.Bind(MenuEvent.FadeOut).To<FadeOutCommand>();
			commandBinder.Bind(MenuEvent.Go_NAVER).To<GoNaverCommand>();
			commandBinder.Bind(MenuEvent.GO_YOUTUBE).To<GoYoutubeCommand>();
			commandBinder.Bind(MenuEvent.GO_LOCAL_MEDIA).To<GoLocalMediaCommand>();
		}

		public IEventDispatcher GetDispatcher() {
			return dispatcher;
		}
	}
}

