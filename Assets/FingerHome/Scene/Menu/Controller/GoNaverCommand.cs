﻿using System;
using UnityEngine;
using System.Collections;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.menu
{
	public class GoNaverCommand : EventCommand
	{
		[Inject]
		public IMainRouterService router { get; set; }

		public override void Execute()
		{
			Debug.Log("Btn is clicked!");
			router.route("Webview", () =>
				{
					Debug.Log("Webview Scene load complete");
				});
		}
	}
}