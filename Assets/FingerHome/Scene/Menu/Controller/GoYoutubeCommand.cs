﻿using System;
using UnityEngine;
using System.Collections;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.menu
{
	public class GoYoutubeCommand : EventCommand
	{
		[Inject]
		public IMainRouterService router { get; set; }

		public override void Execute()
		{
			Debug.Log("Btn is clicked!");
			router.route("Search", () =>
				{
					Debug.Log("Search Scene load complete");
				});
		}
	}
}