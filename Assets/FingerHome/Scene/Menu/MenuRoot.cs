﻿
using System;
using UnityEngine;
using strange.extensions.context.impl;

namespace com.fingertalk.vrhome.menu
{
	public class MenuRoot : ContextView
	{
		void Awake()
		{
			context = new MenuContext (this);
		}
	}
}
