﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.movieplayer.events;
using com.fingertalk.vrhome.main.events;

namespace com.fingertalk.vrhome.movieplayer
{
    public class MoviePlayerContext : MVCSContext
    {
        public MoviePlayerContext(MonoBehaviour view) : base(view)
        {
        }

        public MoviePlayerContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
            injectionBinder.Bind<IMoviePlayerModel>().To<MoviePlayerModel>().ToSingleton();
            injectionBinder.Bind<IMoviePlayerService>().To<MoviePlayerService>().ToSingleton();

            mediationBinder.Bind<MoviePlayerView>().To<MoviePlayerMediator>();

            commandBinder.Bind(ContextEvent.START).To<StartMoviePlayerCommand>().Once();
			commandBinder.Bind(MoviePlayerEvent.PLAY).To<PlayMovieCommand> ();

        }
    }
}

