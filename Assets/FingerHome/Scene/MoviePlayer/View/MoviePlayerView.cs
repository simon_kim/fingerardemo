﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;

namespace com.fingertalk.vrhome.movieplayer
{
	public class MoviePlayerView : View {
		public GameObject finger3x4;

		public GameObject CtrlPanel;
		public GameObject CurrentTime;
		public GameObject CtrlSeekBar;
		public GameObject PlayTime;
		public GameObject CtrlMenuBar;
		public GameObject CtrlPlay;

		public Texture[] menuIconTexture;
		public Texture[] playIconTexture;
		public Texture[] stopIconTexture;

		public Texture[] seekBarIconTexture;

		public MediaPlayerCtrl mediaPlayerCtrl;
		public SeekBarCtrl seekBarCtrl;
		IEventDispatcher dispatcher;

		public DateTime	 focusedAimTime;

		public int playTime;

        public enum MODE
        {
            PLAY,
            CONTROL
        }

        public enum FOCUS
        {
            NULL,
            RETURN,
            PLAY
        }

        public FOCUS currentFocus;
        public MODE currentMode;
        public MediaPlayerCtrl.MEDIAPLAYER_STATE currentMovieMode;

        // Video runtime error should be handled within view to sperate action from error thread
        private bool _isNotSupportedVideo = false;
        private String _fallbackVideoURL;

        public void NotSupportedVideoFallback(String fallbackVideoURL)
        {
            _isNotSupportedVideo = true;
            _fallbackVideoURL = fallbackVideoURL;
        }

        internal void init(IEventDispatcher dispatcher)
		{
			mediaPlayerCtrl = GameObject.Find("VideoManager").GetComponent<MediaPlayerCtrl>();
            this.dispatcher = dispatcher;
            SetPlayMode();
			setPlayTime ();
			CurrentTime.SetActive (false);
			PlayTime.SetActive (false);
        }

		public void ResetAnimation()
		{
			Debug.Log ("2");
			StartCoroutine(DelayExit());
		}

		public void StartAnimation()
		{
			finger3x4.SetActive (true);
			finger3x4.GetComponent<Animator> ().SetBool ("isStart", true);
		}

		private IEnumerator DelayExit()
		{
			Debug.Log ("3");
			finger3x4.GetComponent<Animator> ().SetBool ("isExit", true);
			Debug.Log ("4");
			yield return new WaitForSeconds(1.0f);
			Debug.Log ("5");
			finger3x4.SetActive (false);
		}

		public string changeMsToHHMMSS(int timeMs){
			Debug.Log ("Time is : " + timeMs);
			double timeHH = Math.Truncate( (double) (timeMs / (1000 * 60 * 60) ));//type has to be changed into int
			Debug.Log ("HH is : " + timeHH);
			double timeMM = Math.Truncate( (double) (timeMs % (1000 * 60 * 60)) / (1000 * 60) );
			Debug.Log ("MM is : " + timeMM);
			double timeSS = Math.Truncate( (double) (timeMs % (1000 * 60)) / 1000 );
			Debug.Log ("SS is : " + timeSS);
			return timeHH.ToString() + ":" + timeMM.ToString() + ":" + timeSS.ToString();
		}

		public void setPlayTime() {
			Debug.Log ("GetDuration is " + mediaPlayerCtrl.GetDuration ());
			Debug.Log ("GetCurrentSeekPercent is " + mediaPlayerCtrl.GetCurrentSeekPercent ());
			//PlayTime.GetComponent<Text> ().text = changeMsToHHMMSS( mediaPlayerCtrl.GetCurrentSeekPercent () );//unit is ms.
			//PlayTime.GetComponent<Text> ().text = changeMsToHHMMSS( mediaPlayerCtrl.GetDuration () );//unit is ms.
		}

        void Update()
        {
            if (_isNotSupportedVideo)
            {
                mediaPlayerCtrl.Load(_fallbackVideoURL);
                _isNotSupportedVideo = false;
            }

			TimeSpan gap = DateTime.Now - focusedAimTime;
			if (currentFocus == FOCUS.NULL && gap.TotalSeconds > 2.5) {
				//Debug.Log ("Time Span : " + gap.TotalSeconds);
				//	SetPlayMode ();
			} else {
				currentMovieMode = mediaPlayerCtrl.GetCurrentState ();
				if (currentMovieMode == MediaPlayerCtrl.MEDIAPLAYER_STATE.ERROR) {
					Debug.Log ("MainEvent.END_MOVIE");
					dispatcher.Dispatch (MainEvent.END_MOVIE);
				} else {
					if (currentMode == MODE.CONTROL) {
						updatePlayIcon ();
						updateMenuHighlight ();
					}
				}
			}
        }

        public void OnMenuButtonFocusIn() 
		{
			focusedAimTime = DateTime.Now;
            currentFocus = FOCUS.RETURN;
        }

		public void OnMenuButtonFocusOut() 
		{
            currentFocus = FOCUS.NULL;
        }

		public void OnPlayButtonFocusIn() 
		{
			focusedAimTime = DateTime.Now;
            currentFocus = FOCUS.PLAY;
        }

		public void OnPlayButtonFocusOut() 
		{
            currentFocus = FOCUS.NULL;
        }

		public void OnSeekValueChanged() {
			Debug.Log (mediaPlayerCtrl.GetSeekPosition ());
			//CurrentTime.GetComponent<Text> ().text = changeMsToHHMMSS( mediaPlayerCtrl.GetSeekPosition () );

		}

        public void SetPlayMode()
        {
            SetVideoControlUI(false);
            currentMode = MODE.PLAY;
            currentFocus = FOCUS.NULL;
        }

        public void SetControlMode()
        {
			focusedAimTime = DateTime.Now;
            SetVideoControlUI(true);
            currentMode = MODE.CONTROL;
            currentFocus = FOCUS.NULL;
        }

        void SetVideoControlUI(bool option)
        {
            CtrlMenuBar.SetActive(option);
            CtrlPlay.SetActive(option);
            CtrlSeekBar.SetActive(option);

            // Control DIM Background
            CanvasGroup dim = CtrlPanel.GetComponent<CanvasGroup>();
            dim.alpha = option ? 0.5f : 0.0f;
        }

		void updateMenuHighlight()
		{
            RawImage menuIcon = GameObject.Find("ControllMenuBar").GetComponent<RawImage>();
			if (currentFocus == FOCUS.RETURN) 
			{
				menuIcon.texture = menuIconTexture [1];
			} else 
			{
				menuIcon.texture = menuIconTexture [0];
			}	
		}

        void updatePlayIcon()
		{
			/*
            RawImage playIcon = GameObject.Find("ControlPlay").GetComponent<RawImage>();
            if (currentFocus == FOCUS.PLAY) 
				{
				if (currentMovieMode == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING) 
				{
                    playIcon.texture = stopIconTexture [1];
				} 
				else 
				{
                    playIcon.texture = playIconTexture [1];
				}
			} 
			else 
			{
				if (currentMovieMode == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING) 
				{
                    playIcon.texture = stopIconTexture [0];
				} 
				else 
				{
                    playIcon.texture = playIconTexture [0];
				}
			}
			*/
        }

		void updateSeekBar()
		{
			RawImage seekBarIcon = GameObject.Find("Handle").GetComponent<RawImage>();
			seekBarIcon.texture = seekBarIconTexture [0];
		}

        public void TransitionControlMode()
        {
            Debug.Log("[MoviePlayerView] TransitionControlMode");
            StartAnimation();
            mediaPlayerCtrl.Pause();
            SetControlMode();
        }

        public void TransitionPlayMode()
        {
            Debug.Log("[MoviePlayerView] TransitionPlayMode");
            ResetAnimation();
            SetPlayMode();
        }

        public void GoHome()
        {
            StartCoroutine(TriggerEvent(MainEvent.GO_HOME));
        }

        public void GoPrevious()
        {
            StartCoroutine(TriggerEvent(MainEvent.END_MOVIE));
        }

        IEnumerator TriggerEvent(MainEvent evt)
        {
            yield return new WaitForSeconds(1.5f);
            mediaPlayerCtrl.Stop();
            mediaPlayerCtrl.UnLoad();
            dispatcher.Dispatch(evt);
        }

        public void DelayPlayMovie()
        {
            StartCoroutine(PlayMovie());
        }

        IEnumerator PlayMovie()
        {
            yield return new WaitForSeconds(1.5f);
            mediaPlayerCtrl.Play();
        }
	}
}

