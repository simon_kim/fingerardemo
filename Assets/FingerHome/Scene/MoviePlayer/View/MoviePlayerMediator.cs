﻿/// Example mediator
/// =====================
/// Make your Mediator as thin as possible. Its function is to mediate
using System;
using System.Collections;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.movieplayer.events;
using strange.extensions.context.api;

namespace com.fingertalk.vrhome.movieplayer
{
    public class MoviePlayerMediator : EventMediator
    {
        [Inject]
        public MoviePlayerView view { get; set; }

        [Inject]
        public IMoviePlayerService moviePlayerService { get; set; }

        public override void OnRegister()
        {
            dispatcher.AddListener (MainEvent.SHOW_SCENE, onShow);
            dispatcher.AddListener (MainEvent.HIDE_SCENE, onHide);
			view.init(dispatcher);
        }

        public override void OnRemove()
        {
        }

        private void onShow(IEvent evt)
        {
            if (evt.data.Equals(contextView.scene.name))
            {
                Debug.Log("[MoviePlayerMediator] onShow");
                view.SetPlayMode();
                dispatcher.AddListener(MainEvent.KEY_DOWN, onKeyDown);
                dispatcher.AddListener(MoviePlayerEvent.DRAG, onSeekClick);
                dispatcher.AddListener(MoviePlayerEvent.SELECT, onSelect);
                dispatcher.AddListener(MoviePlayerEvent.VIDEO_NOT_SUPPORTED, onVideoNotSupported);
            }
        }

        private void onHide(IEvent evt)
        {
            if (evt.data.Equals(contextView.scene.name))
            {
                Debug.Log("[MoviePlayerMediator] onHide");
                dispatcher.RemoveListener(MainEvent.KEY_DOWN, onKeyDown);
                dispatcher.RemoveListener(MoviePlayerEvent.DRAG, onSeekClick);
                dispatcher.RemoveListener(MoviePlayerEvent.SELECT, onSelect);
                dispatcher.RemoveListener(MoviePlayerEvent.VIDEO_NOT_SUPPORTED, onVideoNotSupported);
            }
        }

        private void onVideoNotSupported()
        {
            Debug.Log("[MoviePlayerMediator] onVideoNotSupported");
            view.NotSupportedVideoFallback(moviePlayerService.getDefaultVideo());
        }

        private void onKeyDown(IEvent evt)
        {
			Debug.Log ("view mode : " + view.currentMode);
            if (view.currentMode == MoviePlayerView.MODE.CONTROL)
            {
                view.TransitionPlayMode();
                handleControlModeEvent(evt);
            } else if (view.currentMode == MoviePlayerView.MODE.PLAY)
            {
                view.TransitionControlMode();
            }
//			if (!view.seekCurrentFocus) {
/*
				switch (view.currentMode) 
				{
					case MoviePlayerView.MODE.PLAY:
						view.SetControlMode ();
						break;
					case MoviePlayerView.MODE.CONTROL:
						handleControlModeEvent ();
						break;
				}
//			}
*/
        }

        private void handleControlModeEvent(IEvent evt)
        {
            switch ((KeyCode)evt.data)
            {
                case KeyCode.Alpha1:
                    view.GoHome();
                    return;
                case KeyCode.Alpha3:
                    view.GoPrevious();
                    return;
                case KeyCode.Alpha4:
                    view.GoPrevious();
                    return;
                default:
                    view.DelayPlayMovie();
                    break;
            }
        }

        private void handleControlModeEvent()
        {
            switch (view.currentFocus)
            {
                case MoviePlayerView.FOCUS.PLAY:
                    toggleMoviePlay();
                    break;
                case MoviePlayerView.FOCUS.RETURN:
                    returnPreviousScene();
                    break;
                default:
                    view.SetPlayMode();
                    break;
            }
        }

        private void toggleMoviePlay()
        {
            MediaPlayerCtrl.MEDIAPLAYER_STATE currentState = view.mediaPlayerCtrl.GetCurrentState();
            if (currentState == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
            {
                view.mediaPlayerCtrl.Pause();
            }
            else if (currentState == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED)
            {
                view.mediaPlayerCtrl.Play();
            }
        }

        private void returnPreviousScene()
        {
            view.mediaPlayerCtrl.Stop();
            view.mediaPlayerCtrl.UnLoad();
            dispatcher.Dispatch(MainEvent.END_MOVIE);
        }

        private void goHome()
        {
            view.mediaPlayerCtrl.Stop();
            view.mediaPlayerCtrl.UnLoad();
            dispatcher.Dispatch(MainEvent.GO_HOME);
        }

		private void onSeekClick() {}
		private void onSelect() {}
    }
}

