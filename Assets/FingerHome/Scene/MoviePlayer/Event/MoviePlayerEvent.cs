﻿using System;

namespace com.fingertalk.vrhome.movieplayer.events
{
	public enum MoviePlayerEvent {
		PLAY,
		BACK,
		DRAG,
		SELECT,
        VIDEO_NOT_SUPPORTED
    }
}

