﻿using System;
using UnityEngine;
using strange.extensions.context.impl;

namespace com.fingertalk.vrhome.movieplayer
{
    public class MoviePlayerRoot : ContextView
    {
        void Awake()
        {
            context = new MoviePlayerContext(this);
        }
    }
}

