﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace com.fingertalk.vrhome.movieplayer
{
	public class PauseMovieCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		//MediaPlayerCtrl mediaPlayerCtrl;

		public override void Execute()
		{
			Debug.Log ("###@@@ [WOW] PauseMovieCommand "+ evt.data as string);
			GameObject videoManager = GameObject.Find ("VideoManager") as GameObject;
			//mediaPlayerCtrl = videoManager.GetComponent<MediaPlayerCtrl> ();
			//mediaPlayerCtrl.Pause ();
		}
	}
}

