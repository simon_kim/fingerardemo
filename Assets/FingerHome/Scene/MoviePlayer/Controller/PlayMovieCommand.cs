﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace com.fingertalk.vrhome.movieplayer
{
	public class PlayMovieCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

        [Inject]
        public IMoviePlayerService moviePlayerService { get; set; }

		MediaPlayerCtrl mediaPlayerCtrl;

		public override void Execute()
		{
			String videoURL = evt.data as string;
			GameObject videoManager = GameObject.Find ("VideoManager") as GameObject;
			mediaPlayerCtrl = videoManager.GetComponent<MediaPlayerCtrl> ();
			mediaPlayerCtrl.DeleteVideoTexture ();
            mediaPlayerCtrl.Load(videoURL != null ? videoURL : moviePlayerService.getDefaultVideo());
		}
	}
}

