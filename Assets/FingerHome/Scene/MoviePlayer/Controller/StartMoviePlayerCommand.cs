﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.movieplayer.events;

namespace com.fingertalk.vrhome.movieplayer
{
    public class StartMoviePlayerCommand : EventCommand
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        MediaPlayerCtrl mediaPlayerCtrl;

        public override void Execute()
        {
			GameObject videoManager = GameObject.Find ("VideoManager") as GameObject;
			mediaPlayerCtrl = videoManager.GetComponent<MediaPlayerCtrl> ();
			mediaPlayerCtrl.OnReady += OnReady;
			mediaPlayerCtrl.OnEnd += OnEnd;
            mediaPlayerCtrl.OnVideoError += OnError;
        }

		void OnReady() {
            mediaPlayerCtrl.Play ();
		}

		void OnEnd() {
			dispatcher.Dispatch (MainEvent.END_MOVIE);
		}

        void OnError(MediaPlayerCtrl.MEDIAPLAYER_ERROR iCode, MediaPlayerCtrl.MEDIAPLAYER_ERROR iCodeExtra)
        {
            Debug.Log("[StartMoviePlayerCommand] OnError");
            dispatcher.Dispatch(MoviePlayerEvent.VIDEO_NOT_SUPPORTED);
        }
    }
}

