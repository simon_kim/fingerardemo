﻿using System;

namespace com.fingertalk.vrhome.movieplayer
{
    public interface IMoviePlayerService
    {
        String getDefaultVideo();
    }
}
