﻿using System;

namespace com.fingertalk.vrhome.movieplayer
{
    public class MoviePlayerService : IMoviePlayerService
    {
        private String[] defaultVideos = new String[] {
            "SDS01.mp4",
            "SDS02.mp4",
            "SDS03.mp4",
            "SDS04.mp4",
            "SDS05.mp4"
        };

        private int _index = 0;

        public String getDefaultVideo()
        {
            String videoURL = defaultVideos[_index];
            _index = _index < defaultVideos.Length - 1 ? _index + 1 : 0;
            return videoURL;
        }
    }
}
