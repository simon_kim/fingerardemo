﻿using UnityEngine;
using System.Collections;
using com.fingertalk.vrhome.main;
using fingertalk.vr.keyboard;

namespace com.fingertalk.vrhome.main
{
    public class FingerTalkPluginConnector : MonoBehaviour
    {
        MainView nativeBridge;
        MainInputServiceModule mainInputService;

        public delegate void NativeListener(string str);
        private NativeListener _listener;

        private bool isIMEOpen = false;

        public enum KeyEventType
        {
            KEY_UP,
            KEY_DOWN
        }

        // Use this for initialization
        void Start()
        {
            nativeBridge = GameObject.FindGameObjectWithTag("NativeBridgeView").GetComponent<MainView>();
        }

        public void AddListener(NativeListener listener)
        {
            _listener += listener;
        }

        public void RemoveListener(NativeListener listener)
        {
            _listener -= listener;
        }

        public void OnPluginEvent(string evtStr)
        {
            Debug.Log("OnPluginEvent Unity : " + evtStr);
			webview.WebviewPlaneView wv = GameObject.Find ("WebviewPlane").GetComponent<webview.WebviewPlaneView> ();
            if (evtStr.Equals("ShowKeyboard"))
            {
                Debug.Log("[FingerTalkPluginConnector] ShowKeyboard");
                nativeBridge.ShowKeyboard();
				wv.EnableInputAssist ();
                isIMEOpen = true;
            }
            else if (evtStr.Equals("HideKeyboard"))
            {
                Debug.Log("[FingerTalkPluginConnector] HideKeyboard");
                nativeBridge.HideKeyboard();
				wv.DisableInputAssist();
                isIMEOpen = false;
            }
        }
			
        void Update()
        {
            if (Application.isEditor)
            {
                KeyCode keycode = KeyCode.None;
                keycode = getUnityKeyDown();
                if (keycode != KeyCode.None)
                {
                    handleKeyEvent(KeyEventType.KEY_DOWN, keycode);
                }
                keycode = getUnityKeyUp();
                if (keycode != KeyCode.None)
                {
                    handleKeyEvent(KeyEventType.KEY_UP, keycode);
                }
                /* Test */
                /*
                GameObject.Find("FingerTalkPluginConnector").GetComponent<FingerTalkPluginConnector>().OnPluginKeyDown("SPACE");
                dispatcher.Dispatch(MainEvent.SHOW_MESSAGE, "01038778323;john;test");
                */
            }
        }

        public void OnPluginKeyDown(string unityKeycode)
        {
            Debug.Log("[FingerTalkPluginConnector] OnPluginKeyDown keycode : " + unityKeycode);
            handleKeyEvent(KeyEventType.KEY_DOWN, getKeyCode(unityKeycode));
        }

        public void OnPluginKeyUp(string unityKeycode)
        {
            Debug.Log("[FingerTalkPluginConnector] OnPluginKeyUp keycode : " + unityKeycode);
            handleKeyEvent(KeyEventType.KEY_UP, getKeyCode(unityKeycode));
        }

        private void handleKeyEvent(KeyEventType eventType, KeyCode keycode)
        {
            Debug.Log("[FingerTalkPluginConnector] handleKeyEvent EventType(" + eventType.ToString() + ") KeyCode("+keycode.ToString()+")");

            if (eventType == KeyEventType.KEY_DOWN) { 
                gameObject.GetComponent<AudioSource>().Play();
            }

            if (isFingerTalkTextMode())
            {
                passKeyEventToFingerTalkCore(eventType, keycode);
            }
            else
            {
                sendKeyEventToListener(keycode);
                sendKeyEventToMainInputService(eventType, keycode);
            }
        }

        private void sendKeyEventToListener(KeyCode keycode)
        {
            if (_listener != null)
            {
                //Debug.Log("check Unity Listener keydown: " + _listener.Target.ToString());
                _listener("");
            }
            else
            {
                Debug.Log("[FingerTalkPluginConnector] No listener assigned");
            }
        }

        private void sendKeyEventToMainInputService(KeyEventType eventType, KeyCode keycode)
        {
            Debug.Log("[FingerTalkPluginConnector] sendKeyEventToMainInputService");
            if (mainInputService == null)
            {
                mainInputService = GameObject.Find("MainInputServiceModule").GetComponent<MainInputServiceModule>();
            }

            if (eventType == KeyEventType.KEY_DOWN)
            {
                mainInputService.DispatchKeyDown(keycode);
            } else if (eventType == KeyEventType.KEY_UP)
            {
                mainInputService.DispatchKeyUp(keycode);
            }
        }

        private bool isFingerTalkTextMode()
        {
            return GameObject.Find("Fingertalk") != null ? true : false;
        }

        private void passKeyEventToFingerTalkCore(KeyEventType eventType, KeyCode keycode)
        {
//            if (_listener != null && GameObject.Find("ReplyContent") != null)
//            {
//                if (_listener.Target.ToString().Equals(GameObject.Find("SendSMSButton").name) && GameObject.Find("ReplyContent").transform.childCount >= 3)
//                {
//                    _listener("");
//                    Debug.Log("답장써야해");
//                    nativeBridge.HideKeyboard();
//                    isIMEOpen = false;
//                    return;
//                }
//            }
            GameObject core = GameObject.Find("UnityInputInternalModule");
            UnityInputInternalModule coreModule = core.GetComponent<UnityInputInternalModule>();
            if (eventType == KeyEventType.KEY_DOWN)
            {
                coreModule.dispatchKeyDownEvent(keycode);
            } else if (eventType == KeyEventType.KEY_UP)
            {
                coreModule.dispatchKeyUpEvent(keycode);
            }
            
        }

        #region Find KeyCode given android key code
        private KeyCode getKeyCode(string str)
        {
            KeyCode unityKeycode = KeyCode.None;
            switch(str)
            {
                case "8":
                    unityKeycode = KeyCode.Alpha1;
                    break;
                case "9":
                    unityKeycode = KeyCode.Alpha2;
                    break;
                case "10":
                    unityKeycode = KeyCode.Alpha3;
                    break;
                case "11":
                case "45": // QWERTY : Q
                    unityKeycode = KeyCode.Alpha4;
                    break;
                case "12":
                case "51": // QWERTY : W
                    unityKeycode = KeyCode.Alpha5;
                    break;
                case "13":
                case "33": // QWERTY : E
                    unityKeycode = KeyCode.Alpha6;
                    break;
                case "14":
                case "29": // QWERTY : A
                    unityKeycode = KeyCode.Alpha7;
                    break;
                case "15":
                case "47": // QWERTY : S
                    unityKeycode = KeyCode.Alpha8;
                    break;
                case "16":
                case "32": // QWERTY : D
                    unityKeycode = KeyCode.Alpha9;
                    break;
                case "7":
                case "52": // QWERTY : X
                    unityKeycode = KeyCode.Alpha0;
                    break;
                case "131":
                case "54": // QWERTY : Z
                case "62": // QWERTY : Space
                    unityKeycode = KeyCode.F1;
                    break;
                case "132":
                case "31": // QWERTY : C
                case "67": // QWERTY : Backspace
                    unityKeycode = KeyCode.F2;
                    break;
                case "133":
                case "59": // QWERTY : Left Shift
                    unityKeycode = KeyCode.F3;
                    break;
                case "134":
                case "46": // QWERTY : R
                    unityKeycode = KeyCode.F4;
                    break;
                case "135":
                case "34": // QWERTY : F
                    unityKeycode = KeyCode.F5;
                    break;
                case "136":
                case "50": // QWERTY : V
                    unityKeycode = KeyCode.F6;
                    break;
                case "66": // QWERTY : Enter
                    unityKeycode = KeyCode.KeypadEnter;
                    break;
                case "19": // QWERTY : Up
                    unityKeycode = KeyCode.UpArrow;
                    break;
                case "20": // QWERTY : Down
                    unityKeycode = KeyCode.DownArrow;
                    break;
                case "21": // QWERTY : Left
                    unityKeycode = KeyCode.LeftArrow;
                    break;
                case "22": // QWERTY : Right
                    unityKeycode = KeyCode.RightArrow;
                    break;
                default:
                    break;
            }
            return unityKeycode;
        }
        #endregion Find 

        #region Find KeyCode given key down event
        private KeyCode getUnityKeyDown()
        {
            KeyCode keycode = KeyCode.None;
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                keycode = KeyCode.Alpha1;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                keycode = KeyCode.Alpha2;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                keycode = KeyCode.Alpha3;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                keycode = KeyCode.Alpha4;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                keycode = KeyCode.Alpha5;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                keycode = KeyCode.Alpha6;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                keycode = KeyCode.Alpha7;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                keycode = KeyCode.Alpha8;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                keycode = KeyCode.Alpha9;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                keycode = KeyCode.Alpha0;
            }
            else if (Input.GetKeyDown(KeyCode.F1))
            {
                keycode = KeyCode.F1;
            }
            else if (Input.GetKeyDown(KeyCode.F2))
            {
                keycode = KeyCode.F2;
            }
            else if (Input.GetKeyDown(KeyCode.F3))
            {
                keycode = KeyCode.F3;
            }
            else if (Input.GetKeyDown(KeyCode.F4))
            {
                keycode = KeyCode.F4;
            }
            else if (Input.GetKeyDown(KeyCode.F5))
            {
                keycode = KeyCode.F5;
            }
            else if (Input.GetKeyDown(KeyCode.F6))
            {
                keycode = KeyCode.F6;
            }
            else if (Input.GetKeyDown((KeyCode)10) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                keycode = KeyCode.KeypadEnter;
            } else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                keycode = KeyCode.UpArrow;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                keycode = KeyCode.DownArrow;
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                keycode = KeyCode.LeftArrow;
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                keycode = KeyCode.RightArrow;
            }
            return keycode;
        }
        #endregion

        #region Find KeyCode given key up event
        private KeyCode getUnityKeyUp()
        {
            KeyCode keycode = KeyCode.None;
            if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                keycode = KeyCode.Alpha1;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha2))
            {
                keycode = KeyCode.Alpha2;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha3))
            {
                keycode = KeyCode.Alpha3;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha4))
            {
                keycode = KeyCode.Alpha4;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha5))
            {
                keycode = KeyCode.Alpha5;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha6))
            {
                keycode = KeyCode.Alpha6;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha7))
            {
                keycode = KeyCode.Alpha7;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha8))
            {
                keycode = KeyCode.Alpha8;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha9))
            {
                keycode = KeyCode.Alpha9;
            }
            else if (Input.GetKeyUp(KeyCode.Alpha0))
            {
                keycode = KeyCode.Alpha0;
            }
            else if (Input.GetKeyUp(KeyCode.F1))
            {
                keycode = KeyCode.F1;
            }
            else if (Input.GetKeyUp(KeyCode.F2))
            {
                keycode = KeyCode.F2;
            }
            else if (Input.GetKeyUp(KeyCode.F3))
            {
                keycode = KeyCode.F3;
            }
            else if (Input.GetKeyUp(KeyCode.F4))
            {
                keycode = KeyCode.F4;
            }
            else if (Input.GetKeyUp(KeyCode.F5))
            {
                keycode = KeyCode.F5;
            }
            else if (Input.GetKeyUp(KeyCode.F6))
            {
                keycode = KeyCode.F6;
            }
            else if (Input.GetKeyUp((KeyCode)10) || Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Return))
            {
                keycode = KeyCode.KeypadEnter;
            }
            else if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                keycode = KeyCode.UpArrow;
            }
            else if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                keycode = KeyCode.DownArrow;
            }
            else if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                keycode = KeyCode.LeftArrow;
            }
            else if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                keycode = KeyCode.RightArrow;
            }
            return keycode;
        }
        #endregion

    }
}
    
