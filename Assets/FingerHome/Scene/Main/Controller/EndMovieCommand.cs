﻿using System;
using System.Collections;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.movieplayer.events;

namespace com.fingertalk.vrhome.main
{
	public class EndMovieCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		[Inject]
		public IMainRouterService router { get; set; }

		public override void Execute()
		{
			router.route ("Search");
			if(null != GameObject.Find ("MessagePrefab"))
			{
				GameObject.Find ("MessagePrefab").transform.localPosition = new Vector3(650f, 0f, 1000f);
			}
		}
	}
}

