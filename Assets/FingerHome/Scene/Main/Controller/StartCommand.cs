﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main.events;

namespace com.fingertalk.vrhome.main
{
    public class StartCommand : EventCommand
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

		[Inject]
		public IMainInputService mainInputService { get; set; }

		[Inject]
		public IMainRouterService router { get; set; }

		[Inject]
		public IAndroidMessageService androidMessageService { get; set; }

        public override void Execute()
        {
			mainInputService.Activate();
    		androidMessageService.Activate ();
        }
    }
}

