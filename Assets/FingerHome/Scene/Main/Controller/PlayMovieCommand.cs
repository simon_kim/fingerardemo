﻿using System;
using System.Collections;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.movieplayer.events;

namespace com.fingertalk.vrhome.main
{
	public class PlayMovieCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		[Inject]
		public IMainInputService mainInputService { get; set; }

		[Inject]
		public IMainRouterService router { get; set; }
			
		public override void Execute()
		{
			string url = evt.data as string;
			router.route ("MoviePlayer", () => {
				dispatcher.Dispatch (MoviePlayerEvent.PLAY, url);
			});
		}
	}
}

