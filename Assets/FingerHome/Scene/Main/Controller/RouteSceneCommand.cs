﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.main
{
	public class RouteSceneCommand : EventCommand
	{
		[Inject]
		public IMainRouterService router { get; set; }

		public override void Execute()
		{
			Debug.Log("Home Menu " + evt.data as string + " Icon is clicked!");

			router.route(evt.data as string, () =>
				{
					Debug.Log(evt.data as string + " Scene load complete");
				});
		}
	}
}