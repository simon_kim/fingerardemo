﻿using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main.events;

namespace com.fingertalk.vrhome.main
{
	public class MakeToastCommand : EventCommand
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		public override void Execute()
		{
			GameObject.Find ("Toast").GetComponent<Text> ().text = evt.data as string;
		}
	}
}

