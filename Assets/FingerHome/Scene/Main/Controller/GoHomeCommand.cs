﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main;

namespace com.fingertalk.vrhome.main
{
	public class GoHomeCommand : EventCommand
	{
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        [Inject]
        public IMainRouterService router { get; set; }

        public override void Execute()
		{
            router.route("Main");
        }
	}
}