﻿using System;

namespace com.fingertalk.vrhome.main.events
{
	public enum MainEvent {
		CLICK_WEBVIEW,
		CLICK_YOUTUBE,
		CLICK_MOVIES,
		CLICK_HOME,
		PLAY_MOVIE,
		END_MOVIE,
		KEY_DOWN,
        KEY_UP,
        MAKE_TOAST,
		SHOW_SCENE,
		HIDE_SCENE,
		SHOW_MESSAGE,
		RECEIVED_SMS,
		SEND_SMS,
		RECEIVED_CALL,
		SHOW_PHONE_ALARM,
        GO_HOME,
//		CONTROLLER_CONNECTED
	}
}

