﻿using System;
using UnityEngine;
using strange.extensions.context.impl;

namespace com.fingertalk.vrhome.main
{
    public class MainRoot : ContextView
    {
        void Awake()
        {
            context = new MainContext(this);
        }
    }
}