﻿using System;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace com.fingertalk.vrhome.main
{
    public interface IMainInputService
    {
		void Activate();
    }
}

