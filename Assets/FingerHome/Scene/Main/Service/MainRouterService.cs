﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using com.fingertalk.vrhome.main.events;
using fingertalk.vr.keyboard;

namespace com.fingertalk.vrhome.main
{
	public class MainRouterService : IMainRouterService
	{
		[Inject(ContextKeys.CONTEXT_DISPATCHER)]
		public IEventDispatcher dispatcher { get; set; }

		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		public string currentSceneName = "";
		public Dictionary<string, GameObject> loadedViewRoot;

		private SceneAsyncLoader sceneAsyncLoader;
		private PrefabAsyncLoader prefabAsyncLoader;

		public MainRouterService() {
			loadedViewRoot = new Dictionary<string, GameObject>();

			sceneAsyncLoader = GameObject.FindObjectOfType<SceneAsyncLoader> ();
			sceneAsyncLoader.init (dispatcher);

			prefabAsyncLoader = GameObject.FindObjectOfType<PrefabAsyncLoader> ();
			prefabAsyncLoader.init (dispatcher);

            loadedViewRoot.Add("Main", findLoadedScene("Main"));
            currentSceneName = "Main";
        }

		public void route(string sceneName) {
			Debug.Log ("[MainRouterService] route (No callback)");
			route (sceneName, () => {
			});
		}

		public void route(string sceneName, Action routeCallback) {
			Debug.Log ("[MainRouterService] route - " + sceneName);
			if (!currentSceneName.Equals ("")) {
				hideScene (currentSceneName);
			}
			if (!loadedViewRoot.ContainsKey(sceneName)) {
				sceneAsyncLoader.LoadScene (sceneName, () => {
					showScene(sceneName);
					routeCallback();
				});
			} else {
				showScene(sceneName);
				routeCallback();
			}
		}

		public void showScene(string name) {
			Debug.Log ("[MainRouterService] showScene - " + name);
			currentSceneName = name;
			if (loadedViewRoot.ContainsKey (name)) {
				// Created already
				loadedViewRoot [name].SetActive (true);
			} else {
				// First Create
				loadedViewRoot.Add (name, findLoadedScene (name));
			}
			dispatcher.Dispatch(MainEvent.SHOW_SCENE, name);
		}

		public void hideScene(string name) {
			Debug.Log ("[MainRouterService] hideScene - " + name);
			dispatcher.Dispatch(MainEvent.HIDE_SCENE, name);
			loadedViewRoot [name].SetActive (false);
		}

		public GameObject findLoadedScene(string name) {
			GameObject[] viewRoots = GameObject.FindGameObjectsWithTag("ViewRoot");
			foreach (GameObject obj in viewRoots)
			{
				if (obj.scene.name == name) {
					return obj;
				}
			}
			return null;
		}
	}
}

