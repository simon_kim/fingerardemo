﻿using System;
using System.Collections;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.main;
using fingertalk.vr.keyboard;

namespace com.fingertalk.vrhome.main
{
    public class MainInputService : IMainInputService
    {
		[Inject(ContextKeys.CONTEXT_DISPATCHER)]
		public IEventDispatcher dispatcher { get; set; }

		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		public void Activate()
		{
			GameObject go = new GameObject();
			go.name = "MainInputServiceModule";
			go.AddComponent<MainInputServiceModule>();
			go.GetComponent<MainInputServiceModule>().setDispatcher(dispatcher);
			go.transform.parent = GameObject.Find("DontDestroyRoot").transform; 		}
    }

    class MainInputServiceModule : MonoBehaviour
    {
        private Action<KeyCode> directionControlListener;
		private Action<bool> clickListener;

        private Action<String> deviceControlListener;
        private Action<String> deviceConnectionListener;

        private IEventDispatcher dispatcher;

		void Start() 
		{
		}

        private KeyCode[] directionalKeys = new KeyCode[]
        {
            KeyCode.LeftArrow,
            KeyCode.RightArrow,
            KeyCode.UpArrow,
            KeyCode.DownArrow,
			KeyCode.O,
			KeyCode.P
        };

        void Update()
        {
			if (directionControlListener != null)
            {
                foreach (KeyCode key in directionalKeys)
                {
                    if (Input.GetKey(key))
                    {
                        directionControlListener(key);
                    }
                }
				// Check click event
				if (isSelectKeyDown())
				{ 
					clickListener(true);
				} else if (isSelectKeyUp())
				{
					clickListener(false);
				}
            }
				
        }

		private bool isSelectKeyDown()
		{
			if (Input.GetMouseButtonDown(0) 
				|| Input.GetKeyDown((KeyCode)10) 
				|| Input.GetKeyDown(KeyCode.KeypadEnter) 
				|| Input.GetKeyDown(KeyCode.Return))
			{
				return true;
			} 
			else
			{
				return false;
			}
		}

		private bool isSelectKeyUp()
		{
			if (Input.GetMouseButtonUp(0) 
			|| Input.GetKeyUp((KeyCode)10) 
			|| Input.GetKeyUp(KeyCode.KeypadEnter) 
			|| Input.GetKeyUp(KeyCode.Return))
			{
			    return true;
			}
			else
			{
				return false;
			}
		}

        public void setDispatcher(IEventDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
        }

        private bool isDirectionalKey(KeyCode keycode)
        {
            foreach (KeyCode key in directionalKeys)
            {
                if (keycode == key)
                {
                    Debug.LogWarning("[MainInputService] Directional key will be ignored("+keycode+")");
                    return true;
                }
            }
            return false;
        }

        public void DispatchKeyDown(KeyCode keycode)
        {
			Debug.Log ("DispatchKeyDown" + keycode);
            if (!isDirectionalKey(keycode)) { 
                dispatcher.Dispatch(MainEvent.KEY_DOWN, keycode);
            }
        }

        public void DispatchKeyUp(KeyCode keycode)
        {
            if (!isDirectionalKey(keycode))
            {
                dispatcher.Dispatch(MainEvent.KEY_UP, keycode);
            }
        }

		public void addDirectionControlListener(Action<KeyCode> listener)
        {
            directionControlListener = listener;
        }

		public void addClickListener(Action<bool> listener)
		{
		    clickListener = listener;
		}

		public void OnServiceConnected (String connected)
		{
			deviceConnectionListener (connected);
		}

		public void OnGyroscopeEvent (String evt) 
		{
			deviceControlListener (evt);
		}


		public void addDeviceControlListener(Action<String> listener)
        {
            deviceControlListener = listener;
		}

		public void addDeviceConnectionListener(Action<String> listener)
        {
			deviceConnectionListener = listener;
        }
    }
}

