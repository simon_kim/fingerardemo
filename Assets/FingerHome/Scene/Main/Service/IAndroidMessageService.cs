﻿using UnityEngine;
using System.Collections;

namespace com.fingertalk.vrhome.main
{
	public interface IAndroidMessageService 
	{
		void Activate();
	}
}
