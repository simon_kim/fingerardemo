﻿using System;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace com.fingertalk.vrhome.main
{
	public interface IMainRouterService
	{
		void route (string sceneName);
		void route (string sceneName, Action action);
	}
}

