﻿using System;
using System.Collections;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using com.fingertalk.vrhome.main.events;
using fingertalk.vr.keyboard;

namespace com.fingertalk.vrhome.main
{
	public class AndroidMessageService : IAndroidMessageService {

		[Inject(ContextKeys.CONTEXT_DISPATCHER)]
		public IEventDispatcher dispatcher { get; set; }

		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		public void Activate()
		{
			GameObject go = new GameObject();
			go.name = "AndroidMessageServiceModule";
			go.AddComponent<AndroidMessageServiceModule>();
			go.GetComponent<AndroidMessageServiceModule>().setDispatcher(dispatcher);
			go.transform.parent = GameObject.Find("DontDestroyRoot").transform; 
		}
	}

	class AndroidMessageServiceModule : MonoBehaviour
	{
		private IEventDispatcher dispatcher;

		public void setDispatcher(IEventDispatcher dispatcher)
		{
			this.dispatcher = dispatcher;
		}

		public void receiveCall(string data) 
		{
			dispatcher.Dispatch (MainEvent.RECEIVED_CALL, data);
		}

		public void receiveSms(string data) 
		{
			dispatcher.Dispatch (MainEvent.RECEIVED_SMS, data);
		}

		IEnumerator clearToast()
		{
			yield return new WaitForSeconds (3.0f);

			dispatcher.Dispatch (MainEvent.MAKE_TOAST, "");
		}
	}
}
