﻿using UnityEngine;
using System.Collections;

public class BackgroundMountainAlpha : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.GetComponent<SpriteRenderer>().material.color = new Color(1f, 1f, 1f, .2f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
