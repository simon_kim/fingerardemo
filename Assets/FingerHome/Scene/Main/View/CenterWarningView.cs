﻿using System;
using System.Collections;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine.SceneManagement;
using fingertalk.vr.message;

namespace com.fingertalk.vrhome.main
{
	public class CenterWarningView : View
	{

		GameObject warningUI;
		GameObject messagePrefab;
		GameObject messageIcon;
		GameObject messageReplyContent;
		public bool isWarning = false;
		public bool messageNotification = false;
		public float leftThreshold = -0.2f;
		public float rightThreshold = 0.2f;

		[Inject]
		public IEventDispatcher dispatcher { get; set; }

		internal void init()
		{
		}

		void Start() {

		}

		void Update() {
			
		}
	}
}

