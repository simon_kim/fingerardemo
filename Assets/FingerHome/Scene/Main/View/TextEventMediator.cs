﻿using System;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.movieplayer.events;
using fingertalk.vr.keyboard;

namespace com.fingertalk.vrhome.main
{
	public class TextEventMediator : EventMediator
	{	
		private TextEventView textEventView;

		protected void SetTextEventView(TextEventView view) {
			this.textEventView = view;
		}

		public override void OnRegister()
		{
			dispatcher.AddListener (FingertalkEvent.TEXT_CHANGE, onTextChange);
			dispatcher.AddListener (FingertalkEvent.TEXT_COMMIT, onTextCommit);
			dispatcher.AddListener (FingertalkEvent.TEXT_DELETE, onTextDelete);
			dispatcher.AddListener (FingertalkEvent.TEXT_SELECT, onTextSelect);
			dispatcher.AddListener (FingertalkEvent.COMPLETE, onTextComeplete);
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (FingertalkEvent.TEXT_CHANGE, onTextChange);
			dispatcher.RemoveListener (FingertalkEvent.TEXT_COMMIT, onTextCommit);
			dispatcher.RemoveListener (FingertalkEvent.TEXT_DELETE, onTextDelete);
			dispatcher.RemoveListener (FingertalkEvent.TEXT_SELECT, onTextSelect);
			dispatcher.RemoveListener (FingertalkEvent.COMPLETE, onTextComeplete);
		}

		protected virtual void onTextComeplete()
		{
			this.textEventView.DisableInputAssist ();
		}

		protected void onTextSelect()
		{
			Debug.Log ("onTextSelect()");
			if (textEventView.inputFieldFocused) {
				textEventView.SetCursorPositionFromAim ();
			}
		}

		protected virtual void onTextChange(IEvent evt)
		{
			Debug.Log ("SearchMediator onTextChange :"+ (evt.data as string));
			textEventView.OutUIText = evt.data as string;
		}

		protected void onTextCommit(IEvent evt)
		{
			string str = evt.data as string;
			if (str == " ") {
				textEventView.OutUIText = str;
			}

			textEventView.CommitText ();
		}

		protected virtual void onTextDelete()
		{
			textEventView.DeleteCommitedText ();
		}
	}
}