﻿//using System;
//using System.Collections;
using UnityEngine;
//using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
//using UnityEngine.SceneManagement;
//using com.fingertalk.vrhome.main.events;
//using com.fingertalk.vrhome.movieplayer.events;
//using fingertalk.vr.keyboard;
//using com.fingertalk.vrhome.webview;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace com.fingertalk.vrhome.main
{
	public class TextEventView : EventView
	{
		private int cursorPosition = 0;
		private int composingStartPosition = 0;

		private string commitedText = "";
		private string composingText = "";
		private Text textComponent;
		private GameObject underlineGO;
		private RawImage underlineImage;
		private GameObject cursorGO;
		private RawImage cursorImage;
		private CanvasScaler scaler;

		private Font font;
		private string fontName = "Roboto-Black";
		private int fontSize = 20;
		private int textStartPositionX = 0;

		private int[] cursorPositionInfo;

		public bool inputFieldFocused = false;

		public void EnableInputAssist() {
			textComponent = GetComponentInChildren<Text>();
			scaler = GetComponentInChildren<CanvasScaler>();
			textStartPositionX = (int) -textComponent.rectTransform.rect.width / 2;

			font = Font.CreateDynamicFontFromOSFont(fontName, fontSize);

			string sample = "가";
			font.RequestCharactersInTexture (sample);

			CreateUnderline ();
			CreateCursor ();

			underlineGO.SetActive (false);
			cursorGO.SetActive (true);
			DrawCursor ();

			StartCoroutine ("BlinkCursor");

//			DestroyEventTrigger ();
		}

		public void DisableInputAssist() {
			StopCoroutine ("BlinkCursor");
			underlineGO.SetActive (false);
			cursorGO.SetActive (false);
		}

		private void DestroyEventTrigger() {
			Image image = GetComponentInChildren<Image> ();
			EventTrigger trigger = image.GetComponentInChildren<EventTrigger> ();

			Destroy (trigger);
		}

		public string OutUIText {
			get { return textComponent.text; }
			set {
				composingText = value;

				if (composingStartPosition == commitedText.Length) {
					textComponent.text = commitedText + composingText;
				} else {
					textComponent.text = commitedText.Insert (composingStartPosition, composingText);
				}

				cursorPosition = composingStartPosition + composingText.Length;
				DrawCursor ();

				if (composingText.Length == 0) {
					underlineGO.SetActive (false);
				} else {
					underlineGO.SetActive (true);
					DrawUnderline ();
				}
			}
		}

		public void SetCursorPositionFromAim() {
			Debug.Log("setCursorPositionFromAim");
            //float pointerX = pointer.lastPosition [0];
            float pointerX = 0.0f;
            int pointerXPixel = (int) (pointerX * scaler.referencePixelsPerUnit * 2);

			SaveCharPositionInfo ();

			int cursorPosition = 0;
			if (pointerXPixel >= cursorPositionInfo [cursorPositionInfo.Length - 1]) {
				cursorPosition = cursorPositionInfo.Length-1;
			} else {
				for (int i=0; i<cursorPositionInfo.Length; i++) {
					if (pointerXPixel < cursorPositionInfo [i]) {
						cursorPosition = i;
						break;
					}
				}
			}

			Debug.Log(cursorPosition);

			SetCursorPosition (cursorPosition);
		}

		private void SaveCharPositionInfo(){
			int startPoint = textStartPositionX;
			cursorPositionInfo = new int[OutUIText.Length+1];
			cursorPositionInfo [0] = startPoint + font.characterInfo[0].glyphWidth/2;
			font.RequestCharactersInTexture(OutUIText);
			for (int i = 0; i < OutUIText.Length; i++) {
				CharacterInfo info;
				font.GetCharacterInfo (OutUIText [i], out info);
				cursorPositionInfo [i + 1] = cursorPositionInfo [i] + info.advance;
			}
		}

		public void SetCursorPosition(int position) {
			CommitText ();

			cursorPosition = position;
			composingStartPosition = position;

			DrawCursor ();
		}

		public void DeleteCommitedText() {
			if (cursorPosition > 0) {
				cursorPosition--;
				composingStartPosition--;

				commitedText = commitedText.Remove (cursorPosition, 1);
				textComponent.text = commitedText;

				DrawCursor ();
			}
		}

		public void CommitText(){
			if (composingStartPosition == commitedText.Length) {
				commitedText += composingText;
			} else {
				commitedText = commitedText.Insert (composingStartPosition, composingText);
			}

			composingStartPosition += composingText.Length;

			composingText = "";

			underlineGO.SetActive (false);
		}

		private void CreateUnderline() {
			underlineGO = new GameObject ();
			underlineGO.name = "underline";
			underlineImage = underlineGO.AddComponent<RawImage> ();
			underlineImage.texture = Resources.Load("Images/dot", typeof(Texture)) as Texture;
			underlineImage.transform.SetParent (textComponent.transform);
			underlineImage.transform.localScale = new Vector3 (1f, 1f, 1f);
			underlineImage.rectTransform.pivot = new Vector2(0,0);
			underlineImage.rectTransform.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, 3);
		}

		private void CreateCursor() {
			cursorGO = new GameObject ();
			cursorGO.name = "cursor";
			cursorImage = cursorGO.AddComponent<RawImage> ();
			cursorImage.texture = Resources.Load("Images/dot", typeof(Texture)) as Texture;
			cursorImage.transform.SetParent (textComponent.transform);
			cursorImage.transform.localScale = new Vector3 (1f, 1f, 1f);
			cursorImage.rectTransform.pivot = new Vector2(0,0);
			cursorImage.rectTransform.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, 3);
			cursorImage.rectTransform.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, font.characterInfo [0].glyphHeight);
		}

		private void DrawUnderline() {
			float fontHeight = font.characterInfo [0].glyphHeight;

			float startX = GetTextWidth (commitedText.Substring(0,composingStartPosition)) + textStartPositionX;
			float stratY = -underlineImage.rectTransform.rect.height / 2 - fontHeight/2;

			underlineImage.rectTransform.localPosition = new Vector3 (startX, stratY, 0);
			underlineImage.rectTransform.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, GetTextWidth(composingText));
		}

		private void DrawCursor() {
			float fontHeight = font.characterInfo [0].glyphHeight;

			float startX = GetTextWidth (OutUIText.Substring(0,cursorPosition)) - textComponent.rectTransform.rect.width / 2 + 2;
			float stratY = -cursorImage.rectTransform.rect.height / 2;

			cursorImage.rectTransform.localPosition = new Vector3 (startX, stratY, 0);
		}

		private IEnumerator BlinkCursor(){
			bool isActive = true;
			while (true) {
				yield return new WaitForSeconds (0.5f);
				isActive = !isActive;
				cursorGO.SetActive (isActive);
			}
		}

		private float GetTextWidth(string str){
			font.RequestCharactersInTexture(str);

			float width = 0;
			for (int i = 0; i < str.Length; i++) {
				CharacterInfo info;
				font.GetCharacterInfo (str [i], out info);
				width += info.advance;
			}

			return width;
		}


		public void OnFocusIn() 
		{
			inputFieldFocused = true;
		}

		public void OnFocusOut() 
		{
			inputFieldFocused = false;
		}
	}
}

