﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine.SceneManagement;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.movieplayer.events;
using fingertalk.vr.keyboard;
using com.fingertalk.vrhome.webview;


namespace com.fingertalk.vrhome.main
{
	public class MainView : View
    {
        private IEventDispatcher contextDispatcher;

		public Texture keyboardBackgroundTexture;

        internal void init(IEventDispatcher dispatcher)
        {
			Debug.Log ("[MainView] init");
            contextDispatcher = dispatcher;
        }

		void Update() 
		{
		}

        public void ShowKeyboard()
        {
			GameObject WebviewGO = GameObject.Find("WebviewPlane");
			drawKeyboardBackground (WebviewGO);
			PrefabAsyncLoader loader = GameObject.FindObjectOfType<PrefabAsyncLoader>();

			loader.LoadPrefab("Fingertalk", WebviewGO.transform, (GameObject instance) => {
				instance.name = "Fingertalk";
				instance.transform.localPosition = new Vector3(0, 1, 0);
				instance.transform.Rotate(new Vector3(0.0f, 0.0f, 0.0f));
				instance.transform.localScale = new Vector3(0.02f,0.02f,0.02f);
//				contextDispatcher.AddListener(FingertalkEvent.TEXT_CHANGE, OnTextChange);
//                contextDispatcher.AddListener(FingertalkEvent.COMPLETE, onComplete);
            });
        }

        public void HideKeyboard()
        {
			Destroy(GameObject.Find("KeyBoardBackground"));
            Destroy(GameObject.Find("Fingertalk"));
//            contextDispatcher.RemoveListener(FingertalkEvent.TEXT_CHANGE, OnTextChange);
        }

        private void onComplete()
        {
            HideKeyboard();
			// TODO : send key input event to Android.
			AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
			activity.Call ("startSearchToAndroidWebview", "complete");
        }

//        public void OnTextChange(IEvent evt)
//        {
//            GameObject WebviewGO = GameObject.Find("WebviewPlane");
//            WebviewPlaneView webview = WebviewGO.GetComponent<WebviewPlaneView>();
//            webview.SetText(evt.data as String);
//        }

		public void drawKeyboardBackground(GameObject obj) {
			GameObject background = new GameObject ();
			background.name = "KeyBoardBackground";
			background.AddComponent<RawImage> ();
			background.GetComponent<RectTransform> ().localPosition = new Vector3 (0, 1, -0.5f);
			background.GetComponent<RectTransform> ().localRotation = new Quaternion (0, 0, 0, 0);
			background.GetComponent<RectTransform> ().sizeDelta = new Vector2 (90f, 90f);
			background.GetComponent<RectTransform> ().localScale = new Vector3 (0.03f, 0.02f, 0.03f);

			background.transform.SetParent (obj.transform);
			background.GetComponent<RawImage> ().texture = keyboardBackgroundTexture;
			background.GetComponent<RawImage> ().raycastTarget = true;

		}
    }
}

