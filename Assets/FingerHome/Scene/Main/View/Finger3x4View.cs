﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Audio;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace com.fingertalk.vrhome.main
{
	public class Finger3x4View : EventView
	{
		public bool circle1Focused;
		public bool circle2Focused;
		public bool circle3Focused;

		private GameObject[] fingerCircle = new GameObject[7];
		private GameObject gvrControllerPointer;
		public bool isConnected = false;

		internal void init()
		{
			circle1Focused = false;
			circle2Focused = false;
			circle3Focused = false;

		}

		void Update()
		{
            /*
			if (GvrController.State == GvrConnectionState.Connected) 
			{
				float yaw = GvrController.Orientation.eulerAngles.z;
				if(yaw != null) changedMode(yaw * 0.0174533f);
			}
            */
		}

		public void onControllerDown()
		{
			// hide circle 4, 5, 6, 7, 8, 9, 12
			foreach(GameObject go in fingerCircle) 
			{
				go.SetActive (false);
			}
		}

		public void onControllerUp()
		{
			// show circle 4, 5, 6, 7, 8, 9, 12
			foreach(GameObject go in fingerCircle) 
			{
				go.SetActive (true);
			}
		}

		public void OnCircle1FocusIn() 
		{
			circle1Focused = true;

		}

		public void OnCircle1FocusOut() 
		{
			circle1Focused = false;

		}

		public void OnCircle2FocusIn() 
		{
			circle2Focused = true;

		}

		public void OnCircle2FocusOut() 
		{
			circle2Focused = false;

		}

		public void OnCircle3FocusIn() 
		{
			circle3Focused = true;

		}

		public void OnCircle3FocusOut() 
		{
			circle3Focused = false;

		}

		public void SetPosition(Vector3 position)
		{
			gameObject.transform.position = position;
		}

		public void MakeSoundOfClickedIcon()
		{
			gameObject.GetComponent<AudioSource> ().Play ();
		}

		public void changedMode(float _yaw)
		{
			if(gameObject.transform.parent.name.Equals("ContextRoot")) {
				if ((3 * -Mathf.PI / 2 < _yaw % (2 * Mathf.PI) && _yaw % (2 * Mathf.PI) < -Mathf.PI / 2)
					|| (Mathf.PI / 2 < _yaw % (2 * Mathf.PI) && _yaw % (2 * Mathf.PI) < 3 * Mathf.PI / 2)) {
					gvrControllerPointer.SetActive (false);
					onControllerDown ();
				} else {
					gvrControllerPointer.SetActive (true);
					onControllerUp ();
				}
			}
		}
	}
}