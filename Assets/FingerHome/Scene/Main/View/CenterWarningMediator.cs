﻿using System;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;
using fingertalk.vr.message;
using fingertalk.vr.keyboard;
using fingertalk.vr.phone;

namespace com.fingertalk.vrhome.main
{
	public class CenterWarningMediator : EventMediator
	{
		[Inject]
		public CenterWarningView view { get; set; }

		GameObject fingertalkPrefab;
		GameObject messagePrefab;
		GameObject messageReplyContent;
		MessageReplyView mView ;

		public override void OnRegister()
		{
			view.init();
			dispatcher.AddListener (MainEvent.KEY_DOWN, onKeyDown);
		}

		public override void OnRemove()
		{
			dispatcher.RemoveListener (MainEvent.KEY_DOWN, onKeyDown);
		}

		private void onKeyDown() {
			messageReplyContent = GameObject.Find ("ReplyContent");
			messagePrefab = GameObject.Find ("MessagePrefab");
			if (messageReplyContent != null) {
				mView = (MessageReplyView)messageReplyContent.GetComponent<EventView> ();
			}
			if (view.isWarning) {
				checkFingerTalkPrefab ();
				if (mView != null && mView.replyInputFieldFocused) {
					fingertalkPrefab.SetActive (true);
				} else {
					if (mView != null && fingertalkPrefab != null) {
						fingertalkPrefab.SetActive (false);
					}
					Debug.Log ("recenter");
					//GvrViewer.Instance.Recenter ();
				}
			}
		}

		public void checkFingerTalkPrefab() {
			try {
				Transform t = messagePrefab.transform.Find ("Fingertalk");
				if (t != null) {
					fingertalkPrefab = t.gameObject;
				} else {
					fingertalkPrefab = null;
				}
			} catch (Exception e ) {
				fingertalkPrefab = null;
			}
		}
	}
}

