﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.menu.events;

namespace com.fingertalk.vrhome.main
{
	public class Finger3x4Mediator : EventMediator
	{
		[Inject]
		public Finger3x4View view { get; set; }

        public GameObject HomeTitle;
        public GameObject HomeKeypad;

		public override void OnRegister()
		{
            dispatcher.AddListener (MainEvent.KEY_DOWN, onKeyDown);
            dispatcher.AddListener (MainEvent.HIDE_SCENE, onHideScene);
            dispatcher.AddListener (MainEvent.GO_HOME, onGoHome);
//			dispatcher.AddListener (MainEvent.CONTROLLER_CONNECTED, OnConnected);
            view.init ();
        }

//		public void OnConnected() {
//			view.isConnected = true;
//		}

		public override void OnRemove()
		{
//			dispatcher.RemoveListener (MainEvent.CONTROLLER_CONNECTED, OnConnected);
            dispatcher.RemoveListener (MainEvent.KEY_DOWN, onKeyDown);
		}

        public void onGoHome()
        {
            StartCoroutine(delayKeyEnable());
        }

        private IEnumerator delayKeyEnable()
        {
            yield return new WaitForSeconds(2.5f);
            dispatcher.AddListener(MainEvent.KEY_DOWN, onKeyDown);
        }

        public void onHideScene(IEvent evt)
        {
            if (((string)evt.data).Equals(gameObject.scene.name))
            {
                dispatcher.RemoveListener(MainEvent.KEY_DOWN, onKeyDown);
            }
        }

        private void onKeyDown(IEvent evt)
        {
            switch ((KeyCode)evt.data)
            {
                case KeyCode.Alpha1:
					view.gameObject.GetComponent<Animator>().SetBool("isClickCircle01", true);
                    StartCoroutine(HandleClickIconEvent(MainEvent.CLICK_WEBVIEW, "Search"));
                    return;
                case KeyCode.Alpha2:
					view.gameObject.GetComponent<Animator>().SetBool("isClickCircle02", true);
                    StartCoroutine(HandleClickIconEvent(MainEvent.CLICK_YOUTUBE, "Webview"));
                    return;
            }
            if (view.circle1Focused)
            {
                view.OnCircle1FocusOut();
                view.MakeSoundOfClickedIcon();
                StartCoroutine(HandleClickIconEvent(MainEvent.CLICK_WEBVIEW, "Search"));
            } else if (view.circle2Focused)
            {
                view.OnCircle2FocusOut();
                view.MakeSoundOfClickedIcon();
                StartCoroutine(HandleClickIconEvent(MainEvent.CLICK_YOUTUBE, "Webview"));
            }
        }

		private IEnumerator HandleClickIconEvent(MainEvent evt, string sceneName)
		{
			Image fadeOutPanel = GameObject.FindGameObjectWithTag ("FadeOut").GetComponent<Image>();
			Color color;

			yield return new WaitForSeconds(1.0f);
			Debug.Log ("Fade Out");
			for (float i = 0; i <= 1; i += 0.1f) {
				color = new Vector4 (0, 0, 0, i);
				fadeOutPanel.color = color;
				yield return new WaitForSeconds(0.05f);
			}

			color = new Vector4 (0, 0, 0, 0);
			fadeOutPanel.color = color;

			if (GameObject.Find ("FingerHome@Main") != null) {
				GameObject.Find ("FingerHome@Main").SetActive (false);
			}
				
			dispatcher.Dispatch(evt, sceneName);
		}
	}
}			
