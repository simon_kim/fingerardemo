﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FingeHomeAnimationController : MonoBehaviour {

	public GameObject finger3x4;
	public GameObject finger3x4atMovie;
	public GameObject finger3x4atWebview;

	public void ShowFinger3x4()
	{
		finger3x4.SetActive(true);
	}

	public void StartFinger3x4atMovie()
	{
		finger3x4atMovie.GetComponent<Animator>().SetBool("isStart", false);
	}

	public void StartFinger3x4atWebview()
	{
		finger3x4atWebview.GetComponent<Animator>().SetBool("isStart", false);
	}
}
