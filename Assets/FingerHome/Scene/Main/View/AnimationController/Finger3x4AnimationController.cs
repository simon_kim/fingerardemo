﻿using UnityEngine;
using System.Collections;

public class Finger3x4AnimationController : MonoBehaviour {

	public void resetClickedState()
	{
		for (int i = 1; i < 13; i++) {
			gameObject.GetComponent<Animator> ().SetBool ("isClickCircle0" + i.ToString(), false);
		}
	}
}
