﻿using System;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.movieplayer.events;

namespace com.fingertalk.vrhome.main
{
    public class MainMediator : EventMediator
    {
        [Inject]
        public MainView view { get; set; }

        public override void OnRegister()
        {
            view.init(dispatcher);
        }

        public override void OnRemove()
		{
		}
    }
}

