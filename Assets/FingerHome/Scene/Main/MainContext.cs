﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;
using com.fingertalk.vrhome.main.events;
using com.fingertalk.vrhome.movieplayer.events;
using fingertalk.vr.keyboard;
using fingertalk.vr.message;

namespace com.fingertalk.vrhome.main
{
    public class MainContext : MVCSContext
    {
        public MainContext(MonoBehaviour view) : base(view)
        {
        }

        public MainContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
			//Any event that fire across the Context boundary get mapped here.
			crossContextBridge.Bind(MainEvent.RECEIVED_SMS);
			crossContextBridge.Bind(MainEvent.RECEIVED_CALL);


			crossContextBridge.Bind(FingertalkEvent.TEXT_CHANGE);
			crossContextBridge.Bind(FingertalkEvent.TEXT_COMMIT);
			crossContextBridge.Bind(FingertalkEvent.TEXT_DELETE);
			crossContextBridge.Bind(FingertalkEvent.TEXT_SELECT);
            crossContextBridge.Bind(FingertalkEvent.COMPLETE);

            crossContextBridge.Bind(MainEvent.PLAY_MOVIE);
			crossContextBridge.Bind(MainEvent.END_MOVIE);
			crossContextBridge.Bind(MainEvent.KEY_DOWN);
            crossContextBridge.Bind(MainEvent.KEY_UP);
            crossContextBridge.Bind(MainEvent.SHOW_SCENE);
			crossContextBridge.Bind(MainEvent.HIDE_SCENE);
            crossContextBridge.Bind(MainEvent.GO_HOME);

            crossContextBridge.Bind(MoviePlayerEvent.PLAY);
			crossContextBridge.Bind(MessageEvent.SMS_SHOW);

            injectionBinder.Bind<IMainModel>().To<MainModel>().ToSingleton();
			injectionBinder.Bind<IMainInputService>().To<MainInputService>().ToSingleton();
			injectionBinder.Bind<IMainRouterService>().To<MainRouterService>().ToSingleton();
			injectionBinder.Bind<IAndroidMessageService>().To<AndroidMessageService>().ToSingleton();

            mediationBinder.Bind<MainView>().To<MainMediator>();
			mediationBinder.Bind<CenterWarningView>().To<CenterWarningMediator>();
			mediationBinder.Bind<Finger3x4View>().To<Finger3x4Mediator>();

            commandBinder.Bind(ContextEvent.START).To<StartCommand>().Once();
			commandBinder.Bind (MainEvent.PLAY_MOVIE).To<PlayMovieCommand> ();
			commandBinder.Bind (MainEvent.END_MOVIE).To<EndMovieCommand> ();
			commandBinder.Bind (MainEvent.MAKE_TOAST).To<MakeToastCommand> ();
//			commandBinder.Bind (MainEvent.SHOW_MESSAGE).To<ShowMessageCommand>();
			commandBinder.Bind (MainEvent.CLICK_WEBVIEW).To<RouteSceneCommand>();
			commandBinder.Bind (MainEvent.CLICK_YOUTUBE).To<RouteSceneCommand>();
			commandBinder.Bind (MainEvent.CLICK_MOVIES).To<RouteSceneCommand>();
			commandBinder.Bind (MainEvent.CLICK_HOME).To<GoHomeCommand>();
            commandBinder.Bind (MainEvent.GO_HOME).To<GoHomeCommand>();
        }

		public IEventDispatcher GetDispatcher() {
			return dispatcher;
		}
    }
}