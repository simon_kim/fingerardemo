﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class DemoMarker : MonoBehaviour, ITrackableEventHandler
{
    private DemoStage demoStage;
    private TrackableBehaviour mTrackableBehaviour;

    public GameObject TargetCar;

    // Use this for initialization
    void Start() {
        demoStage = GameObject.Find("DemoStage").GetComponent<DemoStage>();
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            //OnTrackingFound();
            demoStage.ShowTarget(TargetCar.name);
        }
        else
        {
            //OnTrackingLost();

        }
    }
}