package com.fingertalk.home;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

/**
 * Created by devcraft42 on 2/7/17.
 */

public class FingerTalkVRApplication extends Application implements Application.ActivityLifecycleCallbacks{
    private static Context context;
    private static Activity activity;

    private boolean activityInForeground;

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = getApplicationContext();
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterActivityLifecycleCallbacks(this);
    }

    public synchronized static Context getAppContext() {
        return context;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        activity = currentActivity;
    }

    public static Activity currentActivity() {
        return activity;
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        activityInForeground = true;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        activityInForeground = false;
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    public boolean isActivityInForeground() {
        return activityInForeground;
    }
}
