package com.fingertalk.home;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

//import com.fingertalk.home.ble.service.BTCTemplateService;
//import com.fingertalk.home.ble.util.AppSettings;
//import com.fingertalk.home.ble.util.BLEConstants;
//import com.fingertalk.home.web.JohnWebView;


public class FingerTalkMainActivity extends UnityPlayerActivity {

    private static final String TAG = FingerTalkMainActivity.class.getSimpleName();

    private final int PERMISSION_REQUEST_RECEIVED_PHONE = 11001;
    private final int PERMISSION_REQUEST_RECEIVED_SMS = 11002;
    private final int PERMISSION_REQUEST_READ_CONTACTS = 11003;
    private final int REQUEST_CODE_APPLY_PERMISSION_RECEIVED_SMS = 11004;
    private final int REQUEST_CODE_DENY_PERMISSION_RECEIVED_SMS = 11005;

//    private BTCTemplateService mService;
//    private ActivityHandler mActivityHandler;

    private int index = 0;

    private boolean gyroConnected = false;

    private BroadcastReceiver mIncomingCallReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String state = intent.getStringExtra("state");
            String number = intent.getStringExtra("number");
            String sender = getContactName(FingerTalkMainActivity.this, number);

            if (!TextUtils.isEmpty(sender)) {
                number = sender;
            }

            switch (state) {
                case "ringing":
                    UnityPlayer.UnitySendMessage("AndroidMessageServiceModule", "receiveCall", number);
                    break;
                default:
                    break;
            }
        }
    };

    public void startSearchToAndroidWebview(String keycode) {
        Log.d(TAG, "startSearchToAndroidWebview()");
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                JohnWebView.isSearch = true;
//                JohnWebView.webview.onKeyPreIme(KeyEvent.KEYCODE_ENTER, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
//            }
//        });
    }

    public void deliverSMSToAndroid(String arg) {
        Log.d(TAG, "deliverSMSToAndroid()");
        String[] smsInfo = arg.split(";");
        Log.d(TAG, "deliverSMSToAndroid() receiver :" + smsInfo[0] + " content :" + smsInfo[1]);
        SmsManager.getDefault().sendTextMessage(smsInfo[0], null, "[Auto receive] " + smsInfo[1], null, null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent()");
        if (intent != null) {
            if (intent.getAction() == "fingerTalk.deliver.sms") {
                String phoneNum = intent.getStringExtra("phoneNumber");
                String msg = intent.getStringExtra("message");
                String name = getContactName(this, phoneNum);
                if (TextUtils.isEmpty(name)) {
                    name = new String(phoneNum);
                }
                // TODO : interaction with Unity
                UnityPlayer.UnitySendMessage("AndroidMessageServiceModule", "receiveSms", name + ";" + phoneNum + ";" + msg);
            }
        }
        super.onNewIntent(intent);
    }


    private String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // call UnityPlayerActivity.onCreate()

        super.onCreate(savedInstanceState);
        FingerTalkVRApplication.setCurrentActivity(this);

//        mActivityHandler = new ActivityHandler();
//        AppSettings.initializeAppSettings(this);
        hideKeyBoard(this);

        Log.d(TAG, "onCreate()");
        IntentFilter intentFilter = new IntentFilter("fingertalk.incall");
        /*
        LocalBroadcastManager.getInstance(this).registerReceiver(mIncomingCallReceiver, intentFilter);
        */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //checkSMSPermission();
        }
//        doStartService();
    }

    private void checkSMSPermission() {
        Log.d(TAG, "checkSMSPermission()");
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECEIVE_SMS},
                    PERMISSION_REQUEST_RECEIVED_SMS);
        } else {
            checkContactPermission();
        }
    }

    private void checkContactPermission() {
        Log.d(TAG, "checkContactPermission()");
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    PERMISSION_REQUEST_READ_CONTACTS);
        } else {
            checkInCallPermission();
        }
    }

    private void checkInCallPermission() {
        Log.d(TAG, "checkInCallPermission()");
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE},
                    PERMISSION_REQUEST_RECEIVED_PHONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult() requestCode : " + requestCode);
        if (requestCode == PERMISSION_REQUEST_RECEIVED_SMS) {
            if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finishAffinity();
            } else {
                checkContactPermission();
            }
        } else if (requestCode == PERMISSION_REQUEST_READ_CONTACTS) {
            if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finishAffinity();
            } else {
                checkInCallPermission();
            }
        } else if (requestCode == PERMISSION_REQUEST_RECEIVED_PHONE) {
            // check PHONE_STATE && CALL_PHONE
            if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                finishAffinity();
            } else {
            }
        } else {
            // not catched.
        }
    }

    private void doSettingInterruptPolicy(boolean interrupt) {
        Log.d(TAG, "doSettingInterruptPolicy() : " + interrupt);
        String packageName = Settings.Secure.getString(getContentResolver(), "enabled_notification_policy_access_packages");
        if (null == packageName || !packageName.contains(getPackageName())) {
            Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
            if (interrupt) {
                startActivityForResult(intent, REQUEST_CODE_APPLY_PERMISSION_RECEIVED_SMS);
            } else {
                startActivityForResult(intent, REQUEST_CODE_DENY_PERMISSION_RECEIVED_SMS);
            }
        } else {
            setInterrupt(interrupt);
        }
    }

    private void setInterrupt(boolean state) {
        if (Build.VERSION.SDK_INT >= 23) {
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            String packageName = Settings.Secure.getString(getContentResolver(), "enabled_notification_policy_access_packages");
            if (null != packageName) {
                if (packageName.contains(getPackageName())) {
                    if (state) {
                        nm.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_NONE);
                    } else {
                        nm.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_ALL);
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult() requestCode : " + requestCode + "requestCode : " + resultCode);

        if (requestCode == REQUEST_CODE_DENY_PERMISSION_RECEIVED_SMS) {
            setInterrupt(true);
        } else {
            setInterrupt(false);
        }
//        if (requestCode == BLEConstants.REQUEST_ENABLE_BT) {
//            // When the request to enable Bluetooth returns
//            if (resultCode == Activity.RESULT_OK) {
//                // Bluetooth is now enabled, so set up a BT session
////                mService.setupBT();
////                mService.connectDevice(BLEConstants.BT_DEVICE_ADDRESS2);
//            }
//        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    protected void onPause() {
        Log.d(TAG, "onPause()");
        setInterrupt(false);
        super.onPause();
    }

    protected void onResume() {
        Log.d(TAG, "onResume()");
        hideKeyBoard(this);
        doSettingInterruptPolicy(true);
        super.onResume();
    }

    public boolean onTouchEvent(MotionEvent var1) {
        Log.d(TAG, "onTouchEvent()");
        return super.onTouchEvent(var1);
    }

    public boolean dispatchKeyEvent(KeyEvent var1) {
        if ((var1.getAction() == KeyEvent.ACTION_DOWN || var1.getAction() == KeyEvent.ACTION_MULTIPLE) && var1.getRepeatCount() == 0) {
            UnityPlayer.UnitySendMessage("FingerTalkPluginConnector", "OnPluginKeyDown", Integer.toString(var1.getKeyCode()));
        } else if (var1.getAction() == KeyEvent.ACTION_UP) {
            UnityPlayer.UnitySendMessage("FingerTalkPluginConnector", "OnPluginKeyUp", Integer.toString(var1.getKeyCode()));
        } else if (var1.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            String packagName = Settings.Secure.getString(getContentResolver(), "enabled_notification_policy_access_packages");
            if (!packagName.contains(getPackageName())) {
                setInterrupt(false);
            }
            finishAffinity();
        }
        return super.dispatchKeyEvent(var1);
    }

    @Override
    protected void onDestroy() {
        setInterrupt(false);
        if (null != mIncomingCallReceiver) {
            //LocalBroadcastManager.getInstance(this).unregisterReceiver(mIncomingCallReceiver);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int var1, KeyEvent var2) {
        Log.d(TAG, "onKeyDown called " + var1);
        return super.onKeyDown(var1, var2);
    }

    public void onBackPressed() {
        Log.d(TAG, "onBackPressed called");
        // instead of calling UnityPlayerActivity.onBackPressed() we just ignore the back button event
        // super.onBackPressed();
    }


    public static void hideKeyBoard(Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromInputMethod(((Activity) context).getCurrentFocus().getWindowToken(), 0);
            inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

//    /**
//     * Service connection
//     */
//    private ServiceConnection mServiceConn = new ServiceConnection() {
//
//        public void onServiceConnected(ComponentName className, IBinder binder) {
//
//            mService = ((BTCTemplateService.ServiceBinder) binder).getService();
//            initialize();
//        }
//
//        public void onServiceDisconnected(ComponentName className) {
//            mService = null;
//        }
//    };
//
//    /**
//     * Start service if it's not running
//     */
//    private void doStartService() {
//        startService(new Intent(this, BTCTemplateService.class));
//        bindService(new Intent(this, BTCTemplateService.class), mServiceConn, Context.BIND_AUTO_CREATE);
//    }
//
//    /**
//     * Stop the service
//     */
//    private void doStopService() {
//        mService.finalizeService();
//        stopService(new Intent(this, BTCTemplateService.class));
//    }
//
//    /**
//     * Initialization / Finalization
//     */
//    private void initialize() {
////        onListPopUp().show();
////        onListPopup();
//        onListPopUp();
//    }
//
//    public class ActivityHandler extends Handler {
//        @Override
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                // Receives BT state messages from service
//                // and updates BT state UI
//                case BLEConstants.MESSAGE_BT_STATE_INITIALIZED:
//                    break;
//                case BLEConstants.MESSAGE_BT_STATE_LISTENING:
//                    break;
//                case BLEConstants.MESSAGE_BT_STATE_CONNECTING:
//                    break;
//                case BLEConstants.MESSAGE_BT_STATE_CONNECTED:
//                    gyroConnected = true;
//                    UnityPlayer.UnitySendMessage("MainInputServiceModule", "OnServiceConnected", "Connected");
//                    Toast.makeText(getApplicationContext(),
//                            getString(R.string.bluetooth_connected_toast_msg), Toast.LENGTH_SHORT).show();
//                    if (mService != null)
//                        break;
//                case BLEConstants.MESSAGE_BT_STATE_ERROR:
//                case BLEConstants.MESSAGE_CMD_ERROR_NOT_CONNECTED:
//                    gyroConnected = false;
//                    UnityPlayer.UnitySendMessage("MainInputServiceModule", "OnServiceConnected", "Disconnected");
//                    break;
//                case BLEConstants.MESSAGE_READ_CHAT_DATA:
//                    if (msg.obj != null) {
//                        makeData((String) msg.obj);
//                    }
//                    break;
//
//                default:
//                    break;
//            }
//
//            super.handleMessage(msg);
//        }
//    }
//
//    private String mGyroStr ="";
//    private void makeData(String data) {
//        for (int i = 0; i < data.length(); i++) {
//            if(data.charAt(i) == '|') {
//                String gyro = mGyroStr.replaceAll("[^(0-9,.\\-)]","");
//                UnityPlayer.UnitySendMessage("MainInputServiceModule", "OnGyroscopeEvent", gyro);
//                mGyroStr = "";
//            } else {
//                mGyroStr += data.charAt(i);
//            }
//        }
//    }
//
//    private void checkBLEConnection(String data) {
//        if (gyroConnected) {
//            UnityPlayer.UnitySendMessage("MainInputServiceModule", "OnServiceConnected", "Connected");
//        } else {
//            UnityPlayer.UnitySendMessage("MainInputServiceModule", "OnServiceConnected", "Disconnected");
//        }
//    }
//
//    private void saveControllerAddress(String addr) {
//        SharedPreferences preferences;
//        SharedPreferences.Editor editor;
//        preferences = getSharedPreferences("CTRL_PREFS", Context.MODE_PRIVATE);
//        editor = preferences.edit();
//        editor.putString("CTRL_ADDR", addr);
//        editor.commit();
//    }
//
//    public String loadControllerAddress() {
//        SharedPreferences preferences;
//        String text;
//        preferences = getSharedPreferences("CTRL_PREFS", Context.MODE_PRIVATE); //1
//        text = preferences.getString("CTRL_ADDR", null); //2
//        return text;
//    }
//
//    public void onListPopUp()
//    {
//        final Dialog d = new Dialog(this);
//        Button mCancelBtn;
//        ListView mDeviceListView;
//
//        d.setContentView(R.layout.popup_list_device);
//        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        mDeviceListView = (ListView) d.findViewById(R.id.lv_popup_list);
//        mCancelBtn = (Button) d.findViewById(R.id.bt_popup_cancel);
//        final ArrayAdapter<String> mAdapter = new ArrayAdapter<>(
//                this,
//                android.R.layout.simple_selectable_list_item);
//        for(int i = 0; i < BLEConstants.BT_DEVICES_ADDRESS.length; i++)
//        {
//            mAdapter.add("Controller #" + (i+1));
//        }
//        mDeviceListView.setAdapter(mAdapter);
//        mDeviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                mService.setupService(mActivityHandler, BLEConstants.BT_DEVICES_ADDRESS[position]);
//                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(enableIntent, BLEConstants.REQUEST_ENABLE_BT);
//                mService.connectDevice(BLEConstants.BT_DEVICES_ADDRESS[position]);
//                d.dismiss();
//            }
//        });
//        mCancelBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(FingerTalkMainActivity.this, "Pointer Controller unavailable!!", Toast.LENGTH_LONG).show();
//                d.dismiss();
//            }
//        });
//        d.setCancelable(false);
//        d.show();
//    }

}