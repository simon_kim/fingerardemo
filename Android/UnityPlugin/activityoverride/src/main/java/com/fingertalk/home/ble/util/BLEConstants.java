package com.fingertalk.home.ble.util;

import java.util.HashMap;

public class BLEConstants {
    public static final String BT_DEVICE_ADDRESS1 = "98:D3:32:30:93:A7";
    public static final String BT_DEVICE_ADDRESS2 = "98:D3:32:20:8C:13";
    public static final String BT_DEVICE_ADDRESS3 = "98:D3:32:30:BF:E9";
    public static final String BT_DEVICE_ADDRESS4 = "98:D3:32:10:C9:AB";
    public static final String BT_DEVICE_ADDRESS5 = "98:D3:32:30:BF:AD";
    public static final String BT_DEVICE_ADDRESS6 = "98:D3:32:20:BC:0B";
    public static final String BT_DEVICE_ADDRESS7 = "98:D3:32:20:BB:AD";

    public static final String[] BT_DEVICES_ADDRESS = {BT_DEVICE_ADDRESS1, BT_DEVICE_ADDRESS2, BT_DEVICE_ADDRESS3, BT_DEVICE_ADDRESS4, BT_DEVICE_ADDRESS5, BT_DEVICE_ADDRESS6, BT_DEVICE_ADDRESS7};
    // Service handler message key
    public static final String SERVICE_HANDLER_MSG_KEY_DEVICE_NAME = "device_name";
    public static final String SERVICE_HANDLER_MSG_KEY_DEVICE_ADDRESS = "device_address";
    public static final String SERVICE_HANDLER_MSG_KEY_TOAST = "toast";

    // Preference
    public static final String PREFERENCE_NAME = "btchatPref";
    public static final String PREFERENCE_KEY_BG_SERVICE = "BackgroundService";
    public static final String PREFERENCE_CONN_INFO_ADDRESS = "device_address";
    public static final String PREFERENCE_CONN_INFO_NAME = "device_name";

    // Message types sent from Service to Activity
    public static final int MESSAGE_CMD_ERROR_NOT_CONNECTED = -50;

    public static final int MESSAGE_BT_STATE_INITIALIZED = 1;
    public static final int MESSAGE_BT_STATE_LISTENING = 2;
    public static final int MESSAGE_BT_STATE_CONNECTING = 3;
    public static final int MESSAGE_BT_STATE_CONNECTED = 4;
    public static final int MESSAGE_BT_STATE_ERROR = 10;

    public static final int MESSAGE_READ_CHAT_DATA = 201;


    // Intent request codes
    public static final int REQUEST_ENABLE_BT = 2;

}
