package com.fingertalk.home.permission;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import com.fingertalk.home.FingerTalkMainActivity;
import com.fingertalk.home.FingerTalkVRApplication;

public class FingerTalkSmsReceiver extends BroadcastReceiver {

    private static final String TAG = FingerTalkSmsReceiver.class.getSimpleName();

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d(TAG, "onReceive()");

        FingerTalkVRApplication application = (FingerTalkVRApplication) context.getApplicationContext();
        if(application.isActivityInForeground()) {
            SmsMessage[] currentMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);

            if(currentMessages.length > 0) {
                final String phoneNumber = currentMessages[0].getDisplayOriginatingAddress();
                final String message = currentMessages[0].getDisplayMessageBody();

                final Intent smsServiceIntent = new Intent(context, FingerTalkMainActivity.class);
                smsServiceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                smsServiceIntent.setAction("fingerTalk.deliver.sms");
                smsServiceIntent.putExtra("phoneNumber", phoneNumber);
                smsServiceIntent.putExtra("message", message);
                context.startActivity(smsServiceIntent);
            }
        }
    }

}
