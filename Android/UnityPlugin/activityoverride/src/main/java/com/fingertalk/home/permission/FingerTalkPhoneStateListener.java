package com.fingertalk.home.permission;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

/**
 * Created by devcraft42 on 2/9/17.
 */

public class FingerTalkPhoneStateListener extends PhoneStateListener {

    private static final String TAG = FingerTalkPhoneStateListener.class.getSimpleName();
    private Context context;
    private ITelephony telephonyService;

    public FingerTalkPhoneStateListener(Context context) {
        super();
        this.context = context;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);
        Log.d(TAG, "onCallStateChanged()" + " - state : " + state);

        Intent i;
        i = new Intent("fingertalk.incall");

        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                try {
                    Class c = Class.forName(telephonyManager.getClass().getName());
                    Method m = c.getDeclaredMethod("getITelephony");
                    m.setAccessible(true);
                    telephonyService = (ITelephony) m.invoke(telephonyManager);
                    telephonyService.endCall();
                } catch(Exception e) {
                    e.printStackTrace();
                }

                i.putExtra("state", "ringing");
                i.putExtra("number", incomingNumber);
                LocalBroadcastManager.getInstance(context).sendBroadcast(i);
                break;

            default:
                i.putExtra("state", "cancel");
                LocalBroadcastManager.getInstance(context).sendBroadcast(i);
                break;
        }
    }
}
