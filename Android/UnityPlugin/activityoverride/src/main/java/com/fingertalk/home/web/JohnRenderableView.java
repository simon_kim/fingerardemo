package com.fingertalk.home.web;

import android.app.Activity;
import android.graphics.Color;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.widget.FrameLayout;

import com.unity3d.player.UnityPlayer;

import java.util.LinkedList;

/**
 * Created by devcraft42 on 3/20/17.
 */

public class JohnRenderableView {
    public static final int ACTION_DOWN = 1;
    public static final int ACTION_UP = 2;
    public static final int ACTION_MOVE = 3;
    public static final int ACTION_CANCEL = 4;
    protected Activity activity;
    protected View view = null;
    protected FrameLayout frameLayout = null;
    protected Surface surface = null;
    protected boolean hide = true;
    protected boolean debug = false;
    protected int width = -1;
    protected int height = -1;
    protected LinkedList<MotionEvent> eventQueue = new LinkedList();
    protected long lastDownTime = 0L;
    protected String delegateGameObjectName = null;
    protected String delegateMethodName = null;
    protected JohnRenderableViewDelegate delegate = null;

    public JohnRenderableView(int width, int height) {
        log("Constructor with width + height = " + width + " + " + height);

        this.width = width;
        this.height = height;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Activity getActivity() {
        if (this.activity == null) {
            return UnityPlayer.currentActivity;
        }
        return this.activity;
    }

    void resolveFrameLayout() {
        try {
            this.frameLayout = ((FrameLayout) getActivity().getWindow().getDecorView().getRootView());
            log("Got framelayout from window");
        } catch (Exception e) {
            this.frameLayout = null;
        }
        if (this.frameLayout == null) {
            this.frameLayout = new FrameLayout(getActivity());
            getActivity().addContentView(this.frameLayout, new FrameLayout.LayoutParams(this.width, this.height));
            log("Made framelayout from code");
        }
        this.frameLayout.setFocusable(true);
        this.frameLayout.setFocusableInTouchMode(true);
    }

    void setUpView(View view) {
        resolveFrameLayout();

        view.setLayerType(1, null);
        this.frameLayout.addView(view, new FrameLayout.LayoutParams(this.width, this.height, 0));
        this.view = view;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public void setHidden(boolean hide) {
        this.hide = hide;
    }

    public void setSurface(Surface surface) {
        log("Set surface with " + surface);
        this.surface = surface;
    }

    public void setBackgroundColor(final String color) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                JohnRenderableView.this.view.setBackgroundColor(Color.parseColor(color));
            }
        });
    }

    public void setDelegate(String gameObjectName, String methodName) {
        this.delegateGameObjectName = gameObjectName;
        this.delegateMethodName = methodName;
    }

    public void setDelegate(JohnRenderableViewDelegate delegate) {
        this.delegate = delegate;
    }

    protected void sendMessageToDelegate(final String message) {
        if ((this.delegateGameObjectName != null) && (this.delegateMethodName != null)) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    UnityPlayer.UnitySendMessage(JohnRenderableView.this.delegateGameObjectName, JohnRenderableView.this.delegateMethodName, message);
                }
            });
        } else if (this.delegate != null) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    JohnRenderableView.this.delegate.sendMessage(message);
                }
            });
        } else {
            log("Delegate is not defined");
        }
    }

    protected void responseMessageToDelegate(final String message) {

    }

    public void eventAdd(int x, int y, int actionType) {
        log("Add event to queue: " + x + "," + y);
        this.lastDownTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        int motionEventType = 2;
        switch (actionType) {
            case 1:
                motionEventType = 0;
                break;
            case 2:
                motionEventType = 1;
                break;
            case 3:
                motionEventType = 2;
                break;
            case 4:
                motionEventType = 3;
                break;
        }
        this.eventQueue.add(MotionEvent.obtain(this.lastDownTime, eventTime, motionEventType, x, y, 0));
    }

    public void eventPoll() {
        if (this.eventQueue.size() <= 0) {
            return;
        }
        if (this.view == null) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                MotionEvent e = JohnRenderableView.this.eventQueue.poll();
                if (e == null) {
                    return;
                }
                JohnRenderableView.this.view.dispatchTouchEvent(e);
//                JohnRenderableView.this.log("Pool event from queue: " + e.getX() + "," + e.getY());
//                if (e.getAction() == KeyEvent.ACTION_DOWN || e.getAction() == KeyEvent.ACTION_UP) {
//                    JohnRenderableView.this.getActivity().dispatchKeyEvent(e);
//                }
            }
        });
    }

    void invalidate() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                JohnRenderableView.this.view.invalidate();
            }
        });
    }

    void log(String message) {
        if (this.debug) {
            Log.i(getClass().getSimpleName(), message);
        }
    }
}
