package com.fingertalk.home.web;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fingertalk.home.FingerTalkMainActivity;
import com.unity3d.player.UnityPlayer;

import java.util.LinkedList;

/**
 * Created by devcraft42 on 3/20/17.
 */

public class JohnWebView extends JohnRenderableView {
    private static final String TAG = "JohnWebView";
    public static CustomWebView webview = null;
    public static boolean isSearch = false;
    private int scale = 300;

    public JohnWebView(int width, int height) {
        super(width, height);
        init();
    }

    public JohnWebView(int width, int height, Activity activity) {
        super(width, height);
        setActivity(activity);
        init();
    }

    private void init() {
        Log.d(TAG, "init()");
        getActivity().runOnUiThread(new Runnable() {
            @SuppressLint({"SetJavaScriptEnabled"})
            public void run() {
                scale = 300;
                JohnWebView.this.webview = new JohnWebView.CustomWebView(JohnWebView.this.getActivity());

                JohnWebView.this.webview.setFocusable(true);
                JohnWebView.this.webview.setFocusableInTouchMode(true);

                JohnWebView.this.webview.getSettings().setJavaScriptEnabled(true);

                webview.getSettings().setSupportZoom(true);
                webview.getSettings().setBuiltInZoomControls(true);
                webview.getSettings().setDisplayZoomControls(true);

                JohnWebView.this.webview.setPadding(0, 0, 0, 0);
                JohnWebView.this.webview.setInitialScale(scale);

                JohnWebView.this.webview.setWebChromeClient(JohnWebView.this.webChromeClient);
                JohnWebView.this.webview.setWebViewClient(JohnWebView.this.webViewClient);
                JohnWebView.this.webview.getSettings().setTextZoom(webview.getSettings().getTextZoom() * 2);
                JohnWebView.this.setUpView(JohnWebView.this.webview);
                JohnWebView.this.webview.setScrollbarFadingEnabled(true);
                JohnWebView.this.webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_INSET);

                JohnWebView.this.webview.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        FingerTalkMainActivity.hideKeyBoard(view.getContext());
                        return false;
                    }
                });
            }
        });
    }

    private int getScale() {
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        Point point = new Point();
        display.getSize(point);
        int screenHeight = point.y;
        return (int) (screenHeight / this.width);
    }

    public WebView getWebview() {
        return JohnWebView.this.webview;
    }

    public void loadUrl(final String url) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                JohnWebView.this.webview.loadUrl(url);
            }
        });
    }

    public void loadData(final String data) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                JohnWebView.this.webview.loadData(data, "text/html; charset=utf-8", "UTF-8");
            }
        });
    }

    public void searchKeyword() {
        Log.d(TAG, "searchKeyword");
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                JohnWebView.this.webview.onKeyDown(KeyEvent.KEYCODE_SEARCH, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_SEARCH));
            }
        });
    }

    public void goBack() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                JohnWebView.this.webview.goBack();
            }
        });
    }

    public void goForward() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                JohnWebView.this.webview.goForward();
            }
        });
    }

    public void setText(final String text) {
        Log.d(TAG, "setText - " + text);
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                InputConnection inputConnection = JohnWebView.this.webview.inputConnection;
                if (JohnWebView.this.webview.inputConnection != null) {
                    try {
                        //inputConnection.finishComposingText();
                        CharSequence currentText = inputConnection.getExtractedText(new ExtractedTextRequest(), 0).text;
                        if (currentText != null) {
                            CharSequence beforCursorText = inputConnection.getTextBeforeCursor(currentText.length(), 0);
                            CharSequence afterCursorText = inputConnection.getTextAfterCursor(currentText.length(), 0);
                            inputConnection.deleteSurroundingText(beforCursorText.length(), afterCursorText.length());
                            inputConnection.commitText(text, text.length() - 1);
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "Error Error Error");
                        inputConnection.commitText(text, text.length() - 1);
                    }
                }
            }
        });
    }

    public void setZoomIn() {
        if (scale < 600) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "setZoomIn : getScale : " + webview.getScale() + "current scale : " + scale + "JohnWebView.this.webview.getHeight() : " + JohnWebView.this.webview.getHeight());
                    scale = scale + 30;
                    JohnWebView.this.webview.setInitialScale(scale);
                }
            });
        }
    }

    public void setZoomOut() {
        if (scale > 150) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "setZoomOut: getScale : " + webview.getScale() + "current scale : " + scale + "JohnWebView.this.webview.getHeight() : " + JohnWebView.this.webview.getHeight());
                    scale = scale - 30;
                    JohnWebView.this.webview.setInitialScale(scale);
                }
            });
        }
    }

    public void setScrollUp() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "setScrollUp : " + JohnWebView.this.webview.scrollY + "JohnWebView.this.webview.getHeight() : " + JohnWebView.this.webview.getHeight());
                if (JohnWebView.this.webview.scrollY >= 0)
                    JohnWebView.this.webview.scrollBy(0, -180);
            }
        });
    }

    public void setScrollDown() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "setScrollDown : " + JohnWebView.this.webview.scrollY + "JohnWebView.this.webview.getHeight() : " + JohnWebView.this.webview.getHeight());
                if (JohnWebView.this.webview.scrollY < JohnWebView.this.webview.getHeight() * 4)
                    JohnWebView.this.webview.scrollBy(0, 180);
            }
        });
    }

    public class CustomWebView extends WebView {
        public InputConnection inputConnection;
        private int scrollX = 0;
        private int scrollY = 0;

        public CustomWebView(Context context) {
            super(context);
        }

        public CustomWebView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        public void loadUrl(String url) {
            JohnWebView.this.log("Load url " + url);
            super.loadUrl(url);
        }

        public void loadData(String data, String mimeType, String encoding) {
            JohnWebView.this.log("Load data " + data);
            super.loadData(data, mimeType, encoding);
        }

        protected void onScrollChanged(int l, int t, int oldl, int oldt) {
            super.onScrollChanged(l, t, oldl, oldt);
            this.scrollX = l;
            this.scrollY = t;
        }

        public void draw(Canvas originCanvas) {
            JohnWebView.this.log("Draw");
            if (JohnWebView.this.surface != null) {
                JohnWebView.this.log("Draw surface not null");
                try {
                    Canvas canvasFromSurface = JohnWebView.this.surface.lockCanvas(null);
                    if (canvasFromSurface == null) {
                        throw new Exception("Canvas from Surface not ready");
                    }
//                    float scale = canvasFromSurface.getWidth() / originCanvas.getWidth();
                    canvasFromSurface.scale(scale / 300, scale / 300);
                    canvasFromSurface.translate(-this.scrollX, -this.scrollY);

                    super.draw(canvasFromSurface);

                    JohnWebView.this.surface.unlockCanvasAndPost(canvasFromSurface);
                } catch (Exception e) {
                    Log.e("JohnWebView", "Exception: " + e.getMessage());
                }
            }
            if (!JohnWebView.this.hide) {
                super.draw(originCanvas);
            }
        }

        private boolean isIME = false;

        @Override
        public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
            Log.d(TAG, "FingerTalk onCreateInputConnection");
            inputConnection = super.onCreateInputConnection(outAttrs);
            if (inputConnection != null) {
                Log.d(TAG, "Input Connection Not Null");
                Log.d(TAG, inputConnection.toString());
                FingerTalkMainActivity.hideKeyBoard(CustomWebView.this.getContext());
                if (!isIME) {
                    UnityPlayer.UnitySendMessage("FingerTalkPluginConnector", "OnPluginEvent", "ShowKeyboard");
                    isIME = true;
                }
            } else {
                Log.d(TAG, "Input Connection Null");
                if (isIME) {
                    UnityPlayer.UnitySendMessage("FingerTalkPluginConnector", "OnPluginEvent", "HideKeyboard");
                    isIME = false;
                }
            }
            return inputConnection;
        }

        @Override
        public boolean onKeyPreIme(int keyCode, KeyEvent event) {
            // Return true if I handle the event:
            // In my case i want the keyboard to not be dismissible so i simply return true
            // Other people might want to handle the event differently
            FingerTalkMainActivity.hideKeyBoard(CustomWebView.this.getContext());
            if (isSearch) {
                dispatchKeyEvent(event);
                isSearch = false;
            }
            if (event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {
                Log.d(TAG, "onKeyPreIme() - Call OnPluginKeyDown : " + Integer.toString(event.getKeyCode()));
                UnityPlayer.UnitySendMessage("FingerTalkPluginConnector", "OnPluginKeyDown", Integer.toString(event.getKeyCode()));
            } else if (event.getAction() == KeyEvent.ACTION_UP) {
                Log.d(TAG, "onKeyPreIme() - Call OnPluginKeyUp : " + Integer.toString(event.getKeyCode()));
                UnityPlayer.UnitySendMessage("FingerTalkPluginConnector", "OnPluginKeyUp", Integer.toString(event.getKeyCode()));

            }
            return true;
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            FingerTalkMainActivity.hideKeyBoard(CustomWebView.this.getContext());
            return super.dispatchTouchEvent(ev);
        }

        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event) {
            Log.d(TAG, "onKeyDown : " + event.toString());
            return super.onKeyDown(keyCode, event);
        }
    }

    private WebChromeClient webChromeClient = new WebChromeClient() {
    };
    private WebViewClient webViewClient = new WebViewClient() {

        public boolean shouldOverrideUrlLoading(WebView view, String request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            scale = 300;
            JohnWebView.this.invalidate();
            JohnWebView.this.sendMessageToDelegate("onPageStarted:" + url);
            super.onPageStarted(view, url, favicon);
        }

        public void onPageFinished(WebView view, String url) {
            JohnWebView.this.invalidate();
            JohnWebView.this.sendMessageToDelegate("onPageFinished:" + url);
            super.onPageFinished(view, url);
        }

        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            JohnWebView.this.invalidate();
            JohnWebView.this.sendMessageToDelegate("onReceivedError:" + error.toString());
            super.onReceivedError(view, request, error);
        }
    };

    private class multiThread {
        LinkedList<Integer> list = new LinkedList<Integer>();
        Object lock = new Object();
        int value = 0;

        private void consume() throws InterruptedException {
            while (true) {
                synchronized (lock) {
                    while (list.size() == 0) {
                        lock.wait();
                    }
                    value = list.removeFirst();
                    lock.notify();
                }
            }
        }

        private void produce() throws InterruptedException {
            while (true) {
                synchronized (lock) {
                    while (list.size() == 10) {
                        lock.wait();
                    }
                    list.add(value++);
                    lock.notify();
                }
            }
        }
    }

}
