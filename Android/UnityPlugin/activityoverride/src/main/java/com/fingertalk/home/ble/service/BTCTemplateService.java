/*
 * Copyright (C) 2014 Bluetooth Connection Template
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fingertalk.home.ble.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.fingertalk.home.R;
import com.fingertalk.home.ble.BluetoothManager;
import com.fingertalk.home.ble.util.ConnectionInfo;
import com.fingertalk.home.ble.TransactionBuilder;
import com.fingertalk.home.ble.TransactionReceiver;
import com.fingertalk.home.ble.util.AppSettings;
import com.fingertalk.home.ble.util.BLEConstants;
import com.unity3d.player.UnityPlayer;

import java.util.Timer;


public class BTCTemplateService extends Service {
    private static final String TAG = BTCTemplateService.class.getSimpleName();

    // Context, System
    private Context mContext = null;
    private static Handler mActivityHandler = null;
    private ServiceHandler mServiceHandler = new ServiceHandler();
    private final IBinder mBinder = new ServiceBinder();

    // Bluetooth
    private BluetoothAdapter mBluetoothAdapter = null;        // local Bluetooth adapter managed by Android Framework
    private BluetoothManager mBtManager = null;
    private ConnectionInfo mConnectionInfo = null;        // Remembers connection info when BT connection is made

    private TransactionBuilder mTransactionBuilder = null;
    private TransactionReceiver mTransactionReceiver = null;

    // Auto-refresh timer
    private Timer mRefreshTimer = null;
    private Timer mDeleteTimer = null;


    /*****************************************************
     * Overrided methods
     ******************************************************/
    @Override
    public void onCreate() {

        mContext = getApplicationContext();
        initialize();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // If service returns START_STICKY, android restarts service automatically after forced close.
        // At this time, onStartCommand() method in service must handle null intent.
        return Service.START_STICKY;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // This prevents reload after configuration changes
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public void onDestroy() {
        finalizeService();
    }

    @Override
    public void onLowMemory() {
        // onDestroy is not always called when applications are finished by Android system.
        finalizeService();
    }


    /*****************************************************
     * Private methods
     ******************************************************/
    private void initialize() {

        AppSettings.initializeAppSettings(mContext);
        startServiceMonitoring();

        // Make instances
        mConnectionInfo = ConnectionInfo.getInstance(mContext);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_state_not_available), Toast.LENGTH_LONG).show();
            return;
        }

        if (!mBluetoothAdapter.isEnabled()) {
            // BT is not on, need to turn on manually.
            // Activity will do this.
        } else {
            if (mBtManager == null) {
                setupBT();
            }
        }
    }

    /**
     * Send message to device.
     *
     * @param message message to send
     */
    private void sendMessageToDevice(String message) {
        if (message == null || message.length() < 1)
            return;

        TransactionBuilder.Transaction transaction = mTransactionBuilder.makeTransaction();
        transaction.begin();
        transaction.setMessage(message);
        transaction.settingFinished();
        transaction.sendTransaction();
    }

    /**
     *
     */


    /*****************************************************
     * Public methods
     ******************************************************/
    public void finalizeService() {

        // Stop the bluetooth session
        mBluetoothAdapter = null;
        if (mBtManager != null)
            mBtManager.stop();
        mBtManager = null;

        // Stop the timer
        if (mRefreshTimer != null) {
            mRefreshTimer.cancel();
            mRefreshTimer = null;
        }
        if (mDeleteTimer != null) {
            mDeleteTimer.cancel();
            mDeleteTimer = null;
        }

    }

    /**
     * Setting up bluetooth connection
     *
     * @param h
     */
    public void setupService(Handler h, String address) {
        mActivityHandler = h;

        // Double check BT manager instance
        //FIXME : temporary block.
        if (mBtManager == null)
            setupBT();

        // Initialize transaction builder & receiver
        if (mTransactionBuilder == null)
            mTransactionBuilder = new TransactionBuilder(mBtManager, mActivityHandler);
        if (mTransactionReceiver == null)
            mTransactionReceiver = new TransactionReceiver(mActivityHandler);
        mConnectionInfo.setDeviceAddress(address);
        // If ConnectionInfo holds previous connection info,
        // try to connect using it.
        if (mConnectionInfo.getDeviceAddress() != null && mConnectionInfo.getDeviceName() != null) {
            if (isBluetoothEnabled())
                connectDevice(mConnectionInfo.getDeviceAddress());
        }
        // or wait in listening mode
        else {
            if (mBtManager.getState() == BluetoothManager.STATE_NONE) {
                // Start the bluetooth service
                mBtManager.start();
            }
        }
    }

    /**
     * Setup and initialize BT manager
     */
    public void setupBT() {

        // Initialize the BluetoothManager to perform bluetooth connections
        if (mBtManager == null) {
            mBtManager = new BluetoothManager(this, mServiceHandler);
        }
    }

    /**
     * Check bluetooth is enabled or not.
     */
    public boolean isBluetoothEnabled() {
        if (mBluetoothAdapter == null) {
            return false;
        }
        return mBluetoothAdapter.isEnabled();
    }

    /**
     * Get scan mode
     */
    public int getBluetoothScanMode() {
        int scanMode = -1;
        if (mBluetoothAdapter != null)
            scanMode = mBluetoothAdapter.getScanMode();

        return scanMode;
    }

    /**
     * Initiate a connection to a remote device.
     *
     * @param address Device's MAC address to connect
     */
    public void connectDevice(String address) {
    mConnectionInfo.setDeviceAddress(address);
        // Get the BluetoothDevice object
        if (mBluetoothAdapter != null) {
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

            if (device != null && mBtManager != null) {
                mBtManager.connect(device);
            }
        }
    }

    /**
     * Connect to a remote device.
     *
     * @param device The BluetoothDevice to connect
     */
    public void connectDevice(BluetoothDevice device) {
        if (device != null && mBtManager != null) {
            mBtManager.connect(device);
        }
    }

    /**
     * Get connected device name
     */
    public String getDeviceName() {
        return mConnectionInfo.getDeviceName();
    }

    /**
     * Send message to remote device using Bluetooth
     */
    public void sendMessageToRemote(String message) {
        sendMessageToDevice(message);
    }

    /**
     * Start service monitoring. Service monitoring prevents
     * unintended close of service.
     */
    public void startServiceMonitoring() {
        if (AppSettings.getBgService()) {
            ServiceMonitoring.startMonitoring(mContext);
        } else {
            ServiceMonitoring.stopMonitoring(mContext);
        }
    }


    /*****************************************************
     * Handler, Listener, Timer, Sub classes
     ******************************************************/
    public class ServiceBinder extends Binder {
        public BTCTemplateService getService() {
            return BTCTemplateService.this;
        }
    }

    /**
     * Receives messages from bluetooth manager
     */
    class ServiceHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                // Bluetooth state changed
                case BluetoothManager.MESSAGE_STATE_CHANGE:
                    // Bluetooth state Changed

                    switch (msg.arg1) {
                        case BluetoothManager.STATE_NONE:
                            mActivityHandler.obtainMessage(BLEConstants.MESSAGE_BT_STATE_INITIALIZED).sendToTarget();
                            if (mRefreshTimer != null) {
                                mRefreshTimer.cancel();
                                mRefreshTimer = null;
                            }
                            break;

                        case BluetoothManager.STATE_LISTEN:
                            mActivityHandler.obtainMessage(BLEConstants.MESSAGE_BT_STATE_LISTENING).sendToTarget();
                            break;

                        case BluetoothManager.STATE_CONNECTING:
                            mActivityHandler.obtainMessage(BLEConstants.MESSAGE_BT_STATE_CONNECTING).sendToTarget();
                            break;

                        case BluetoothManager.STATE_CONNECTED:
                            mActivityHandler.obtainMessage(BLEConstants.MESSAGE_BT_STATE_CONNECTED).sendToTarget();
                            break;
                    }
                    break;

                // If you want to send data to remote
                case BluetoothManager.MESSAGE_WRITE:
                    break;

                // Received packets from remote
                case BluetoothManager.MESSAGE_READ:

                    byte[] readBuf = (byte[]) msg.obj;
                    int readCount = msg.arg1;
                    // send bytes in the buffer to activity
                    if (msg.arg1 > 0) {
                        String strMsg = new String(readBuf, 0, msg.arg1);
                        mActivityHandler.obtainMessage(BLEConstants.MESSAGE_READ_CHAT_DATA, strMsg).sendToTarget();
                    }
                    break;

                case BluetoothManager.MESSAGE_DEVICE_NAME:

                    // save connected device's name and notify using toast
                    String deviceAddress = msg.getData().getString(BLEConstants.SERVICE_HANDLER_MSG_KEY_DEVICE_ADDRESS);
                    String deviceName = msg.getData().getString(BLEConstants.SERVICE_HANDLER_MSG_KEY_DEVICE_NAME);

                    if (deviceName != null && deviceAddress != null) {
                        // Remember device's address and name
                        mConnectionInfo.setDeviceAddress(deviceAddress);
                        mConnectionInfo.setDeviceName(deviceName);
                    }
                    break;

                case BluetoothManager.MESSAGE_TOAST:

                    Toast.makeText(getApplicationContext(),
                            msg.getData().getString(BLEConstants.SERVICE_HANDLER_MSG_KEY_TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }    // End of switch(msg.what)

            super.handleMessage(msg);
        }
    }    // End of class MainHandler

}
