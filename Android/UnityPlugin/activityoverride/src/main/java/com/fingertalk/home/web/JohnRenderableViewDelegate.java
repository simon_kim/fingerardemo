package com.fingertalk.home.web;

/**
 * Created by devcraft42 on 3/20/17.
 */

public abstract interface JohnRenderableViewDelegate {
    public abstract void sendMessage(String paramString);
    public abstract void responseMessage(String paramString);
}
